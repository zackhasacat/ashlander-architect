local util = require("openmw.util")
local world = require("openmw.world")
local core = require("openmw.core")
local types = require("openmw.types")
local storage = require("openmw.storage")
local I = require("openmw.interfaces")
local interfaces = require("openmw.interfaces")
local objectList = {}
local idList = {}


local function tableToString(tbl, lineLength, currentLength,top)
    lineLength = lineLength or 260
    currentLength = currentLength or 0
    local str = ""

    if type(tbl) == "table" then
        if not top then
            
        str = str .. "{"
        end
        for k, v in pairs(tbl) do
            local keyStr = tableToString(k, lineLength, currentLength + 2)
            local valueStr = tableToString(v, lineLength, currentLength + #keyStr + 6)
            local entryStr = "[" .. keyStr .. "]=" .. valueStr .. ","
            if top then
                entryStr =valueStr .. "|SPL|"
            else
                entryStr = "[" .. keyStr .. "]=" .. valueStr .. ","
            end
            if currentLength + #entryStr > lineLength then
                str = str .. "\n  " .. entryStr
                currentLength = #entryStr + 2
            else
                str = str .. " " .. entryStr
                currentLength = currentLength + #entryStr
            end
        end
        if not top then
        str = str .. "}" 
        end
    elseif type(tbl) == "number" then
        str = str .. tostring(tbl)
    elseif type(tbl) == "string" then
        str = str .. string.format("%q", tbl)
    else
        str = str .. "\"" .. tostring(tbl) .. "\""
    end

    return str
end
local function getEquipped(recordId, actor)
    for i, x in pairs(types.Actor.getEquipment(actor)) do
        if x.recordId == recordId then return true end
    end
    return false
end
local function seralizeInventory(obj)
    local items = {}
    local isActor = false
    if obj.type == types.NPC or obj.type == types.Creature then
        isActor = true
    end
    for index, item in ipairs(types.Container.content(obj):getAll()) do
        local data = {
            recordId = item.recordId,
            count = item.count
        }
        if item.type == types.Miscellaneous and types.Miscellaneous.getSoul(item) then
            data.soulId =  types.Miscellaneous.getSoul(item) 
        end
        if isActor then
            if getEquipped(item.recordId,obj) then
                data.equipped = true
            end
        end
        table.insert(items,data)
    end
    return items
end
local function getRefNum(objId)
    local idNumber = objId--tonumber(string.match(selectedObject.id, "^(.-)_"))
    if idNumber:sub(1, 1) == "@" then
        -- Remove the '@' and convert the rest to a number
        local hex_part = idNumber:sub(2)
        idNumber = tonumber(hex_part, 16) -- Convert from hex to decimal
    end
    return  idNumber % 0x10000
end
local function serializeObject(obj)
    local tbl = {}
    tbl.position = { x = obj.position.x, y = obj.position.y, z = obj.position.z }
    tbl.scale = obj.scale
    tbl.recordId = obj.recordId
    tbl.id = obj.id

    tbl.refNum = getRefNum(obj.id)
    if obj.count ~= 1 then
        tbl.count = obj.count
    end
    --TODO: Handle inventories
    if obj.type == types.NPC or obj.type == types.Creature or obj.type == types.Container then
      
        if not types.Container.content(obj):isResolved() then
            types.Container.content(obj):resolve()
        end
       tbl.inventory = seralizeInventory(obj)
       --print("Saving inventory")
    end
    local z, y, x = obj.rotation:getAnglesZYX()
    if obj.startingRotation and obj.contentFile then
        z, y, x = obj.startingRotation:getAnglesZYX() --make sure doors stay closed
    end
    if obj.contentFile then
        tbl.contentFile = obj.contentFile
    end
    tbl.rotation = { x = x, y = y, z = z }
    if obj.type == types.Miscellaneous and types.Miscellaneous.getSoul(obj) then
        tbl.soulId =  types.Miscellaneous.getSoul(obj) 
    end

    if obj.type == types.Door and types.Door.isTeleport(obj) then
        tbl.teleport                   = {}
        local tpos                     = types.Door.destPosition(obj)
        tbl.teleport.position          = { x = tpos.x, y = tpos.y, z = tpos.z }
        local z, y, x                  = types.Door.destRotation(obj):getAnglesZYX()
        tbl.teleport.rotation          = { x = x, y = y, z = z }
        tbl.teleport.cell              = { name = types.Door.destCell(obj).name }
        local destdoor                 = I.AA_CellGen_2.findDoorPair(obj)[1]
        tbl.teleport.destDoor          = {

        }
        tbl.teleport.destDoor.id       = destdoor.id
        local tpos                     = types.Door.destPosition(destdoor)
        tbl.teleport.destDoor.position = { x = tpos.x, y = tpos.y, z = tpos.z }
        local z, y, x                  = types.Door.destRotation(destdoor):getAnglesZYX()
        tbl.teleport.destDoor.rotation = { x = x, y = y, z = z }
    end
    return tbl
end
local function serializeObjectLs(objLs)
    local tbl = {}

    for index, value in ipairs(objLs) do

        if value.enabled and value.count > 0 then
            
        table.insert(tbl, serializeObject(value))
        end
    end

    local tableToSave = (tableToString(tbl,nil,nil,true)) 
    --print(tableToSave)
    --I.ZackUtilsG.printToConsole(tableToSave)
    return tableToSave
end
local function serializeCell(cell)
    local cellTable = { [cell.name] = {} }
    for index, value in ipairs(cell:getAll()) do
        if I.AA_CellGen_2_CellCopy.canCopyObject(value) then
            table.insert(cellTable[cell.name], serializeObject(value))
        end
    end

    local tableToSave = (tableToString(cellTable))
    -- print(tableToSave)
    -- I.ZackUtilsG.printToConsole(tableToSave)
    return tableToSave, cellTable
end


return {
    interfaceName = "CellSave",
    interface = {
        serializeObject = serializeObject,
        serializeCell = serializeCell,
        serializeObjectLs = serializeObjectLs,
    },
    eventHandlers = {
        -- createItemReturn_AA = createItemReturn,
        printToConsoleEvent_AA = printToConsoleEvent,
        addItemEquipReturn_AA = addItemEquipReturn,
    },
}
