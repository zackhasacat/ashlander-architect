return {
    [1]={ ["position"]={ ["y"]=-1635.0767822266, ["z"]=-699.38885498047, ["x"]=39.269409179688,}, ["id"]="0x100de81", ["contentFile"]="morrowind.esm", ["recordId"]="active_de_p_bed_03", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=1.570796251297, ["x"]=0,},
    ["refNum"]=56961,},
    [2]={
    ["position"]={
    ["y"]=-1405.2448730469, ["z"]=-699.38885498047, ["x"]=39.269409179688,}, ["id"]="0x100de82", ["contentFile"]="morrowind.esm", ["recordId"]="active_de_p_bed_03", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=1.570796251297, ["x"]=0,},
    ["refNum"]=56962,},
    [3]={
    ["position"]={
    ["y"]=-704.66400146484, ["z"]=-763.10302734375, ["x"]=-655.88000488281,}, ["id"]="0x100de89", ["contentFile"]="morrowind.esm", ["recordId"]="apparatus_a_calcinator_01", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=0, ["x"]=0,}, ["refNum"]=56969,},
    [4]={
    ["position"]={
    ["y"]=-831.55389404297, ["z"]=-692.35302734375, ["x"]=-539.56176757813,}, ["id"]="0x100de8a", ["contentFile"]="morrowind.esm", ["recordId"]="apparatus_m_mortar_01", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=0, ["x"]=0,}, ["refNum"]=56970,},
    [5]={
    ["position"]={
    ["y"]=-738.95202636719, ["z"]=-697.71398925781, ["x"]=-833.77600097656,}, ["id"]="0x100de8b", ["contentFile"]="morrowind.esm", ["recordId"]="apparatus_j_retort_01", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=2.0996310710907, ["x"]=0,},
    ["refNum"]=56971,},
    [6]={
    ["position"]={
    ["y"]=-743.53302001953, ["z"]=-696.96600341797, ["x"]=-781.94897460938,}, ["id"]="0x100de8c", ["contentFile"]="morrowind.esm", ["recordId"]="apparatus_j_alembic_01", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=0, ["x"]=0,}, ["refNum"]=56972,},
    [7]={
    ["position"]={
    ["y"]=13.13852596283, ["z"]=66.478210449219, ["x"]=-561.69274902344,}, ["id"]="0x100a3bf", ["contentFile"]="morrowind.esm", ["recordId"]="bk_originofthemagesguild", ["scale"]=0.65000003576279,
    ["rotation"]={ ["y"]=0.029670597985387,
    ["z"]=-0.50000029802322, ["x"]=0,}, ["refNum"]=41919,},
    [8]={
    ["position"]={
    ["y"]=-292.28332519531, ["z"]=-668.68450927734, ["x"]=244.09155273438,}, ["id"]="0x100de62", ["contentFile"]="morrowind.esm", ["recordId"]="bk_briefhistoryempire1", ["scale"]=1,
    ["rotation"]={ ["y"]=1.570796251297, ["z"]=3.141592502594,
    ["x"]=-1.5707963705063,}, ["refNum"]=56930,},
    [9]={
    ["position"]={
    ["y"]=-292.28332519531, ["z"]=-668.68450927734, ["x"]=250.42904663086,}, ["id"]="0x100de63", ["contentFile"]="morrowind.esm", ["recordId"]="bk_briefhistoryempire2", ["scale"]=1,
    ["rotation"]={ ["y"]=1.570796251297, ["z"]=3.141592502594,
    ["x"]=-1.5707963705063,}, ["refNum"]=56931,},
    [10]={
    ["position"]={
    ["y"]=-292.28332519531, ["z"]=-668.68450927734, ["x"]=255.96737670898,}, ["id"]="0x100de64", ["contentFile"]="morrowind.esm", ["recordId"]="bk_briefhistoryempire3", ["scale"]=1,
    ["rotation"]={ ["y"]=1.570796251297, ["z"]=3.141592502594,
    ["x"]=-1.5707963705063,}, ["refNum"]=56932,},
    [11]={
    ["position"]={
    ["y"]=-292.28298950195, ["z"]=-668.68450927734, ["x"]=262.31298828125,}, ["id"]="0x100de65", ["contentFile"]="morrowind.esm", ["recordId"]="bk_briefhistoryempire4", ["scale"]=1,
    ["rotation"]={ ["y"]=1.570796251297, ["z"]=3.141592502594,
    ["x"]=-1.5707963705063,}, ["refNum"]=56933,},
    [12]={
    ["position"]={
    ["y"]=-879.82885742188, ["z"]=-692.28369140625, ["x"]=-815.15228271484,}, ["id"]="0x100de69", ["contentFile"]="morrowind.esm", ["recordId"]="bk_houseoftroubles_o", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=1.1920928955078e-07, ["x"]=0,},
    ["refNum"]=56937,},
    [13]={
    ["position"]={
    ["y"]=-856.24890136719, ["z"]=-692.84497070313, ["x"]=-534.62573242188,}, ["id"]="0x100de6e", ["contentFile"]="morrowind.esm", ["recordId"]="sc_paper plain", ["scale"]=1.3200000524521, ["rotation"]={ ["y"]=-0, ["z"]=0.31415930390358, ["x"]=0,},
    ["refNum"]=56942,},
    [14]={
    ["position"]={
    ["y"]=-846.24700927734, ["z"]=-692.84503173828, ["x"]=-457.20178222656,}, ["id"]="0x100de6f", ["contentFile"]="morrowind.esm", ["recordId"]="sc_paper plain", ["scale"]=1.7200000286102, ["rotation"]={ ["y"]=-0, ["z"]=-0.48539859056473, ["x"]=0,},
    ["refNum"]=56943,},
    [15]={
    ["position"]={
    ["y"]=-182.40643310547, ["z"]=-251, ["x"]=292.83773803711,}, ["id"]="0x105f5d7", ["contentFile"]="morrowind.esm", ["recordId"]="bk_ajira1", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=0.28539818525314, ["x"]=0,}, ["refNum"]=62935,},
    [16]={
    ["position"]={
    ["y"]=-1485.8801269531, ["z"]=-763, ["x"]=-313.29635620117,}, ["id"]="0x105f5d8", ["contentFile"]="morrowind.esm", ["recordId"]="bk_ajira2", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=2.6004664897919, ["x"]=0,}, ["refNum"]=62936,},
    [17]={
    ["position"]={
    ["y"]=-523.74523925781, ["z"]=-701.78497314453, ["x"]=-59.84135055542,}, ["id"]="0x1076005", ["contentFile"]="morrowind.esm", ["recordId"]="bk_chartermg", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=-2.9231848716736, ["x"]=0,}, ["refNum"]=24581,},
    [18]={
    ["position"]={
    ["y"]=-791.43347167969, ["z"]=-701.78503417969, ["x"]=-223.40309143066,}, ["id"]="0x1076006", ["contentFile"]="morrowind.esm", ["recordId"]="bk_wherewereyoudragonbroke", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=-2.9700000286102, ["x"]=0,},
    ["refNum"]=24582,},
    [19]={
    ["position"]={
    ["y"]=-796.3623046875, ["z"]=-701.78503417969, ["x"]=-159.84773254395,}, ["id"]="0x1076007", ["contentFile"]="morrowind.esm", ["recordId"]="bk_galerionthemystic", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=-3.0599999427795, ["x"]=0,},
    ["refNum"]=24583,},
    [20]={
    ["position"]={
    ["y"]=-795.35345458984, ["z"]=-695.78308105469, ["x"]=-160.88809204102,}, ["id"]="0x1076008", ["contentFile"]="morrowind.esm", ["recordId"]="bk_fragmentonartaeum", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=-3.0300002098083, ["x"]=0,},
    ["refNum"]=24584,},
    [21]={
    ["position"]={
    ["y"]=-578.30285644531, ["z"]=-718.74676513672, ["x"]=-312.99496459961,}, ["id"]="0x1d002a4e", ["contentFile"]="beautiful cities of morrowind.esp", ["recordId"]="bk_fragmentonartaeum", ["scale"]=1,
    ["rotation"]={ ["y"]=-8.7422783678903e-08,
    ["z"]=-3.141592502594, ["x"]=1.570796251297,}, ["refNum"]=10830,},
    [22]={
    ["position"]={
    ["y"]=-540.54241943359, ["z"]=-720.79766845703, ["x"]=-312.85131835938,}, ["id"]="0x1d002a4f", ["contentFile"]="beautiful cities of morrowind.esp", ["recordId"]="bk_galerionthemystic", ["scale"]=1,
    ["rotation"]={ ["y"]=-8.9406967163086e-08,
    ["z"]=-3.0665926933289, ["x"]=0.74579703807831,}, ["refNum"]=10831,},
    [23]={
    ["position"]={
    ["y"]=-447.7678527832, ["z"]=-684.06170654297, ["x"]=-305.19805908203,}, ["id"]="0x1d002a50", ["contentFile"]="beautiful cities of morrowind.esp", ["recordId"]="bk_abcs", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=-1.8000000715256, ["x"]=0,},
    ["refNum"]=10832,},
    [24]={
    ["position"]={
    ["y"]=-324.61260986328, ["z"]=-666.8017578125, ["x"]=-113.07838439941,}, ["id"]="0x1d002a51", ["contentFile"]="beautiful cities of morrowind.esp", ["recordId"]="bk_aedraanddaedra", ["scale"]=1,
    ["rotation"]={ ["y"]=-0, ["z"]=-0.10000068694353,
    ["x"]=0,}, ["refNum"]=10833,},
    [25]={
    ["position"]={
    ["y"]=-393.73651123047, ["z"]=-684.05859375, ["x"]=72.293640136719,}, ["id"]="0x1d002a52", ["contentFile"]="beautiful cities of morrowind.esp", ["recordId"]="t_bk_affairsofwizardstr", ["scale"]=1,
    ["rotation"]={ ["y"]=-0,
    ["z"]=-0.074999630451202, ["x"]=0,}, ["refNum"]=10834,},
    [26]={
    ["position"]={
    ["y"]=-329.69396972656, ["z"]=-670.95904541016, ["x"]=-160.64329528809,}, ["id"]="0x1d002a53", ["contentFile"]="beautiful cities of morrowind.esp", ["recordId"]="bk_annotatedanuad", ["scale"]=1,
    ["rotation"]={ ["y"]=-1.570796251297,
    ["z"]=-1.5707963705063, ["x"]=0,}, ["refNum"]=10835,},
    [27]={
    ["position"]={
    ["y"]=-641.87524414063, ["z"]=-725.90240478516, ["x"]=-312.64974975586,}, ["id"]="0x1d002a54", ["contentFile"]="beautiful cities of morrowind.esp", ["recordId"]="bk_arcturianheresy", ["scale"]=1,
    ["rotation"]={ ["y"]=-0, ["z"]=-2.9999985694885,
    ["x"]=0,}, ["refNum"]=10836,},
    [28]={
    ["position"]={
    ["y"]=-506.62911987305, ["z"]=-672.81231689453, ["x"]=81.215972900391,}, ["id"]="0x1d002a55", ["contentFile"]="beautiful cities of morrowind.esp", ["recordId"]="bk_biographybarenziah3", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=0, ["x"]=0,},
    ["refNum"]=10837,},
    [29]={
    ["position"]={
    ["y"]=-511.61901855469, ["z"]=-725.99475097656, ["x"]=76.959228515625,}, ["id"]="0x1d002a56", ["contentFile"]="beautiful cities of morrowind.esp", ["recordId"]="bk_blackglove", ["scale"]=1,
    ["rotation"]={ ["y"]=-0, ["z"]=0.30000001192093, ["x"]=0,}, ["refNum"]=10838,},
    [30]={
    ["position"]={
    ["y"]=-324.92700195313, ["z"]=-731.80920410156, ["x"]=-136.55885314941,}, ["id"]="0x1d002a57", ["contentFile"]="beautiful cities of morrowind.esp", ["recordId"]="bk_bookdawnanddusk", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=0, ["x"]=0,},
    ["refNum"]=10839,},
    [31]={
    ["position"]={
    ["y"]=-398.92755126953, ["z"]=-613.02069091797, ["x"]=69.578163146973,}, ["id"]="0x1d002a58", ["contentFile"]="beautiful cities of morrowind.esp", ["recordId"]="bk_briefhistoryempire1", ["scale"]=1,
    ["rotation"]={ ["y"]=-0, ["z"]=0,
    ["x"]=-1.570796251297,}, ["refNum"]=10840,},
    [32]={
    ["position"]={
    ["y"]=-478.96353149414, ["z"]=-613.02069091797, ["x"]=76.435546875,}, ["id"]="0x1d002a59", ["contentFile"]="beautiful cities of morrowind.esp", ["recordId"]="bk_briefhistoryempire3", ["scale"]=1,
    ["rotation"]={ ["y"]=-0, ["z"]=0,
    ["x"]=-1.570796251297,}, ["refNum"]=10841,},
    [33]={
    ["position"]={
    ["y"]=-503.28643798828, ["z"]=-613.02069091797, ["x"]=78.49275970459,}, ["id"]="0x1d002a5a", ["contentFile"]="beautiful cities of morrowind.esp", ["recordId"]="bk_briefhistoryempire4", ["scale"]=1,
    ["rotation"]={ ["y"]=-0, ["z"]=0,
    ["x"]=-1.570796251297,}, ["refNum"]=10842,},
    [34]={
    ["position"]={
    ["y"]=-652.73394775391, ["z"]=-732.09130859375, ["x"]=74.408317565918,}, ["id"]="0x1d002a5b", ["contentFile"]="beautiful cities of morrowind.esp", ["recordId"]="bk_brothersofdarkness", ["scale"]=1,
    ["rotation"]={ ["y"]=-0, ["z"]=0.22500002384186,
    ["x"]=0,}, ["refNum"]=10843,},
    [35]={
    ["position"]={
    ["y"]=-323.16296386719, ["z"]=-726.08215332031, ["x"]=-82.57837677002,}, ["id"]="0x1d002a5c", ["contentFile"]="beautiful cities of morrowind.esp", ["recordId"]="bk_cantatasofvivec", ["scale"]=1,
    ["rotation"]={ ["y"]=-0, ["z"]=-0.8249996304512,
    ["x"]=0,}, ["refNum"]=10844,},
    [36]={
    ["position"]={
    ["y"]=-663.05737304688, ["z"]=-620.41888427734, ["x"]=-315.39599609375,}, ["id"]="0x1d002a5d", ["contentFile"]="beautiful cities of morrowind.esp", ["recordId"]="bk_changedones", ["scale"]=1,
    ["rotation"]={ ["y"]=-8.7422783678903e-08,
    ["z"]=-3.141592502594, ["x"]=1.570796251297,}, ["refNum"]=10845,},
    [37]={
    ["position"]={
    ["y"]=-675.75622558594, ["z"]=-620.41888427734, ["x"]=-316.9560546875,}, ["id"]="0x1d002a5e", ["contentFile"]="beautiful cities of morrowind.esp", ["recordId"]="bk_childrenofthesky", ["scale"]=1,
    ["rotation"]={ ["y"]=-8.7422783678903e-08,
    ["z"]=-3.141592502594, ["x"]=1.570796251297,}, ["refNum"]=10846,},
    [38]={
    ["position"]={
    ["y"]=-332.70321655273, ["z"]=-620.66821289063, ["x"]=-11.750341415405,}, ["id"]="0x1d002a5f", ["contentFile"]="beautiful cities of morrowind.esp", ["recordId"]="bk_consolationsofprayer", ["scale"]=1,
    ["rotation"]={ ["y"]=-1.5184382200241,
    ["z"]=-1.162290686807e-06, ["x"]=-1.5707951784134,}, ["refNum"]=10847,},
    [39]={
    ["position"]={
    ["y"]=-399.73559570313, ["z"]=-683.77215576172, ["x"]=-277.52386474609,}, ["id"]="0x1d002a60", ["contentFile"]="beautiful cities of morrowind.esp", ["recordId"]="bk_doorsofthespirit", ["scale"]=1,
    ["rotation"]={ ["y"]=-0, ["z"]=-0.82499819993973,
    ["x"]=0,}, ["refNum"]=10848,},
    [40]={
    ["position"]={
    ["y"]=-509.59051513672, ["z"]=-732.09130859375, ["x"]=72.53734588623,}, ["id"]="0x1d002a61", ["contentFile"]="beautiful cities of morrowind.esp", ["recordId"]="bk_easternprovincesimpartial", ["scale"]=1,
    ["rotation"]={ ["y"]=-0,
    ["z"]=0.30000001192093, ["x"]=0,}, ["refNum"]=10849,},
    [41]={
    ["position"]={
    ["y"]=-402.40029907227, ["z"]=-732.09130859375, ["x"]=61.856506347656,}, ["id"]="0x1d002a62", ["contentFile"]="beautiful cities of morrowind.esp", ["recordId"]="bk_formygodsandemperor", ["scale"]=1,
    ["rotation"]={ ["y"]=-0,
    ["z"]=-0.52499896287918, ["x"]=0,}, ["refNum"]=10850,},
    [42]={
    ["position"]={
    ["y"]=-726.08166503906, ["z"]=-729.15734863281, ["x"]=59.000038146973,}, ["id"]="0x1d002a63", ["contentFile"]="beautiful cities of morrowind.esp", ["recordId"]="bk_great_houses", ["scale"]=1,
    ["rotation"]={ ["y"]=-0, ["z"]=0.4499998986721,
    ["x"]=0,}, ["refNum"]=10851,},
    [43]={
    ["position"]={
    ["y"]=-475.26257324219, ["z"]=-724.27716064453, ["x"]=65.444236755371,}, ["id"]="0x1d002a64", ["contentFile"]="beautiful cities of morrowind.esp", ["recordId"]="bk_guide_to_ald_ruhn", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=1.5, ["x"]=0,},
    ["refNum"]=10852,},
    [44]={
    ["position"]={
    ["y"]=-332.47271728516, ["z"]=-675.05883789063, ["x"]=-62.498252868652,}, ["id"]="0x1d002a65", ["contentFile"]="beautiful cities of morrowind.esp", ["recordId"]="bk_guide_to_sadrithmora", ["scale"]=1,
    ["rotation"]={ ["y"]=-0,
    ["z"]=2.9250004291534, ["x"]=0,}, ["refNum"]=10853,},
    [45]={
    ["position"]={
    ["y"]=-497.1916809082, ["z"]=-669.81988525391, ["x"]=81.363273620605,}, ["id"]="0x1d002a66", ["contentFile"]="beautiful cities of morrowind.esp", ["recordId"]="bk_guide_to_vvardenfell", ["scale"]=1,
    ["rotation"]={ ["y"]=-0, ["z"]=1.5749999284744,
    ["x"]=0,}, ["refNum"]=10854,},
    [46]={
    ["position"]={
    ["y"]=-328.53094482422, ["z"]=-732.09130859375, ["x"]=-82.269470214844,}, ["id"]="0x1d002a67", ["contentFile"]="beautiful cities of morrowind.esp", ["recordId"]="bk_houseoftroubles_c", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=0, ["x"]=0,},
    ["refNum"]=10855,},
    [47]={
    ["position"]={
    ["y"]=-576.35784912109, ["z"]=-682.86151123047, ["x"]=-312.29769897461,}, ["id"]="0x1d002a68", ["contentFile"]="beautiful cities of morrowind.esp", ["recordId"]="bk_homiliesofblessedalmalexia", ["scale"]=1,
    ["rotation"]={ ["y"]=-0,
    ["z"]=0.1499999910593, ["x"]=0,}, ["refNum"]=10856,},
    [48]={
    ["position"]={
    ["y"]=-728.37854003906, ["z"]=-678.81311035156, ["x"]=53.403076171875,}, ["id"]="0x1d002a69", ["contentFile"]="beautiful cities of morrowind.esp", ["recordId"]="bk_mysticism", ["scale"]=1,
    ["rotation"]={ ["y"]=-0, ["z"]=-0.074999630451202,
    ["x"]=0,}, ["refNum"]=10857,},
    [49]={
    ["position"]={
    ["y"]=-683.89147949219, ["z"]=-720.59490966797, ["x"]=-306.66723632813,}, ["id"]="0x1d002a6a", ["contentFile"]="beautiful cities of morrowind.esp", ["recordId"]="bk_overviewofgodsandworship", ["scale"]=1,
    ["rotation"]={
    ["y"]=-8.7422783678903e-08, ["z"]=-3.141592502594, ["x"]=1.570796251297,}, ["refNum"]=10858,},
    [50]={
    ["position"]={
    ["y"]=-619.27282714844, ["z"]=-627.51409912109, ["x"]=-315.22467041016,}, ["id"]="0x1d002a6b", ["contentFile"]="beautiful cities of morrowind.esp", ["recordId"]="bk_pigchildren", ["scale"]=1,
    ["rotation"]={ ["y"]=-0, ["z"]=-3.0749988555908,
    ["x"]=0,}, ["refNum"]=10859,},
    [51]={
    ["position"]={
    ["y"]=-325.2038269043, ["z"]=-609.94567871094, ["x"]=-51.036991119385,}, ["id"]="0x1d002a6c", ["contentFile"]="beautiful cities of morrowind.esp", ["recordId"]="bk_pilgrimspath", ["scale"]=1,
    ["rotation"]={ ["y"]=-1.0506882667542,
    ["z"]=-1.5550884008408, ["x"]=-2.9983780791554e-07,}, ["refNum"]=10860,},
    [52]={
    ["position"]={
    ["y"]=-446.14981079102, ["z"]=-732.09130859375, ["x"]=-301.50595092773,}, ["id"]="0x1d002a6d", ["contentFile"]="beautiful cities of morrowind.esp", ["recordId"]="bk_poisonsong1", ["scale"]=1,
    ["rotation"]={ ["y"]=-0, ["z"]=-2.0249989032745,
    ["x"]=0,}, ["refNum"]=10861,},
    [53]={
    ["position"]={
    ["y"]=-444.00161743164, ["z"]=-728.61944580078, ["x"]=-304.63619995117,}, ["id"]="0x1d002a6e", ["contentFile"]="beautiful cities of morrowind.esp", ["recordId"]="bk_poisonsong2", ["scale"]=1,
    ["rotation"]={ ["y"]=-0, ["z"]=-2.0249989032745,
    ["x"]=0,}, ["refNum"]=10862,},
    [54]={
    ["position"]={
    ["y"]=-542.74523925781, ["z"]=-735.09893798828, ["x"]=81.656829833984,}, ["id"]="0x1d002a6f", ["contentFile"]="beautiful cities of morrowind.esp", ["recordId"]="bk_provinces_of_tamriel", ["scale"]=1,
    ["rotation"]={ ["y"]=-0,
    ["z"]=1.4250001907349, ["x"]=0,}, ["refNum"]=10863,},
    [55]={
    ["position"]={
    ["y"]=-516.08410644531, ["z"]=-618.26818847656, ["x"]=84.85848236084,}, ["id"]="0x1d002a70", ["contentFile"]="beautiful cities of morrowind.esp", ["recordId"]="bk_redbook426", ["scale"]=1,
    ["rotation"]={ ["y"]=-0, ["z"]=0, ["x"]=-1.570796251297,}, ["refNum"]=10864,},
    [56]={
    ["position"]={
    ["y"]=-677.97454833984, ["z"]=-620.66821289063, ["x"]=72.210655212402,}, ["id"]="0x1d002a71", ["contentFile"]="beautiful cities of morrowind.esp", ["recordId"]="bk_spiritofthedaedra", ["scale"]=1,
    ["rotation"]={ ["y"]=-0, ["z"]=0,
    ["x"]=-1.570796251297,}, ["refNum"]=10865,},
    [57]={
    ["position"]={
    ["y"]=-530.73571777344, ["z"]=-668.55902099609, ["x"]=82.478446960449,}, ["id"]="0x1d002a72", ["contentFile"]="beautiful cities of morrowind.esp", ["recordId"]="bk_truenoblescode", ["scale"]=1,
    ["rotation"]={ ["y"]=-0, ["z"]=0,
    ["x"]=-1.570796251297,}, ["refNum"]=10866,},
    [58]={
    ["position"]={
    ["y"]=-761.97601318359, ["z"]=-669.708984375, ["x"]=50.233322143555,}, ["id"]="0x1d002a73", ["contentFile"]="beautiful cities of morrowind.esp", ["recordId"]="bk_vampiresofvvardenfell1", ["scale"]=1,
    ["rotation"]={ ["y"]=0.15000000596046,
    ["z"]=0, ["x"]=-1.9457968473434,}, ["refNum"]=10867,},
    [59]={
    ["position"]={
    ["y"]=-322.6653137207, ["z"]=-670.95904541016, ["x"]=7.4418058395386,}, ["id"]="0x1d002a74", ["contentFile"]="beautiful cities of morrowind.esp", ["recordId"]="bk_vivecandmephala", ["scale"]=1,
    ["rotation"]={ ["y"]=-1.570796251297,
    ["z"]=-1.5707963705063, ["x"]=0,}, ["refNum"]=10868,},
    [60]={
    ["position"]={
    ["y"]=-605.16125488281, ["z"]=-613.02075195313, ["x"]=87.347770690918,}, ["id"]="0x1d002a75", ["contentFile"]="beautiful cities of morrowind.esp", ["recordId"]="bk_watersofoblivion", ["scale"]=1,
    ["rotation"]={ ["y"]=-0.17453321814537,
    ["z"]=-7.4505814851022e-09, ["x"]=1.5707963705063,}, ["refNum"]=10869,},
    [61]={
    ["position"]={
    ["y"]=-608.90112304688, ["z"]=-715.66766357422, ["x"]=80.004089355469,}, ["id"]="0x1d002a76", ["contentFile"]="beautiful cities of morrowind.esp", ["recordId"]="bk_wherewereyoudragonbroke", ["scale"]=1,
    ["rotation"]={ ["y"]=0.17453292012215,
    ["z"]=0, ["x"]=-1.0995577573776,}, ["refNum"]=10870,},
    [62]={
    ["position"]={
    ["y"]=-546.92669677734, ["z"]=-620.66821289063, ["x"]=85.649635314941,}, ["id"]="0x1d002a77", ["contentFile"]="beautiful cities of morrowind.esp", ["recordId"]="bk_yellowbook426", ["scale"]=1,
    ["rotation"]={ ["y"]=-0, ["z"]=0,
    ["x"]=-1.570796251297,}, ["refNum"]=10871,},
    [63]={
    ["position"]={
    ["y"]=-766.63952636719, ["z"]=-718.99609375, ["x"]=45.972217559814,}, ["id"]="0x1d002a78", ["contentFile"]="beautiful cities of morrowind.esp", ["recordId"]="bookskill_alteration1", ["scale"]=1,
    ["rotation"]={ ["y"]=-0, ["z"]=0,
    ["x"]=-1.570796251297,}, ["refNum"]=10872,},
    [64]={
    ["position"]={
    ["y"]=-687.9365234375, ["z"]=-620.41888427734, ["x"]=-316.31707763672,}, ["id"]="0x1d002a79", ["contentFile"]="beautiful cities of morrowind.esp", ["recordId"]="bk_childrenofthesky", ["scale"]=1,
    ["rotation"]={ ["y"]=-8.7422783678903e-08,
    ["z"]=-3.141592502594, ["x"]=1.570796251297,}, ["refNum"]=10873,},
    [65]={
    ["position"]={
    ["y"]=-681.97094726563, ["z"]=-620.41888427734, ["x"]=-316.24035644531,}, ["id"]="0x1d002a7a", ["contentFile"]="beautiful cities of morrowind.esp", ["recordId"]="bk_childrenofthesky", ["scale"]=1,
    ["rotation"]={ ["y"]=-8.7422783678903e-08,
    ["z"]=-3.141592502594, ["x"]=1.570796251297,}, ["refNum"]=10874,},
    [66]={
    ["position"]={
    ["y"]=-669.56628417969, ["z"]=-620.41888427734, ["x"]=-316.25244140625,}, ["id"]="0x1d002a7b", ["contentFile"]="beautiful cities of morrowind.esp", ["recordId"]="bk_changedones", ["scale"]=1,
    ["rotation"]={ ["y"]=-8.7422783678903e-08,
    ["z"]=-3.141592502594, ["x"]=1.570796251297,}, ["refNum"]=10875,},
    [67]={
    ["position"]={
    ["y"]=-688.68768310547, ["z"]=-670.70971679688, ["x"]=-309.14157104492,}, ["id"]="0x1d002a7d", ["contentFile"]="beautiful cities of morrowind.esp", ["recordId"]="bk_chartermg", ["scale"]=1,
    ["rotation"]={ ["y"]=-8.7422783678903e-08,
    ["z"]=-3.141592502594, ["x"]=1.570796251297,}, ["refNum"]=10877,},
    [68]={
    ["position"]={
    ["y"]=-681.74011230469, ["z"]=-670.70971679688, ["x"]=-309.4059753418,}, ["id"]="0x1d002a7e", ["contentFile"]="beautiful cities of morrowind.esp", ["recordId"]="bk_chartermg", ["scale"]=1,
    ["rotation"]={ ["y"]=-8.7422783678903e-08,
    ["z"]=-3.141592502594, ["x"]=1.570796251297,}, ["refNum"]=10878,},
    [69]={
    ["position"]={
    ["y"]=-675.10900878906, ["z"]=-670.70971679688, ["x"]=-310.33889770508,}, ["id"]="0x1d002a7f", ["contentFile"]="beautiful cities of morrowind.esp", ["recordId"]="bk_chartermg", ["scale"]=1,
    ["rotation"]={ ["y"]=-8.7422783678903e-08,
    ["z"]=-3.141592502594, ["x"]=1.570796251297,}, ["refNum"]=10879,},
    [70]={
    ["position"]={
    ["y"]=-668.65209960938, ["z"]=-670.70971679688, ["x"]=-309.8254699707,}, ["id"]="0x1d002a80", ["contentFile"]="beautiful cities of morrowind.esp", ["recordId"]="bk_chartermg", ["scale"]=1,
    ["rotation"]={ ["y"]=-8.7422783678903e-08,
    ["z"]=-3.141592502594, ["x"]=1.570796251297,}, ["refNum"]=10880,},
    [71]={
    ["position"]={
    ["y"]=-662.19006347656, ["z"]=-670.70971679688, ["x"]=-309.45938110352,}, ["id"]="0x1d002a81", ["contentFile"]="beautiful cities of morrowind.esp", ["recordId"]="bk_chartermg", ["scale"]=1,
    ["rotation"]={ ["y"]=-8.7422783678903e-08,
    ["z"]=-3.141592502594, ["x"]=1.570796251297,}, ["refNum"]=10881,},
    [72]={
    ["position"]={
    ["y"]=-453.28210449219, ["z"]=-678.28796386719, ["x"]=-309.85739135742,}, ["id"]="0x1d002a82", ["contentFile"]="beautiful cities of morrowind.esp", ["recordId"]="bk_chartermg", ["scale"]=1,
    ["rotation"]={ ["y"]=-8.7422783678903e-08,
    ["z"]=2.4665927886963, ["x"]=3.141592502594,}, ["refNum"]=10882,},
    [73]={
    ["position"]={
    ["y"]=-635.76635742188, ["z"]=-670.44842529297, ["x"]=-309.34176635742,}, ["id"]="0x1d002a83", ["contentFile"]="beautiful cities of morrowind.esp", ["recordId"]="bk_chartermg", ["scale"]=1,
    ["rotation"]={ ["y"]=-8.7422797889758e-08,
    ["z"]=-3.141592502594, ["x"]=1.4311698675156,}, ["refNum"]=10883,},
    [74]={
    ["position"]={
    ["y"]=-690.17547607422, ["z"]=-720.59490966797, ["x"]=-308.16928100586,}, ["id"]="0x1d002a84", ["contentFile"]="beautiful cities of morrowind.esp", ["recordId"]="bk_overviewofgodsandworship", ["scale"]=1,
    ["rotation"]={
    ["y"]=-8.7422783678903e-08, ["z"]=-3.141592502594, ["x"]=1.570796251297,}, ["refNum"]=10884,},
    [75]={
    ["position"]={
    ["y"]=-643.88787841797, ["z"]=-670.85565185547, ["x"]=-316.66491699219,}, ["id"]="0x1d002a85", ["contentFile"]="beautiful cities of morrowind.esp", ["recordId"]="bk_originofthemagesguild", ["scale"]=1,
    ["rotation"]={ ["y"]=-0,
    ["z"]=3.1331856250763, ["x"]=1.5707964897156,}, ["refNum"]=10885,},
    [76]={
    ["position"]={
    ["y"]=-656.13037109375, ["z"]=-670.85565185547, ["x"]=-317.75634765625,}, ["id"]="0x1d002a86", ["contentFile"]="beautiful cities of morrowind.esp", ["recordId"]="bk_originofthemagesguild", ["scale"]=1,
    ["rotation"]={ ["y"]=-0,
    ["z"]=3.1331856250763, ["x"]=1.5707964897156,}, ["refNum"]=10886,},
    [77]={
    ["position"]={
    ["y"]=-649.98901367188, ["z"]=-670.85565185547, ["x"]=-318.71566772461,}, ["id"]="0x1d002a87", ["contentFile"]="beautiful cities of morrowind.esp", ["recordId"]="bk_originofthemagesguild", ["scale"]=1,
    ["rotation"]={ ["y"]=-0,
    ["z"]=3.1331856250763, ["x"]=1.5707964897156,}, ["refNum"]=10887,},
    [78]={
    ["position"]={
    ["y"]=-677.73785400391, ["z"]=-718.74676513672, ["x"]=-306.68243408203,}, ["id"]="0x1d002a88", ["contentFile"]="beautiful cities of morrowind.esp", ["recordId"]="bk_firmament", ["scale"]=1,
    ["rotation"]={ ["y"]=-8.7422783678903e-08,
    ["z"]=-3.141592502594, ["x"]=1.570796251297,}, ["refNum"]=10888,},
    [79]={
    ["position"]={
    ["y"]=-671.25054931641, ["z"]=-718.74676513672, ["x"]=-307.02835083008,}, ["id"]="0x1d002a89", ["contentFile"]="beautiful cities of morrowind.esp", ["recordId"]="bk_firmament", ["scale"]=1,
    ["rotation"]={ ["y"]=-8.7422783678903e-08,
    ["z"]=-3.141592502594, ["x"]=1.570796251297,}, ["refNum"]=10889,},
    [80]={
    ["position"]={
    ["y"]=-641.83111572266, ["z"]=-731.84197998047, ["x"]=-311.13977050781,}, ["id"]="0x1d002a8a", ["contentFile"]="beautiful cities of morrowind.esp", ["recordId"]="bk_arcturianheresy", ["scale"]=1,
    ["rotation"]={ ["y"]=-0, ["z"]=-2.9999985694885,
    ["x"]=0,}, ["refNum"]=10890,},
    [81]={
    ["position"]={
    ["y"]=-589.79547119141, ["z"]=-718.74676513672, ["x"]=-313.58068847656,}, ["id"]="0x1d002a8c", ["contentFile"]="beautiful cities of morrowind.esp", ["recordId"]="bk_fragmentonartaeum", ["scale"]=1,
    ["rotation"]={ ["y"]=-8.7422783678903e-08,
    ["z"]=-3.141592502594, ["x"]=1.570796251297,}, ["refNum"]=10892,},
    [82]={
    ["position"]={
    ["y"]=-584.03491210938, ["z"]=-718.74676513672, ["x"]=-313.60708618164,}, ["id"]="0x1d002a8d", ["contentFile"]="beautiful cities of morrowind.esp", ["recordId"]="bk_fragmentonartaeum", ["scale"]=1,
    ["rotation"]={ ["y"]=-8.7422783678903e-08,
    ["z"]=-3.141592502594, ["x"]=1.570796251297,}, ["refNum"]=10893,},
    [83]={
    ["position"]={
    ["y"]=-571.83068847656, ["z"]=-716.34674072266, ["x"]=-315.57684326172,}, ["id"]="0x1d002a8e", ["contentFile"]="beautiful cities of morrowind.esp", ["recordId"]="bk_galerionthemystic", ["scale"]=1,
    ["rotation"]={ ["y"]=-8.7422783678903e-08,
    ["z"]=-3.141592502594, ["x"]=1.570796251297,}, ["refNum"]=10894,},
    [84]={
    ["position"]={
    ["y"]=-565.71734619141, ["z"]=-716.34674072266, ["x"]=-314.94805908203,}, ["id"]="0x1d002a8f", ["contentFile"]="beautiful cities of morrowind.esp", ["recordId"]="bk_galerionthemystic", ["scale"]=1,
    ["rotation"]={ ["y"]=-8.7422783678903e-08,
    ["z"]=-3.141592502594, ["x"]=1.570796251297,}, ["refNum"]=10895,},
    [85]={
    ["position"]={
    ["y"]=-559.51654052734, ["z"]=-716.34674072266, ["x"]=-313.54336547852,}, ["id"]="0x1d002a90", ["contentFile"]="beautiful cities of morrowind.esp", ["recordId"]="bk_galerionthemystic", ["scale"]=1,
    ["rotation"]={ ["y"]=-8.7422783678903e-08,
    ["z"]=-3.141592502594, ["x"]=1.570796251297,}, ["refNum"]=10896,},
    [86]={
    ["position"]={
    ["y"]=-619.27282714844, ["z"]=-633.51409912109, ["x"]=-315.22467041016,}, ["id"]="0x1d002a98", ["contentFile"]="beautiful cities of morrowind.esp", ["recordId"]="bk_pigchildren", ["scale"]=1,
    ["rotation"]={ ["y"]=-0, ["z"]=-3.0749988555908,
    ["x"]=0,}, ["refNum"]=10904,},
    [87]={
    ["position"]={
    ["y"]=-380.447265625, ["z"]=-613.02069091797, ["x"]=68.495948791504,}, ["id"]="0x1d002a99", ["contentFile"]="beautiful cities of morrowind.esp", ["recordId"]="bk_briefhistoryempire1", ["scale"]=1,
    ["rotation"]={ ["y"]=-0, ["z"]=0,
    ["x"]=-1.570796251297,}, ["refNum"]=10905,},
    [88]={
    ["position"]={
    ["y"]=-386.77462768555, ["z"]=-613.02069091797, ["x"]=69.229553222656,}, ["id"]="0x1d002a9a", ["contentFile"]="beautiful cities of morrowind.esp", ["recordId"]="bk_briefhistoryempire1", ["scale"]=1,
    ["rotation"]={ ["y"]=-0, ["z"]=0,
    ["x"]=-1.570796251297,}, ["refNum"]=10906,},
    [89]={
    ["position"]={
    ["y"]=-406.79489135742, ["z"]=-612.86206054688, ["x"]=70.4482421875,}, ["id"]="0x1d002a9b", ["contentFile"]="beautiful cities of morrowind.esp", ["recordId"]="bk_briefhistoryempire1", ["scale"]=1,
    ["rotation"]={ ["y"]=-0, ["z"]=0,
    ["x"]=-1.4957964420319,}, ["refNum"]=10907,},
    [90]={
    ["position"]={
    ["y"]=-392.80667114258, ["z"]=-613.02069091797, ["x"]=69.532043457031,}, ["id"]="0x1d002a9c", ["contentFile"]="beautiful cities of morrowind.esp", ["recordId"]="bk_briefhistoryempire1", ["scale"]=1,
    ["rotation"]={ ["y"]=-0, ["z"]=0,
    ["x"]=-1.570796251297,}, ["refNum"]=10908,},
    [91]={
    ["position"]={
    ["y"]=-485.0612487793, ["z"]=-613.02069091797, ["x"]=77.365364074707,}, ["id"]="0x1d002a9e", ["contentFile"]="beautiful cities of morrowind.esp", ["recordId"]="bk_briefhistoryempire4", ["scale"]=1,
    ["rotation"]={ ["y"]=-0, ["z"]=0,
    ["x"]=-1.570796251297,}, ["refNum"]=10910,},
    [92]={
    ["position"]={
    ["y"]=-491.45428466797, ["z"]=-613.02069091797, ["x"]=77.53832244873,}, ["id"]="0x1d002a9f", ["contentFile"]="beautiful cities of morrowind.esp", ["recordId"]="bk_briefhistoryempire4", ["scale"]=1,
    ["rotation"]={ ["y"]=-0, ["z"]=0,
    ["x"]=-1.570796251297,}, ["refNum"]=10911,},
    [93]={
    ["position"]={
    ["y"]=-509.79837036133, ["z"]=-613.02069091797, ["x"]=79.015716552734,}, ["id"]="0x1d002aa0", ["contentFile"]="beautiful cities of morrowind.esp", ["recordId"]="bk_briefhistoryempire4", ["scale"]=1,
    ["rotation"]={ ["y"]=-0, ["z"]=0,
    ["x"]=-1.570796251297,}, ["refNum"]=10912,},
    [94]={
    ["position"]={
    ["y"]=-497.42681884766, ["z"]=-613.02069091797, ["x"]=77.477928161621,}, ["id"]="0x1d002aa1", ["contentFile"]="beautiful cities of morrowind.esp", ["recordId"]="bk_briefhistoryempire4", ["scale"]=1,
    ["rotation"]={ ["y"]=-0, ["z"]=0,
    ["x"]=-1.570796251297,}, ["refNum"]=10913,},
    [95]={
    ["position"]={
    ["y"]=-522.84771728516, ["z"]=-620.66821289063, ["x"]=85.461685180664,}, ["id"]="0x1d002aa2", ["contentFile"]="beautiful cities of morrowind.esp", ["recordId"]="bk_yellowbook426", ["scale"]=1,
    ["rotation"]={ ["y"]=-0, ["z"]=0,
    ["x"]=-1.570796251297,}, ["refNum"]=10914,},
    [96]={
    ["position"]={
    ["y"]=-528.68707275391, ["z"]=-620.66821289063, ["x"]=85.68896484375,}, ["id"]="0x1d002aa3", ["contentFile"]="beautiful cities of morrowind.esp", ["recordId"]="bk_yellowbook426", ["scale"]=1,
    ["rotation"]={ ["y"]=-0, ["z"]=0,
    ["x"]=-1.570796251297,}, ["refNum"]=10915,},
    [97]={
    ["position"]={
    ["y"]=-535.11920166016, ["z"]=-620.66821289063, ["x"]=84.578063964844,}, ["id"]="0x1d002aa4", ["contentFile"]="beautiful cities of morrowind.esp", ["recordId"]="bk_yellowbook426", ["scale"]=1,
    ["rotation"]={ ["y"]=-0, ["z"]=0,
    ["x"]=-1.570796251297,}, ["refNum"]=10916,},
    [98]={
    ["position"]={
    ["y"]=-540.81500244141, ["z"]=-620.66821289063, ["x"]=84.845657348633,}, ["id"]="0x1d002aa5", ["contentFile"]="beautiful cities of morrowind.esp", ["recordId"]="bk_yellowbook426", ["scale"]=1,
    ["rotation"]={ ["y"]=-0, ["z"]=0,
    ["x"]=-1.570796251297,}, ["refNum"]=10917,},
    [99]={
    ["position"]={
    ["y"]=-626.76159667969, ["z"]=-684.05426025391, ["x"]=81.860778808594,}, ["id"]="0x1d002aa6", ["contentFile"]="beautiful cities of morrowind.esp", ["recordId"]="bk_arcanarestored", ["scale"]=1,
    ["rotation"]={ ["y"]=-0, ["z"]=0.22500002384186,
    ["x"]=0,}, ["refNum"]=10918,},
    [100]={
    ["position"]={
    ["y"]=-626.76159667969, ["z"]=-678.81311035156, ["x"]=81.860778808594,}, ["id"]="0x1d002aa7", ["contentFile"]="beautiful cities of morrowind.esp", ["recordId"]="bk_arcanarestored", ["scale"]=1,
    ["rotation"]={ ["y"]=-0, ["z"]=0.22500002384186,
    ["x"]=0,}, ["refNum"]=10919,},
    [101]={
    ["position"]={
    ["y"]=-631.19775390625, ["z"]=-672.8095703125, ["x"]=82.013916015625,}, ["id"]="0x1d002aa8", ["contentFile"]="beautiful cities of morrowind.esp", ["recordId"]="bk_arcanarestored", ["scale"]=1,
    ["rotation"]={ ["y"]=-0, ["z"]=0.59999996423721,
    ["x"]=0,}, ["refNum"]=10920,},
    [102]={
    ["position"]={
    ["y"]=-728.37854003906, ["z"]=-684.05426025391, ["x"]=53.403076171875,}, ["id"]="0x1d002aa9", ["contentFile"]="beautiful cities of morrowind.esp", ["recordId"]="bk_mysticism", ["scale"]=1,
    ["rotation"]={ ["y"]=-0, ["z"]=0.22500002384186, ["x"]=0,}, ["refNum"]=10921,},
    [103]={
    ["position"]={
    ["y"]=-625.70202636719, ["z"]=-633.76776123047, ["x"]=86.643608093262,}, ["id"]="0x1d002aaa", ["contentFile"]="beautiful cities of morrowind.esp", ["recordId"]="bk_specialfloraoftamriel", ["scale"]=1,
    ["rotation"]={ ["y"]=-0,
    ["z"]=0.14999993145466, ["x"]=0,}, ["refNum"]=10922,},
    [104]={
    ["position"]={
    ["y"]=-625.70202636719, ["z"]=-627.76776123047, ["x"]=86.643608093262,}, ["id"]="0x1d002aab", ["contentFile"]="beautiful cities of morrowind.esp", ["recordId"]="bk_specialfloraoftamriel", ["scale"]=1,
    ["rotation"]={ ["y"]=-0,
    ["z"]=0.22499996423721, ["x"]=0,}, ["refNum"]=10923,},
    [105]={
    ["position"]={
    ["y"]=-671.40325927734, ["z"]=-620.66821289063, ["x"]=70.980361938477,}, ["id"]="0x1d002aac", ["contentFile"]="beautiful cities of morrowind.esp", ["recordId"]="bk_spiritofthedaedra", ["scale"]=1,
    ["rotation"]={ ["y"]=-0, ["z"]=0,
    ["x"]=-1.570796251297,}, ["refNum"]=10924,},
    [106]={
    ["position"]={
    ["y"]=-662.86169433594, ["z"]=-668.55902099609, ["x"]=67.577644348145,}, ["id"]="0x1d002aae", ["contentFile"]="beautiful cities of morrowind.esp", ["recordId"]="bk_onoblivion", ["scale"]=1,
    ["rotation"]={ ["y"]=-0, ["z"]=0, ["x"]=-1.570796251297,}, ["refNum"]=10926,},
    [107]={
    ["position"]={
    ["y"]=-668.99078369141, ["z"]=-668.55902099609, ["x"]=67.622177124023,}, ["id"]="0x1d002aaf", ["contentFile"]="beautiful cities of morrowind.esp", ["recordId"]="bk_onoblivion", ["scale"]=1,
    ["rotation"]={ ["y"]=-0, ["z"]=0, ["x"]=-1.570796251297,}, ["refNum"]=10927,},
    [108]={
    ["position"]={
    ["y"]=-675.03350830078, ["z"]=-668.55902099609, ["x"]=66.477516174316,}, ["id"]="0x1d002ab0", ["contentFile"]="beautiful cities of morrowind.esp", ["recordId"]="bk_onoblivion", ["scale"]=1,
    ["rotation"]={ ["y"]=-0, ["z"]=0, ["x"]=-1.570796251297,}, ["refNum"]=10928,},
    [109]={
    ["position"]={
    ["y"]=-394.75369262695, ["z"]=-678.04779052734, ["x"]=70.984870910645,}, ["id"]="0x1d002ab1", ["contentFile"]="beautiful cities of morrowind.esp", ["recordId"]="bk_mixedunittactics", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=0, ["x"]=0,},
    ["refNum"]=10929,},
    [110]={
    ["position"]={
    ["y"]=-462.05508422852, ["z"]=-732.85125732422, ["x"]=69.677131652832,}, ["id"]="0x1d002ab2", ["contentFile"]="beautiful cities of morrowind.esp", ["recordId"]="bk_mysteriousakavir", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=0, ["x"]=0,},
    ["refNum"]=10930,},
    [111]={
    ["position"]={
    ["y"]=-462.05508422852, ["z"]=-726.85125732422, ["x"]=69.677131652832,}, ["id"]="0x1d002ab3", ["contentFile"]="beautiful cities of morrowind.esp", ["recordId"]="bk_mysteriousakavir", ["scale"]=1,
    ["rotation"]={ ["y"]=-0, ["z"]=-2.9831855297089,
    ["x"]=0,}, ["refNum"]=10931,},
    [112]={
    ["position"]={
    ["y"]=-470.78210449219, ["z"]=-675.33306884766, ["x"]=75.305313110352,}, ["id"]="0x1d002ab4", ["contentFile"]="beautiful cities of morrowind.esp", ["recordId"]="bk_frontierconquestaccommodat", ["scale"]=1,
    ["rotation"]={ ["y"]=-0, ["z"]=0,
    ["x"]=-2.6581363677979,}, ["refNum"]=10932,},
    [113]={
    ["position"]={
    ["y"]=-503.94403076172, ["z"]=-684.05426025391, ["x"]=81.632881164551,}, ["id"]="0x1d002ab5", ["contentFile"]="beautiful cities of morrowind.esp", ["recordId"]="bk_biographybarenziah3", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=0, ["x"]=0,},
    ["refNum"]=10933,},
    [114]={
    ["position"]={
    ["y"]=-505.720703125, ["z"]=-678.05426025391, ["x"]=81.291526794434,}, ["id"]="0x1d002ab6", ["contentFile"]="beautiful cities of morrowind.esp", ["recordId"]="bk_biographybarenziah3", ["scale"]=1,
    ["rotation"]={ ["y"]=-0, ["z"]=0.099999994039536,
    ["x"]=0,}, ["refNum"]=10934,},
    [115]={
    ["position"]={
    ["y"]=-544.94128417969, ["z"]=-670.95904541016, ["x"]=82.552261352539,}, ["id"]="0x1d002ab7", ["contentFile"]="beautiful cities of morrowind.esp", ["recordId"]="bk_graspingfortune", ["scale"]=1,
    ["rotation"]={ ["y"]=-0, ["z"]=0,
    ["x"]=-1.4207961559296,}, ["refNum"]=10935,},
    [116]={
    ["position"]={
    ["y"]=-536.82983398438, ["z"]=-670.95904541016, ["x"]=82.896286010742,}, ["id"]="0x1d002ab8", ["contentFile"]="beautiful cities of morrowind.esp", ["recordId"]="bk_graspingfortune", ["scale"]=1,
    ["rotation"]={ ["y"]=-0, ["z"]=0,
    ["x"]=-1.570796251297,}, ["refNum"]=10936,},
    [117]={
    ["position"]={
    ["y"]=-509.87533569336, ["z"]=-723.75103759766, ["x"]=79.643249511719,}, ["id"]="0x1d002ab9", ["contentFile"]="beautiful cities of morrowind.esp", ["recordId"]="bk_guide_to_vvardenfell", ["scale"]=1,
    ["rotation"]={ ["y"]=-0,
    ["z"]=2.1750001907349, ["x"]=0,}, ["refNum"]=10937,},
    [118]={
    ["position"]={
    ["y"]=-512.87182617188, ["z"]=-669.81958007813, ["x"]=80.523109436035,}, ["id"]="0x1d002aba", ["contentFile"]="beautiful cities of morrowind.esp", ["recordId"]="bk_guide_to_vvardenfell", ["scale"]=1,
    ["rotation"]={ ["y"]=-0,
    ["z"]=1.4999998807907, ["x"]=0,}, ["refNum"]=10938,},
    [119]={
    ["position"]={
    ["y"]=-691.31597900391, ["z"]=-735.09893798828, ["x"]=69.169776916504,}, ["id"]="0x1d002abb", ["contentFile"]="beautiful cities of morrowind.esp", ["recordId"]="bk_guide_to_vivec", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=1.5, ["x"]=0,},
    ["refNum"]=10939,},
    [120]={
    ["position"]={
    ["y"]=-664.76202392578, ["z"]=-729.09881591797, ["x"]=73.31859588623,}, ["id"]="0x1d002abc", ["contentFile"]="beautiful cities of morrowind.esp", ["recordId"]="bk_guide_to_vivec", ["scale"]=1,
    ["rotation"]={ ["y"]=-0, ["z"]=1.9500004053116,
    ["x"]=0,}, ["refNum"]=10940,},
    [121]={
    ["position"]={
    ["y"]=-716.62170410156, ["z"]=-735.09893798828, ["x"]=61.398532867432,}, ["id"]="0x1d002abd", ["contentFile"]="beautiful cities of morrowind.esp", ["recordId"]="bk_great_houses", ["scale"]=1,
    ["rotation"]={ ["y"]=-0, ["z"]=1.4999998807907,
    ["x"]=0,}, ["refNum"]=10941,},
    [122]={
    ["position"]={
    ["y"]=-737.49383544922, ["z"]=-735.09893798828, ["x"]=56.864139556885,}, ["id"]="0x1d002abe", ["contentFile"]="beautiful cities of morrowind.esp", ["recordId"]="bk_great_houses", ["scale"]=1,
    ["rotation"]={ ["y"]=-0, ["z"]=1.8000000715256,
    ["x"]=0,}, ["refNum"]=10942,},
    [123]={
    ["position"]={
    ["y"]=-294.38784790039, ["z"]=-689.91772460938, ["x"]=519.20672607422,}, ["id"]="0x1d002abf", ["contentFile"]="beautiful cities of morrowind.esp", ["recordId"]="bk_abcs", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=-1.8749998807907, ["x"]=0,},
    ["refNum"]=10943,},
    [124]={
    ["position"]={
    ["y"]=36.455459594727, ["z"]=-186.4679107666, ["x"]=-551.44659423828,}, ["id"]="0x1d002ac7", ["contentFile"]="beautiful cities of morrowind.esp", ["recordId"]="luce_mio_bmg_recruitbook", ["scale"]=1,
    ["rotation"]={ ["y"]=-0,
    ["z"]=0.67499995231628, ["x"]=0,}, ["refNum"]=10951,},
    [125]={
    ["position"]={
    ["y"]=-326.1708984375, ["z"]=-620.66821289063, ["x"]=-160.57624816895,}, ["id"]="0x1d002adc", ["contentFile"]="beautiful cities of morrowind.esp", ["recordId"]="bk_anticipations", ["scale"]=1,
    ["rotation"]={ ["y"]=-1.570796251297,
    ["z"]=-1.5707963705063, ["x"]=0,}, ["refNum"]=10972,},
    [126]={
    ["position"]={
    ["y"]=-324.10928344727, ["z"]=-620.66821289063, ["x"]=-154.2138671875,}, ["id"]="0x1d002add", ["contentFile"]="beautiful cities of morrowind.esp", ["recordId"]="bk_anticipations", ["scale"]=1,
    ["rotation"]={ ["y"]=-1.570796251297,
    ["z"]=-1.5707963705063, ["x"]=0,}, ["refNum"]=10973,},
    [127]={
    ["position"]={
    ["y"]=-324.2555847168, ["z"]=-620.66821289063, ["x"]=-147.96130371094,}, ["id"]="0x1d002ade", ["contentFile"]="beautiful cities of morrowind.esp", ["recordId"]="bk_anticipations", ["scale"]=1,
    ["rotation"]={ ["y"]=-1.570796251297,
    ["z"]=-1.5707963705063, ["x"]=0,}, ["refNum"]=10974,},
    [128]={
    ["position"]={
    ["y"]=-325.37463378906, ["z"]=-620.66821289063, ["x"]=-141.44226074219,}, ["id"]="0x1d002adf", ["contentFile"]="beautiful cities of morrowind.esp", ["recordId"]="bk_anticipations", ["scale"]=1,
    ["rotation"]={ ["y"]=-1.570796251297,
    ["z"]=-1.5707963705063, ["x"]=0,}, ["refNum"]=10975,},
    [129]={
    ["position"]={
    ["y"]=-326.17541503906, ["z"]=-632.13946533203, ["x"]=-86.150115966797,}, ["id"]="0x1d002ae0", ["contentFile"]="beautiful cities of morrowind.esp", ["recordId"]="bk_pilgrimspath", ["scale"]=1,
    ["rotation"]={ ["y"]=-0, ["z"]=-1.5749992132187,
    ["x"]=0,}, ["refNum"]=10976,},
    [130]={
    ["position"]={
    ["y"]=-407.95761108398, ["z"]=-729.52850341797, ["x"]=71.083465576172,}, ["id"]="0x1d002ae1", ["contentFile"]="beautiful cities of morrowind.esp", ["recordId"]="bk_guide_to_sadrithmora", ["scale"]=1,
    ["rotation"]={ ["y"]=-0,
    ["z"]=1.5749999284744, ["x"]=0,}, ["refNum"]=10977,},
    [131]={
    ["position"]={
    ["y"]=-328.17956542969, ["z"]=-730.46307373047, ["x"]=-24.216041564941,}, ["id"]="0x1d002ae2", ["contentFile"]="beautiful cities of morrowind.esp", ["recordId"]="bk_saryonissermons", ["scale"]=1,
    ["rotation"]={ ["y"]=-0, ["z"]=-1.1999987363815,
    ["x"]=0,}, ["refNum"]=10978,},
    [132]={
    ["position"]={
    ["y"]=-320.62130737305, ["z"]=-670.95904541016, ["x"]=-4.2839670181274,}, ["id"]="0x1d002ae9", ["contentFile"]="beautiful cities of morrowind.esp", ["recordId"]="bk_vivecandmephala", ["scale"]=1,
    ["rotation"]={ ["y"]=-1.570796251297,
    ["z"]=-1.5707963705063, ["x"]=0,}, ["refNum"]=10985,},
    [133]={
    ["position"]={
    ["y"]=-321.26226806641, ["z"]=-670.95904541016, ["x"]=-10.25773525238,}, ["id"]="0x1d002aea", ["contentFile"]="beautiful cities of morrowind.esp", ["recordId"]="bk_vivecandmephala", ["scale"]=1,
    ["rotation"]={ ["y"]=-1.570796251297,
    ["z"]=-1.5707963705063, ["x"]=0,}, ["refNum"]=10986,},
    [134]={
    ["position"]={
    ["y"]=-321.89129638672, ["z"]=-670.95904541016, ["x"]=1.5346397161484,}, ["id"]="0x1d002aeb", ["contentFile"]="beautiful cities of morrowind.esp", ["recordId"]="bk_vivecandmephala", ["scale"]=1,
    ["rotation"]={ ["y"]=-1.570796251297,
    ["z"]=-1.5707963705063, ["x"]=0,}, ["refNum"]=10987,},
    [135]={
    ["position"]={
    ["y"]=-319.99227905273, ["z"]=-670.95904541016, ["x"]=-16.076343536377,}, ["id"]="0x1d002aec", ["contentFile"]="beautiful cities of morrowind.esp", ["recordId"]="bk_vivecandmephala", ["scale"]=1,
    ["rotation"]={ ["y"]=-1.570796251297,
    ["z"]=-1.5707963705063, ["x"]=0,}, ["refNum"]=10988,},
    [136]={
    ["position"]={
    ["y"]=-327.33267211914, ["z"]=-684.05426025391, ["x"]=-58.101753234863,}, ["id"]="0x1d002aed", ["contentFile"]="beautiful cities of morrowind.esp", ["recordId"]="bk_ancestorsandthedunmer", ["scale"]=1,
    ["rotation"]={ ["y"]=-0,
    ["z"]=-1.5749976634979, ["x"]=0,}, ["refNum"]=10989,},
    [137]={
    ["position"]={
    ["y"]=-327.33267211914, ["z"]=-678.05426025391, ["x"]=-58.101753234863,}, ["id"]="0x1d002aee", ["contentFile"]="beautiful cities of morrowind.esp", ["recordId"]="bk_ancestorsandthedunmer", ["scale"]=1,
    ["rotation"]={ ["y"]=-0,
    ["z"]=-1.5749976634979, ["x"]=0,}, ["refNum"]=10990,},
    [138]={
    ["position"]={
    ["y"]=-331.37344360352, ["z"]=-670.95904541016, ["x"]=-147.7882232666,}, ["id"]="0x1d002aef", ["contentFile"]="beautiful cities of morrowind.esp", ["recordId"]="bk_annotatedanuad", ["scale"]=1,
    ["rotation"]={ ["y"]=-1.570796251297,
    ["z"]=-1.5707963705063, ["x"]=0,}, ["refNum"]=10991,},
    [139]={
    ["position"]={
    ["y"]=-330.50668334961, ["z"]=-670.95904541016, ["x"]=-154.4270324707,}, ["id"]="0x1d002af0", ["contentFile"]="beautiful cities of morrowind.esp", ["recordId"]="bk_annotatedanuad", ["scale"]=1,
    ["rotation"]={ ["y"]=-1.570796251297,
    ["z"]=-1.5707963705063, ["x"]=0,}, ["refNum"]=10992,},
    [140]={
    ["position"]={
    ["y"]=-332.36294555664, ["z"]=-620.66821289063, ["x"]=7.6325116157532,}, ["id"]="0x1d002af5", ["contentFile"]="beautiful cities of morrowind.esp", ["recordId"]="bk_consolationsofprayer", ["scale"]=1,
    ["rotation"]={ ["y"]=-1.570796251297,
    ["z"]=-1.5707963705063, ["x"]=0,}, ["refNum"]=10997,},
    [141]={
    ["position"]={
    ["y"]=-333.01312255859, ["z"]=-620.66821289063, ["x"]=1.2941174507141,}, ["id"]="0x1d002af6", ["contentFile"]="beautiful cities of morrowind.esp", ["recordId"]="bk_consolationsofprayer", ["scale"]=1,
    ["rotation"]={ ["y"]=-1.570796251297,
    ["z"]=-1.5707963705063, ["x"]=0,}, ["refNum"]=10998,},
    [142]={
    ["position"]={
    ["y"]=-332.81729125977, ["z"]=-620.66821289063, ["x"]=-5.5610165596008,}, ["id"]="0x1d002af7", ["contentFile"]="beautiful cities of morrowind.esp", ["recordId"]="bk_consolationsofprayer", ["scale"]=1,
    ["rotation"]={ ["y"]=-1.5184382200241,
    ["z"]=-1.162290686807e-06, ["x"]=-1.5707951784134,}, ["refNum"]=10999,},
    [143]={
    ["position"]={
    ["y"]=-326.17541503906, ["z"]=-615.14471435547, ["x"]=-86.150115966797,}, ["id"]="0x1d002af8", ["contentFile"]="beautiful cities of morrowind.esp", ["recordId"]="bk_pilgrimspath", ["scale"]=1,
    ["rotation"]={ ["y"]=-0, ["z"]=-1.5749992132187,
    ["x"]=0,}, ["refNum"]=11000,},
    [144]={
    ["position"]={
    ["y"]=-326.17541503906, ["z"]=-623.64208984375, ["x"]=-86.150115966797,}, ["id"]="0x1d002af9", ["contentFile"]="beautiful cities of morrowind.esp", ["recordId"]="bk_pilgrimspath", ["scale"]=1,
    ["rotation"]={ ["y"]=-0, ["z"]=-1.5749992132187,
    ["x"]=0,}, ["refNum"]=11001,},
    [145]={
    ["position"]={
    ["y"]=-324.61260986328, ["z"]=-678.04333496094, ["x"]=-113.07838439941,}, ["id"]="0x1d002afa", ["contentFile"]="beautiful cities of morrowind.esp", ["recordId"]="bk_aedraanddaedra", ["scale"]=1,
    ["rotation"]={ ["y"]=-0, ["z"]=-0.32500007748604,
    ["x"]=0,}, ["refNum"]=11002,},
    [146]={
    ["position"]={
    ["y"]=-324.61260986328, ["z"]=-672.8017578125, ["x"]=-113.07838439941,}, ["id"]="0x1d002afb", ["contentFile"]="beautiful cities of morrowind.esp", ["recordId"]="bk_aedraanddaedra", ["scale"]=1,
    ["rotation"]={ ["y"]=-0, ["z"]=-0.10000068694353,
    ["x"]=0,}, ["refNum"]=11003,},
    [147]={
    ["position"]={
    ["y"]=-328.15603637695, ["z"]=-684.05426025391, ["x"]=-114.11734008789,}, ["id"]="0x1d002afc", ["contentFile"]="beautiful cities of morrowind.esp", ["recordId"]="bk_cantatasofvivec", ["scale"]=1,
    ["rotation"]={ ["y"]=-0, ["z"]=-0.32500007748604,
    ["x"]=0,}, ["refNum"]=11004,},
    [148]={
    ["position"]={
    ["y"]=-451.30368041992, ["z"]=-723.84411621094, ["x"]=67.879539489746,}, ["id"]="0x1d002afd", ["contentFile"]="beautiful cities of morrowind.esp", ["recordId"]="bk_guide_to_sadrithmora", ["scale"]=1,
    ["rotation"]={ ["y"]=-0,
    ["z"]=1.9499999284744, ["x"]=0,}, ["refNum"]=11005,},
    [149]={
    ["position"]={
    ["y"]=-364.45025634766, ["z"]=-732.09130859375, ["x"]=-243.31080627441,}, ["id"]="0x1d002b04", ["contentFile"]="beautiful cities of morrowind.esp", ["recordId"]="bk_poisonsong6", ["scale"]=1,
    ["rotation"]={ ["y"]=-0, ["z"]=-0.97499883174896,
    ["x"]=0,}, ["refNum"]=11012,},
    [150]={
    ["position"]={
    ["y"]=-365.60699462891, ["z"]=-726.09307861328, ["x"]=-239.85572814941,}, ["id"]="0x1d002b05", ["contentFile"]="beautiful cities of morrowind.esp", ["recordId"]="bookskill_destruction5", ["scale"]=1,
    ["rotation"]={ ["y"]=-0,
    ["z"]=0.37499997019768, ["x"]=0,}, ["refNum"]=11013,},
    [151]={
    ["position"]={
    ["y"]=-337.43463134766, ["z"]=-613.26300048828, ["x"]=-215.13095092773,}, ["id"]="0x1d002b06", ["contentFile"]="beautiful cities of morrowind.esp", ["recordId"]="bk_mysteriousakavir", ["scale"]=1,
    ["rotation"]={ ["y"]=0.65973448753357,
    ["z"]=0.27227133512497, ["x"]=-1.1249997615814,}, ["refNum"]=11014,},
    [152]={
    ["position"]={
    ["y"]=-442.13897705078, ["z"]=-627.76318359375, ["x"]=-296.35604858398,}, ["id"]="0x1d002b07", ["contentFile"]="beautiful cities of morrowind.esp", ["recordId"]="bk_fivesongsofkingwulfharth", ["scale"]=1,
    ["rotation"]={ ["y"]=-0,
    ["z"]=-2.4749991893768, ["x"]=0,}, ["refNum"]=11015,},
    [153]={
    ["position"]={
    ["y"]=-443.14169311523, ["z"]=-633.76342773438, ["x"]=-297.73400878906,}, ["id"]="0x1d002b08", ["contentFile"]="beautiful cities of morrowind.esp", ["recordId"]="bk_fivesongsofkingwulfharth", ["scale"]=1,
    ["rotation"]={ ["y"]=-0,
    ["z"]=-2.4749991893768, ["x"]=0,}, ["refNum"]=11016,},
    [154]={
    ["position"]={
    ["y"]=-359.79663085938, ["z"]=-627.76342773438, ["x"]=-241.39122009277,}, ["id"]="0x1d002b09", ["contentFile"]="beautiful cities of morrowind.esp", ["recordId"]="bk_realbarenziah2", ["scale"]=1,
    ["rotation"]={ ["y"]=-0, ["z"]=-1.274999499321,
    ["x"]=0,}, ["refNum"]=11017,},
    [155]={
    ["position"]={
    ["y"]=-359.79663085938, ["z"]=-633.76342773438, ["x"]=-241.39122009277,}, ["id"]="0x1d002b0a", ["contentFile"]="beautiful cities of morrowind.esp", ["recordId"]="bk_realbarenziah2", ["scale"]=1,
    ["rotation"]={ ["y"]=-0, ["z"]=-1.274999499321,
    ["x"]=0,}, ["refNum"]=11018,},
    [156]={
    ["position"]={
    ["y"]=-373.44039916992, ["z"]=-619.58465576172, ["x"]=-250.1902923584,}, ["id"]="0x1d002b0b", ["contentFile"]="beautiful cities of morrowind.esp", ["recordId"]="bk_realbarenziah4", ["scale"]=1,
    ["rotation"]={ ["y"]=-0, ["z"]=9.5367443009309e-07,
    ["x"]=-0.67499858140945,}, ["refNum"]=11019,},
    [157]={
    ["position"]={
    ["y"]=-354.04327392578, ["z"]=-621.75201416016, ["x"]=-244.66511535645,}, ["id"]="0x1d002b0c", ["contentFile"]="beautiful cities of morrowind.esp", ["recordId"]="bk_realbarenziah5", ["scale"]=1,
    ["rotation"]={ ["y"]=-0, ["z"]=-1.5749992132187,
    ["x"]=0,}, ["refNum"]=11020,},
    [158]={
    ["position"]={
    ["y"]=-672, ["z"]=-784, ["x"]=-16,}, ["id"]="0x1072f30", ["contentFile"]="morrowind.esm", ["recordId"]="common_ring_01_mgbwg", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=0, ["x"]=0,}, ["refNum"]=12080,},
    [159]={
    ["position"]={
    ["y"]=-1036, ["z"]=-775, ["x"]=-130,}, ["id"]="0x105f7d4", ["contentFile"]="morrowind.esm", ["recordId"]="common_ring_01_mge", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=0, ["x"]=0,}, ["refNum"]=63444,},
    [160]={
    ["position"]={
    ["y"]=37.137012481689, ["z"]=34.905181884766, ["x"]=-520.04107666016,}, ["id"]="0x105f5d5", ["contentFile"]="morrowind.esm", ["rotation"]={ ["y"]=-0, ["z"]=-0.017453495413065, ["x"]=0,}, ["recordId"]="de_p_desk_01_galbedir", ["scale"]=1,
    ["refNum"]=62933, ["inventory"]={ [1]={ ["recordId"]="misc_soulgem_lesser", ["count"]=1,},},},
    [161]={
    ["position"]={
    ["y"]=-287.81405639648, ["z"]=-217.62899780273, ["x"]=319.64706420898,}, ["id"]="0x100de10", ["contentFile"]="morrowind.esm", ["rotation"]={ ["y"]=-0, ["z"]=0, ["x"]=0,}, ["recordId"]="barrel_01_cheapfood5", ["scale"]=1, ["refNum"]=56848,
    ["inventory"]={},},
    [162]={
    ["position"]={
    ["y"]=-316.99667358398, ["z"]=-186.98626708984, ["x"]=262.34381103516,}, ["id"]="0x100de11", ["contentFile"]="morrowind.esm", ["rotation"]={ ["y"]=-0, ["z"]=0.35000002384186, ["x"]=0,}, ["recordId"]="barrel_02_ingred", ["scale"]=1,
    ["refNum"]=56849, ["inventory"]={},},
    [163]={
    ["position"]={
    ["y"]=-186.93835449219, ["z"]=-215.74584960938, ["x"]=316.18359375,}, ["id"]="0x100de12", ["contentFile"]="morrowind.esm", ["rotation"]={ ["y"]=-0, ["z"]=-2.1999976634979, ["x"]=0,}, ["recordId"]="com_basket_01_ingredien", ["scale"]=1,
    ["refNum"]=56850, ["inventory"]={},},
    [164]={
    ["position"]={
    ["y"]=-189.6604309082, ["z"]=-209.29034423828, ["x"]=259.57302856445,}, ["id"]="0x100de14", ["contentFile"]="morrowind.esm", ["rotation"]={ ["y"]=-0, ["z"]=0.40000000596046, ["x"]=0,}, ["recordId"]="com_basket_01_food", ["scale"]=1.1600000858307,
    ["refNum"]=56852, ["inventory"]={},},
    [165]={
    ["position"]={
    ["y"]=-239.359375, ["z"]=-244.23092651367, ["x"]=296.61016845703,}, ["id"]="0x100de15", ["contentFile"]="morrowind.esm", ["rotation"]={ ["y"]=-0, ["z"]=-0.29999953508377, ["x"]=0,}, ["recordId"]="com_sack_01_saltrice_10",
    ["scale"]=1.3600000143051, ["refNum"]=56853, ["inventory"]={ [1]={ ["recordId"]="ingred_saltrice_01", ["count"]=10,},},},
    [166]={
    ["position"]={
    ["y"]=-186.55337524414, ["z"]=-228.72099304199, ["x"]=216.89109802246,}, ["id"]="0x100de16", ["contentFile"]="morrowind.esm", ["rotation"]={ ["y"]=-0, ["z"]=0.54977869987488, ["x"]=0,}, ["recordId"]="com_sack_02_chpfood3", ["scale"]=1,
    ["refNum"]=56854, ["inventory"]={},},
    [167]={
    ["position"]={
    ["y"]=-119.4951171875, ["z"]=-109.5425567627, ["x"]=-296.38671875,}, ["id"]="0x100de1b", ["contentFile"]="morrowind.esm", ["rotation"]={ ["y"]=-0, ["z"]=-3.141592502594, ["x"]=0,}, ["recordId"]="chest_small_01_ingredie", ["scale"]=1,
    ["refNum"]=56859, ["inventory"]={},},
    [168]={
    ["position"]={
    ["y"]=-859.22589111328, ["z"]=-712.99011230469, ["x"]=-820.83123779297,}, ["id"]="0x100de1d", ["contentFile"]="morrowind.esm", ["rotation"]={ ["y"]=-0, ["z"]=-1.5707957744598, ["x"]=0,}, ["recordId"]="de_p_desk_01_masalinie ", ["scale"]=1,
    ["refNum"]=56861, ["inventory"]={},},
    [169]={
    ["position"]={
    ["y"]=-848.88214111328, ["z"]=-710.39727783203, ["x"]=-489.59677124023,}, ["id"]="0x100de1e", ["contentFile"]="morrowind.esm", ["rotation"]={ ["y"]=-0, ["z"]=-3.141592502594, ["x"]=0,}, ["recordId"]="de_p_desk_01_ajira", ["scale"]=1,
    ["refNum"]=56862, ["inventory"]={},},
    [170]={
    ["position"]={
    ["y"]=-730.4716796875, ["z"]=-739.72210693359, ["x"]=-775.79187011719,}, ["id"]="0x100de22", ["contentFile"]="morrowind.esm", ["rotation"]={ ["y"]=-0, ["z"]=-1.0707968473434, ["x"]=0,}, ["recordId"]="com_sack_02_ingred", ["scale"]=1,
    ["refNum"]=56866, ["inventory"]={},},
    [171]={
    ["position"]={
    ["y"]=-728.72406005859, ["z"]=-736.92529296875, ["x"]=-819.42065429688,}, ["id"]="0x100de23", ["contentFile"]="morrowind.esm", ["rotation"]={ ["y"]=-0, ["z"]=0, ["x"]=0,}, ["recordId"]="com_sack_02_ingred", ["scale"]=1, ["refNum"]=56867,
    ["inventory"]={},},
    [172]={
    ["position"]={
    ["y"]=-732.07000732422, ["z"]=-743.86499023438, ["x"]=-729.88897705078,}, ["id"]="0x100de24", ["contentFile"]="morrowind.esm", ["rotation"]={ ["y"]=-0, ["z"]=-1.3002704381943, ["x"]=0,}, ["recordId"]="com_sack_01_ingred", ["scale"]=1,
    ["refNum"]=56868, ["inventory"]={},},
    [173]={
    ["position"]={
    ["y"]=-729.47473144531, ["z"]=-756.12127685547, ["x"]=-732.4296875,}, ["id"]="0x100de25", ["contentFile"]="morrowind.esm", ["rotation"]={ ["y"]=-0, ["z"]=-1.3999985456467, ["x"]=0,}, ["recordId"]="com_sack_01_ingred", ["scale"]=1,
    ["refNum"]=56869, ["inventory"]={},},
    [174]={
    ["position"]={
    ["y"]=-1403.9289550781, ["z"]=-683.69598388672, ["x"]=-304.97198486328,}, ["id"]="0x100de88", ["contentFile"]="morrowind.esm", ["rotation"]={ ["y"]=-0, ["z"]=-1.570796251297, ["x"]=0,}, ["recordId"]="de_p_closet_02_mguild", ["scale"]=1,
    ["refNum"]=56968, ["inventory"]={ [1]={ ["recordId"]="common_robe_03", ["count"]=1,}, [2]={ ["recordId"]="common_robe_05", ["count"]=1,},},},
    [175]={
    ["position"]={
    ["y"]=-1351.2390136719, ["z"]=-747.00299072266, ["x"]=571.75701904297,}, ["id"]="0x1051116", ["contentFile"]="morrowind.esm", ["rotation"]={ ["y"]=-0, ["z"]=-1.4992378950119, ["x"]=0,}, ["recordId"]="chest_01_v_potion_h_03", ["scale"]=1,
    ["refNum"]=4374,
    ["inventory"]={ [1]={ ["recordId"]="p_fortify_health_c", ["count"]=2,}, [2]={ ["recordId"]="p_disease_resistance_c", ["count"]=2,}, [3]={ ["recordId"]="p_restore_fatigue_c", ["count"]=2,}, [4]={ ["recordId"]="p_restore_health_c", ["count"]=5,},
    [5]={
    ["recordId"]="p_restore_magicka_c", ["count"]=1,}, [6]={ ["recordId"]="p_fortify_health_b", ["count"]=2,}, [7]={ ["recordId"]="p_disease_resistance_b", ["count"]=2,}, [8]={ ["recordId"]="p_restore_fatigue_b", ["count"]=3,},
    [9]={
    ["recordId"]="p_restore_health_b", ["count"]=3,}, [10]={ ["recordId"]="p_restore_magicka_b", ["count"]=3,}, [11]={ ["recordId"]="p_cure_common_s", ["count"]=10,}, [12]={ ["recordId"]="p_cure_paralyzation_s", ["count"]=1,},
    [13]={
    ["recordId"]="p_cure_poison_s", ["count"]=1,}, [14]={ ["recordId"]="p_disease_resistance_s", ["count"]=2,}, [15]={ ["recordId"]="p_disease_resistance_q", ["count"]=2,}, [16]={ ["recordId"]="p_cure_blight_s", ["count"]=5,},
    [17]={
    ["recordId"]="p_restore_health_s", ["count"]=5,}, [18]={ ["recordId"]="p_restore_health_q", ["count"]=2,}, [19]={ ["recordId"]="p_restore_agility_s", ["count"]=3,}, [20]={ ["recordId"]="p_restore_endurance_s", ["count"]=3,},
    [21]={
    ["recordId"]="p_restore_intelligence_s", ["count"]=3,}, [22]={ ["recordId"]="p_restore_luck_s", ["count"]=3,}, [23]={ ["recordId"]="p_restore_personality_s", ["count"]=3,}, [24]={ ["recordId"]="p_restore_speed_s", ["count"]=3,},
    [25]={
    ["recordId"]="p_restore_strength_s", ["count"]=3,}, [26]={ ["recordId"]="p_restore_willpower_s", ["count"]=3,},},},
    [176]={
    ["position"]={
    ["y"]=-718.76501464844, ["z"]=-747.0830078125, ["x"]=-444.33700561523,}, ["id"]="0x1051115", ["contentFile"]="morrowind.esm", ["rotation"]={ ["y"]=-0, ["z"]=-1.600466966629, ["x"]=0,}, ["recordId"]="chest_01_v_potion_al_02", ["scale"]=1,
    ["refNum"]=4373,
    ["inventory"]={ [1]={ ["recordId"]="p_invisibility_s", ["count"]=1,}, [2]={ ["recordId"]="p_levitation_s", ["count"]=1,}, [3]={ ["recordId"]="p_water_walking_s", ["count"]=2,}, [4]={ ["recordId"]="p_water_breathing_s", ["count"]=3,},
    [5]={
    ["recordId"]="p_lightning shield_s", ["count"]=1,}, [6]={ ["recordId"]="p_swift_swim_q", ["count"]=1,}, [7]={ ["recordId"]="p_fire_shield_s", ["count"]=1,}, [8]={ ["recordId"]="p_restore_willpower_s", ["count"]=1,},
    [9]={
    ["recordId"]="p_restore_strength_s", ["count"]=1,}, [10]={ ["recordId"]="p_restore_magicka_s", ["count"]=1,}, [11]={ ["recordId"]="p_restore_speed_s", ["count"]=1,}, [12]={ ["recordId"]="p_restore_personality_s", ["count"]=1,},
    [13]={
    ["recordId"]="p_restore_luck_s", ["count"]=1,}, [14]={ ["recordId"]="p_restore_intelligence_s", ["count"]=1,}, [15]={ ["recordId"]="p_restore_fatigue_s", ["count"]=3,}, [16]={ ["recordId"]="p_restore_endurance_s", ["count"]=1,},
    [17]={
    ["recordId"]="p_restore_agility_s", ["count"]=1,}, [18]={ ["recordId"]="p_night-eye_s", ["count"]=3,}, [19]={ ["recordId"]="p_reflection_s", ["count"]=2,}, [20]={ ["recordId"]="p_poison_resistance_s", ["count"]=3,},
    [21]={
    ["recordId"]="p_disease_resistance_s", ["count"]=3,}, [22]={ ["recordId"]="p_disease_resistance_b", ["count"]=3,}, [23]={ ["recordId"]="p_light_b", ["count"]=1,}, [24]={ ["recordId"]="p_fortify_magicka_s", ["count"]=1,},
    [25]={
    ["recordId"]="p_fortify_health_s", ["count"]=1,}, [26]={ ["recordId"]="p_fortify_fatigue_s", ["count"]=1,}, [27]={ ["recordId"]="p_cure_poison_s", ["count"]=1,}, [28]={ ["recordId"]="p_cure_paralyzation_s", ["count"]=1,},
    [29]={
    ["recordId"]="p_cure_common_s", ["count"]=2,},},},
    [177]={
    ["position"]={
    ["y"]=-1429.0479736328, ["z"]=-740.38702392578, ["x"]=570.22497558594,}, ["id"]="0x1015317", ["contentFile"]="morrowind.esm", ["rotation"]={ ["y"]=-0, ["z"]=1.570796251297, ["x"]=0,}, ["recordId"]="de_r_chest_01_sharn", ["scale"]=1,
    ["refNum"]=21271,
    ["inventory"]={ [1]={ ["recordId"]="bk_ancestorsandthedunmer", ["count"]=1,}, [2]={ ["recordId"]="bk_darkestdarkness", ["count"]=1,}, [3]={ ["recordId"]="bk_vampiresofvvardenfell1", ["count"]=1,},
    [4]={
    ["recordId"]="bk_sharnslegionsofthedead", ["count"]=1,},},},
    [178]={
    ["position"]={
    ["y"]=-103.09822845459, ["z"]=-225.90673828125, ["x"]=-254.39503479004,}, ["id"]="0x1070562", ["contentFile"]="morrowind.esm", ["rotation"]={ ["y"]=-0, ["z"]=-3.141592502594, ["x"]=0,}, ["recordId"]="com_chest_02_mg_supply",
    ["scale"]=1.1900000572205, ["refNum"]=1378,
    ["inventory"]={ [1]={ ["recordId"]="p_restore_magicka_s", ["count"]=10,}, [2]={ ["recordId"]="p_fortify_willpower_s", ["count"]=5,}, [3]={ ["recordId"]="sc_almsiviintervention", ["count"]=1,},
    [4]={
    ["recordId"]="sc_divineintervention", ["count"]=1,}, [5]={ ["recordId"]="sc_daydenespanacea", ["count"]=3,}, [6]={ ["recordId"]="misc_soulgem_lesser", ["count"]=3,},},},
    [179]={
    ["position"]={
    ["y"]=66.061195373535, ["z"]=26.912477493286, ["x"]=-408.61517333984,}, ["id"]="0x1074983", ["contentFile"]="morrowind.esm", ["rotation"]={ ["y"]=-0, ["z"]=0, ["x"]=0,}, ["recordId"]="com_chest_02_galbedir", ["scale"]=1, ["refNum"]=18819,
    ["inventory"]={},},
    [180]={
    ["position"]={
    ["y"]=0, ["z"]=128, ["x"]=-128,}, ["id"]="0x100de0f", ["contentFile"]="morrowind.esm",
    ["teleport"]={ ["position"]={ ["y"]=-14564.056640625, ["z"]=934.44451904297, ["x"]=-21604.548828125,},
    ["rotation"]={ ["y"]=-0, ["z"]=-3.141592502594,
    ["x"]=0,}, ["cell"]={ ["name"]="Balmora",},
    ["destDoor"]={ ["position"]={ ["y"]=8.2170000076294, ["z"]=97.337997436523, ["x"]=-253.5,}, ["id"]="0x1008027",
    ["rotation"]={ ["y"]=-0, ["z"]=-1.570796251297, ["x"]=0,},},},
    ["recordId"]="in_hlaalu_loaddoor_01", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=0, ["x"]=0,}, ["refNum"]=56847,},
    [181]={
    ["position"]={
    ["y"]=-384, ["z"]=128, ["x"]=-512,}, ["id"]="0x100a3b8", ["contentFile"]="morrowind.esm",
    ["teleport"]={ ["position"]={ ["y"]=-14051.29296875, ["z"]=894.87341308594, ["x"]=-22046.8515625,},
    ["rotation"]={ ["y"]=-0, ["z"]=-1.570796251297,
    ["x"]=0,}, ["cell"]={ ["name"]="Balmora",},
    ["destDoor"]={ ["position"]={ ["y"]=-241.44999694824, ["z"]=109.44799804688, ["x"]=-528.11999511719,}, ["id"]="0x100802b", ["rotation"]={ ["y"]=-0, ["z"]=0, ["x"]=0,},},},
    ["recordId"]="in_hlaalu_loaddoor_01", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=1.570796251297, ["x"]=0,}, ["refNum"]=41912,},
    [182]={
    ["position"]={
    ["y"]=-384, ["z"]=-128, ["x"]=-512,}, ["id"]="0x100a3b6", ["contentFile"]="morrowind.esm",
    ["teleport"]={ ["position"]={ ["y"]=-14049.249023438, ["z"]=588.82458496094, ["x"]=-22122.521484375,},
    ["rotation"]={ ["y"]=-0, ["z"]=-1.570796251297,
    ["x"]=0,}, ["cell"]={ ["name"]="Balmora",},
    ["destDoor"]={ ["position"]={ ["y"]=-241.44999694824, ["z"]=109.44799804688, ["x"]=-528.11999511719,}, ["id"]="0x100802b", ["rotation"]={ ["y"]=-0, ["z"]=0, ["x"]=0,},},},
    ["recordId"]="in_hlaalu_loaddoor_01", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=1.570796251297, ["x"]=0,}, ["refNum"]=41910,},
    [183]={
    ["position"]={
    ["y"]=-710.30230712891, ["z"]=-691.89801025391, ["x"]=-711.03881835938,}, ["id"]="0x100de2a", ["contentFile"]="morrowind.esm", ["recordId"]="ingred_bonemeal_01", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=0, ["x"]=0,}, ["refNum"]=56874,},
    [184]={
    ["position"]={
    ["y"]=-713.45727539063, ["z"]=-691.82733154297, ["x"]=-730.35021972656,}, ["id"]="0x100de2b", ["contentFile"]="morrowind.esm", ["recordId"]="ingred_crab_meat_01", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=0, ["x"]=0,}, ["refNum"]=56875,},
    [185]={
    ["position"]={
    ["y"]=-730.69915771484, ["z"]=-691.09912109375, ["x"]=-714.45220947266,}, ["id"]="0x100de2d", ["contentFile"]="morrowind.esm", ["recordId"]="ingred_emerald_01", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=0, ["x"]=0,}, ["refNum"]=56877,},
    [186]={
    ["position"]={
    ["y"]=-738.68463134766, ["z"]=-691.96978759766, ["x"]=-726.83111572266,}, ["id"]="0x100de2e", ["contentFile"]="morrowind.esm", ["recordId"]="ingred_green_lichen_01", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=0, ["x"]=0,}, ["refNum"]=56878,},
    [187]={
    ["position"]={
    ["y"]=-747.71942138672, ["z"]=-690.88708496094, ["x"]=-712.14862060547,}, ["id"]="0x100de2f", ["contentFile"]="morrowind.esm", ["recordId"]="ingred_saltrice_01", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=0.70000004768372, ["x"]=0,},
    ["refNum"]=56879,},
    [188]={
    ["position"]={
    ["y"]=-834.82092285156, ["z"]=-691.53900146484, ["x"]=-515.27575683594,}, ["id"]="0x100de6c", ["contentFile"]="morrowind.esm", ["recordId"]="ingred_saltrice_01", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=0, ["x"]=0,}, ["refNum"]=56940,},
    [189]={
    ["position"]={
    ["y"]=-853.59490966797, ["z"]=-693.32702636719, ["x"]=-501.47882080078,}, ["id"]="0x100de6d", ["contentFile"]="morrowind.esm", ["recordId"]="ingred_scrap_metal_01", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=0, ["x"]=0,}, ["refNum"]=56941,},
    [190]={
    ["position"]={
    ["y"]=44.241870880127, ["z"]=-248.35546875, ["x"]=-557.11798095703,}, ["id"]="0x1d002ac5", ["contentFile"]="beautiful cities of morrowind.esp", ["recordId"]="ingred_crab_meat_01", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=0, ["x"]=0,},
    ["refNum"]=10949,},
    [191]={
    ["position"]={
    ["y"]=44.241870880127, ["z"]=-245.51167297363, ["x"]=-557.11798095703,}, ["id"]="0x1d002ac6", ["contentFile"]="beautiful cities of morrowind.esp", ["recordId"]="ingred_crab_meat_01", ["scale"]=1,
    ["rotation"]={ ["y"]=-0, ["z"]=1.5750000476837,
    ["x"]=0,}, ["refNum"]=10950,},
    [192]={
    ["position"]={
    ["y"]=-1648.5747070313, ["z"]=-700.0810546875, ["x"]=-194.98336791992,}, ["id"]="0x1d002ad6", ["contentFile"]="beautiful cities of morrowind.esp", ["recordId"]="ingred_ash_yam_01", ["scale"]=1,
    ["rotation"]={ ["y"]=-0, ["z"]=-2.9999985694885,
    ["x"]=0,}, ["refNum"]=10966,},
    [193]={
    ["position"]={
    ["y"]=-1646.6927490234, ["z"]=-700.0810546875, ["x"]=-200.83764648438,}, ["id"]="0x1d002ad7", ["contentFile"]="beautiful cities of morrowind.esp", ["recordId"]="ingred_ash_yam_01", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=0, ["x"]=0,},
    ["refNum"]=10967,},
    [194]={
    ["position"]={
    ["y"]=-1648.4273681641, ["z"]=-700.0810546875, ["x"]=-207.4496307373,}, ["id"]="0x1d002ad8", ["contentFile"]="beautiful cities of morrowind.esp", ["recordId"]="ingred_ash_yam_01", ["scale"]=1,
    ["rotation"]={ ["y"]=-0, ["z"]=-0.37499883770943,
    ["x"]=0,}, ["refNum"]=10968,},
    [195]={
    ["position"]={
    ["y"]=-1602.6873779297, ["z"]=-695.63354492188, ["x"]=-231.33581542969,}, ["id"]="0x1d002ad9", ["contentFile"]="beautiful cities of morrowind.esp", ["recordId"]="ingred_bread_01", ["scale"]=0.90000003576279,
    ["rotation"]={ ["y"]=-0,
    ["z"]=3.0000004768372, ["x"]=0,}, ["refNum"]=10969,},
    [196]={
    ["position"]={
    ["y"]=-1595.5791015625, ["z"]=-695.65753173828, ["x"]=-231.88288879395,}, ["id"]="0x1d002ada", ["contentFile"]="beautiful cities of morrowind.esp", ["recordId"]="ingred_bread_01", ["scale"]=1,
    ["rotation"]={ ["y"]=-0, ["z"]=3.0000004768372,
    ["x"]=0,}, ["refNum"]=10970,},
    [197]={
    ["position"]={
    ["y"]=-1588.0925292969, ["z"]=-695.63354492188, ["x"]=-234.51849365234,}, ["id"]="0x1d002adb", ["contentFile"]="beautiful cities of morrowind.esp", ["recordId"]="ingred_bread_01", ["scale"]=0.90000003576279,
    ["rotation"]={ ["y"]=-0,
    ["z"]=3.0000004768372, ["x"]=0,}, ["refNum"]=10971,},
    [198]={
    ["position"]={
    ["y"]=-1667.2259521484, ["z"]=-702.93621826172, ["x"]=-273.37796020508,}, ["id"]="0x1d002af2", ["contentFile"]="beautiful cities of morrowind.esp", ["recordId"]="ingred_crab_meat_01", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=0, ["x"]=0,},
    ["refNum"]=10994,},
    [199]={
    ["position"]={
    ["y"]=47.693878173828, ["z"]=93.480003356934, ["x"]=-480.59848022461,}, ["id"]="0x100a3bd", ["contentFile"]="morrowind.esm", ["recordId"]="light_de_candle_14_64", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=0, ["x"]=0,}, ["refNum"]=41917,},
    [200]={
    ["position"]={
    ["y"]=59.008514404297, ["z"]=-171.16117858887, ["x"]=-573.39398193359,}, ["id"]="0x100de0b", ["contentFile"]="morrowind.esm", ["recordId"]="light_com_candle_07_128", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=-2.3561942577362, ["x"]=0,},
    ["refNum"]=56843,},
    [201]={
    ["position"]={
    ["y"]=-183.71055603027, ["z"]=-123.09968566895, ["x"]=182.33570861816,}, ["id"]="0x100de18", ["contentFile"]="morrowind.esm", ["recordId"]="light_de_lamp_01", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=-0.59999924898148, ["x"]=0,},
    ["refNum"]=56856,},
    [202]={
    ["position"]={
    ["y"]=-118.31784820557, ["z"]=-96.160858154297, ["x"]=-257.52160644531,}, ["id"]="0x100de1a", ["contentFile"]="morrowind.esm", ["recordId"]="light_de_candle_08_64", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=2.7561943531036, ["x"]=0,},
    ["refNum"]=56858,},
    [203]={
    ["position"]={
    ["y"]=-116.31858062744, ["z"]=-96.160858154297, ["x"]=-335.90124511719,}, ["id"]="0x100de1c", ["contentFile"]="morrowind.esm", ["recordId"]="light_de_candle_08_64", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=-1.200000166893, ["x"]=0,},
    ["refNum"]=56860,},
    [204]={
    ["position"]={
    ["y"]=-835.62841796875, ["z"]=-686.86328125, ["x"]=-816.26873779297,}, ["id"]="0x100de31", ["contentFile"]="morrowind.esm", ["recordId"]="light_de_candle_06_64", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=-1.5707957744598, ["x"]=0,},
    ["refNum"]=56881,},
    [205]={
    ["position"]={
    ["y"]=-1424.0810546875, ["z"]=-549.48950195313, ["x"]=592.46258544922,}, ["id"]="0x100de35", ["contentFile"]="morrowind.esm", ["recordId"]="light_de_lantern_10_128_static", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=0, ["x"]=0,},
    ["refNum"]=56885,},
    [206]={
    ["position"]={
    ["y"]=-1746.2449951172, ["z"]=-549.97998046875, ["x"]=279.9469909668,}, ["id"]="0x100de36", ["contentFile"]="morrowind.esm", ["recordId"]="light_de_lantern_10_177_static", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=1.5830136537552, ["x"]=0,},
    ["refNum"]=56886,},
    [207]={
    ["position"]={
    ["y"]=-617.54846191406, ["z"]=-547.02429199219, ["x"]=592.67736816406,}, ["id"]="0x100de38", ["contentFile"]="morrowind.esm", ["recordId"]="light_de_lantern_10_177_static", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=0, ["x"]=0,},
    ["refNum"]=56888,},
    [208]={
    ["position"]={
    ["y"]=-301.85510253906, ["z"]=-551.58386230469, ["x"]=276.43173217773,}, ["id"]="0x100de3a", ["contentFile"]="morrowind.esm", ["recordId"]="light_de_lantern_10_177_static", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=-1.5999982357025, ["x"]=0,},
    ["refNum"]=56890,},
    [209]={
    ["position"]={
    ["y"]=-300.60751342773, ["z"]=-552.28332519531, ["x"]=-19.394102096558,}, ["id"]="0x100de3c", ["contentFile"]="morrowind.esm", ["recordId"]="light_de_lantern_10_177_static", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=-1.4992378950119, ["x"]=0,},
    ["refNum"]=56892,},
    [210]={
    ["position"]={
    ["y"]=-608.94403076172, ["z"]=-551.51861572266, ["x"]=-351.16104125977,}, ["id"]="0x100de3e", ["contentFile"]="morrowind.esm", ["recordId"]="light_de_lantern_10_128_static", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=3.083188533783, ["x"]=0,},
    ["refNum"]=56894,},
    [211]={
    ["position"]={
    ["y"]=-1434.9066162109, ["z"]=-554.61376953125, ["x"]=-336.79040527344,}, ["id"]="0x100de40", ["contentFile"]="morrowind.esm", ["recordId"]="light_de_lantern_10_128_static", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=-3.099996805191, ["x"]=0,},
    ["refNum"]=56896,},
    [212]={
    ["position"]={
    ["y"]=-1741.5939941406, ["z"]=-546.52801513672, ["x"]=-13.859000205994,}, ["id"]="0x100de42", ["contentFile"]="morrowind.esm", ["recordId"]="light_de_lantern_10_177_static", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=1.4835296869278, ["x"]=0,},
    ["refNum"]=56898,},
    [213]={
    ["position"]={
    ["y"]=-801.57684326172, ["z"]=-695.58520507813, ["x"]=-129.04959106445,}, ["id"]="0x100de4f", ["contentFile"]="morrowind.esm", ["recordId"]="light_de_candle_17_64", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=-1.5707957744598, ["x"]=0,},
    ["refNum"]=56911,},
    [214]={
    ["position"]={
    ["y"]=-532.84851074219, ["z"]=-695.58502197266, ["x"]=-31.481143951416,}, ["id"]="0x100de54", ["contentFile"]="morrowind.esm", ["recordId"]="light_de_candle_17_64", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=-1.570796251297, ["x"]=0,},
    ["refNum"]=56916,},
    [215]={
    ["position"]={
    ["y"]=-537.27618408203, ["z"]=-695.58514404297, ["x"]=-134.38372802734,}, ["id"]="0x100de55", ["contentFile"]="morrowind.esm", ["recordId"]="light_de_candle_17_64", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=-1.5707957744598, ["x"]=0,},
    ["refNum"]=56917,},
    [216]={
    ["position"]={
    ["y"]=-1328.5751953125, ["z"]=-694.50891113281, ["x"]=227.16744995117,}, ["id"]="0x100de7e", ["contentFile"]="morrowind.esm", ["recordId"]="light_de_candle_17_64", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=-1.5707957744598, ["x"]=0,},
    ["refNum"]=56958,},
    [217]={
    ["position"]={
    ["y"]=-866.06689453125, ["z"]=-649.20397949219, ["x"]=-536.50378417969,}, ["id"]="0x105c5d3", ["contentFile"]="morrowind.esm", ["recordId"]="light_de_candle_06_64", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=0, ["x"]=0,}, ["refNum"]=50643,},
    [218]={
    ["position"]={
    ["y"]=-703.97998046875, ["z"]=-675.71899414063, ["x"]=-829.10302734375,}, ["id"]="0x105c5d4", ["contentFile"]="morrowind.esm", ["recordId"]="light_com_candle_07_77", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=-2.2008605003357, ["x"]=0,},
    ["refNum"]=50644,},
    [219]={
    ["position"]={
    ["y"]=-1021.6599731445, ["z"]=-608.72357177734, ["x"]=131.46499633789,}, ["id"]="0x105c5e0", ["contentFile"]="morrowind.esm", ["recordId"]="light_de_lamp_01_128", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=0, ["x"]=0,}, ["refNum"]=50656,},
    [220]={
    ["position"]={
    ["y"]=-1005.2236938477, ["z"]=-540.25646972656, ["x"]=750.42712402344,}, ["id"]="0x105c5e5", ["contentFile"]="morrowind.esm", ["recordId"]="light_de_lantern_08_177_01", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=-2.3561947345734, ["x"]=0,},
    ["refNum"]=50661,},
    [221]={
    ["position"]={
    ["y"]=22.073329925537, ["z"]=-300.38375854492, ["x"]=791.40972900391,}, ["id"]="0x105c5e6", ["contentFile"]="morrowind.esm", ["recordId"]="light_de_lantern_03_200", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=-0.78539824485779, ["x"]=0,},
    ["refNum"]=50662,},
    [222]={
    ["position"]={
    ["y"]=10.998757362366, ["z"]=68.195999145508, ["x"]=-579.54125976563,}, ["id"]="0x1072974", ["contentFile"]="morrowind.esm", ["recordId"]="light_de_candle_14_64", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=0, ["x"]=0,}, ["refNum"]=10612,},
    [223]={
    ["position"]={
    ["y"]=22.483823776245, ["z"]=222.97882080078, ["x"]=-225.7243347168,}, ["id"]="0x1074e1d", ["contentFile"]="morrowind.esm", ["recordId"]="light_de_lantern_03_200", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=-0.20000013709068, ["x"]=0,},
    ["refNum"]=19997,},
    [224]={
    ["position"]={
    ["y"]=-309.26983642578, ["z"]=-36.203563690186, ["x"]=-510.03219604492,}, ["id"]="0x1074e21", ["contentFile"]="morrowind.esm", ["recordId"]="light_de_lantern_03_200", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=-0.98539769649506, ["x"]=0,},
    ["refNum"]=20001,},
    [225]={
    ["position"]={
    ["y"]=-56.069141387939, ["z"]=-30.412553787231, ["x"]=10.142179489136,}, ["id"]="0x1074e24", ["contentFile"]="morrowind.esm", ["recordId"]="light_de_lantern_03_200", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=1.9561941623688, ["x"]=0,},
    ["refNum"]=20004,},
    [226]={
    ["position"]={
    ["y"]=272.69097900391, ["z"]=-123.71390533447, ["x"]=186.85356140137,}, ["id"]="0x1074e2c", ["contentFile"]="morrowind.esm", ["recordId"]="flame light_64", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=0, ["x"]=0,}, ["refNum"]=20012,},
    [227]={
    ["position"]={
    ["y"]=-1093.6450195313, ["z"]=-551.72271728516, ["x"]=-835.90533447266,}, ["id"]="0x10753f5", ["contentFile"]="morrowind.esm", ["recordId"]="light_de_lantern_10_128_static", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=1.4835296869278, ["x"]=0,},
    ["refNum"]=21493,},
    [228]={
    ["position"]={
    ["y"]=-275.38754272461, ["z"]=422.56909179688, ["x"]=-515.58245849609,}, ["id"]="0x1d002a49", ["contentFile"]="beautiful cities of morrowind.esp", ["recordId"]="rp_light_de_lantern_08", ["scale"]=1,
    ["rotation"]={ ["y"]=-0, ["z"]=1.570796251297,
    ["x"]=0,}, ["refNum"]=10825,},
    [229]={
    ["position"]={
    ["y"]=51.855529785156, ["z"]=89.385848999023, ["x"]=-561.53741455078,}, ["id"]="0x100a3c0", ["contentFile"]="morrowind.esm", ["recordId"]="misc_de_pot_redware_02", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=0, ["x"]=0,}, ["refNum"]=41920,},
    [230]={
    ["position"]={
    ["y"]=44.032135009766, ["z"]=-244.99368286133, ["x"]=-558.27734375,}, ["id"]="0x100de0c", ["contentFile"]="morrowind.esm", ["recordId"]="misc_de_bowl_orange_green_01", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=-0.3996804356575, ["x"]=0,},
    ["refNum"]=56844,},
    [231]={
    ["position"]={
    ["y"]=-722.82098388672, ["z"]=-697.37799072266, ["x"]=-798.80102539063,}, ["id"]="0x100de26", ["contentFile"]="morrowind.esm", ["recordId"]="misc_de_pot_blue_01", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=0, ["x"]=0,}, ["refNum"]=56870,},
    [232]={
    ["position"]={
    ["y"]=-701.92883300781, ["z"]=-697.79748535156, ["x"]=-749.67156982422,}, ["id"]="0x100de27", ["contentFile"]="morrowind.esm", ["recordId"]="misc_de_pot_glass_peach_01", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=0, ["x"]=0,}, ["refNum"]=56871,},
    [233]={
    ["position"]={
    ["y"]=-709.04211425781, ["z"]=-698.00805664063, ["x"]=-783.19659423828,}, ["id"]="0x100de28", ["contentFile"]="morrowind.esm", ["recordId"]="misc_de_pot_glass_peach_01", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=0, ["x"]=0,}, ["refNum"]=56872,},
    [234]={
    ["position"]={
    ["y"]=-719.33801269531, ["z"]=-697.07000732422, ["x"]=-759.67297363281,}, ["id"]="0x100de29", ["contentFile"]="morrowind.esm", ["recordId"]="misc_de_pot_glass_peach_02", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=0, ["x"]=0,}, ["refNum"]=56873,},
    [235]={
    ["position"]={
    ["y"]=-291.65112304688, ["z"]=-692.23760986328, ["x"]=273.60986328125,}, ["id"]="0x100de66", ["contentFile"]="morrowind.esm", ["recordId"]="misc_de_pot_redware_04", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=0, ["x"]=0,}, ["refNum"]=56934,},
    [236]={
    ["position"]={
    ["y"]=-291.65112304688, ["z"]=-692.23760986328, ["x"]=233.60250854492,}, ["id"]="0x100de67", ["contentFile"]="morrowind.esm", ["recordId"]="misc_de_pot_redware_04", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=0, ["x"]=0,}, ["refNum"]=56935,},
    [237]={
    ["position"]={
    ["y"]=-816.94708251953, ["z"]=-695.53784179688, ["x"]=-821.64544677734,}, ["id"]="0x100de68", ["contentFile"]="morrowind.esm", ["recordId"]="misc_de_pot_glass_peach_01", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=-1.2707960605621, ["x"]=0,},
    ["refNum"]=56936,},
    [238]={
    ["position"]={
    ["y"]=-1347.0948486328, ["z"]=-688.04699707031, ["x"]=253.94053649902,}, ["id"]="0x100de90", ["contentFile"]="morrowind.esm", ["recordId"]="misc_com_bottle_09", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=-0.3996804356575, ["x"]=0,},
    ["refNum"]=56976,},
    [239]={
    ["position"]={
    ["y"]=-1563.2176513672, ["z"]=-688.04699707031, ["x"]=308.68759155273,}, ["id"]="0x100de91", ["contentFile"]="morrowind.esm", ["recordId"]="misc_com_bottle_09", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=0, ["x"]=0,}, ["refNum"]=56977,},
    [240]={
    ["position"]={
    ["y"]=-1568.1401367188, ["z"]=-685.84161376953, ["x"]=202.44259643555,}, ["id"]="0x100de92", ["contentFile"]="morrowind.esm", ["recordId"]="misc_com_bottle_15", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=0, ["x"]=0,}, ["refNum"]=56978,},
    [241]={
    ["position"]={
    ["y"]=-1582.1118164063, ["z"]=-703.96203613281, ["x"]=337.68228149414,}, ["id"]="0x100de93", ["contentFile"]="morrowind.esm", ["recordId"]="misc_de_foldedcloth00", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=-0.49999931454659, ["x"]=0,},
    ["refNum"]=56979,},
    [242]={
    ["position"]={
    ["y"]=-1552.0865478516, ["z"]=-697.56097412109, ["x"]=252.07307434082,}, ["id"]="0x100de94", ["contentFile"]="morrowind.esm", ["recordId"]="misc_de_goblet_02", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=0, ["x"]=0,}, ["refNum"]=56980,},
    [243]={
    ["position"]={
    ["y"]=-1579.7562255859, ["z"]=-697.56097412109, ["x"]=288.17123413086,}, ["id"]="0x100de95", ["contentFile"]="morrowind.esm", ["recordId"]="misc_de_goblet_02", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=0, ["x"]=0,}, ["refNum"]=56981,},
    [244]={
    ["position"]={
    ["y"]=-1312.3139648438, ["z"]=-687.06591796875, ["x"]=305.19317626953,}, ["id"]="0x100de96", ["contentFile"]="morrowind.esm", ["recordId"]="misc_com_bottle_04", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=-0.99999892711639, ["x"]=0,},
    ["refNum"]=56982,},
    [245]={
    ["position"]={
    ["y"]=-1310.7073974609, ["z"]=-697.56097412109, ["x"]=267.07391357422,}, ["id"]="0x100de97", ["contentFile"]="morrowind.esm", ["recordId"]="misc_de_goblet_07", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=0, ["x"]=0,}, ["refNum"]=56983,},
    [246]={
    ["position"]={
    ["y"]=-1344.4768066406, ["z"]=-697.56097412109, ["x"]=306.85363769531,}, ["id"]="0x100de98", ["contentFile"]="morrowind.esm", ["recordId"]="misc_de_goblet_07", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=0, ["x"]=0,}, ["refNum"]=56984,},
    [247]={
    ["position"]={
    ["y"]=-1593.9299316406, ["z"]=-671.76544189453, ["x"]=574.96264648438,}, ["id"]="0x100de99", ["contentFile"]="morrowind.esm", ["recordId"]="misc_com_basket_02", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=-0.1999996304512, ["x"]=0,},
    ["refNum"]=56985,},
    [248]={
    ["position"]={
    ["y"]=-1666.4665527344, ["z"]=-620.92022705078, ["x"]=574.58715820313,}, ["id"]="0x100de9a", ["contentFile"]="morrowind.esm", ["recordId"]="misc_com_bottle_03", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=-0.099999733269215, ["x"]=0,},
    ["refNum"]=56986,},
    [249]={
    ["position"]={
    ["y"]=-1675.8070068359, ["z"]=-668.80279541016, ["x"]=563.04479980469,}, ["id"]="0x100de9b", ["contentFile"]="morrowind.esm", ["recordId"]="misc_com_bottle_04", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=0, ["x"]=0,}, ["refNum"]=56987,},
    [250]={
    ["position"]={
    ["y"]=-1643.4370117188, ["z"]=-669.77691650391, ["x"]=563.7421875,}, ["id"]="0x100de9c", ["contentFile"]="morrowind.esm", ["recordId"]="misc_com_bottle_05", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=-0.59999924898148, ["x"]=0,},
    ["refNum"]=56988,},
    [251]={
    ["position"]={
    ["y"]=-1717.3369140625, ["z"]=-668.80279541016, ["x"]=578.63873291016,}, ["id"]="0x100de9d", ["contentFile"]="morrowind.esm", ["recordId"]="misc_com_bottle_04", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=0, ["x"]=0,}, ["refNum"]=56989,},
    [252]={
    ["position"]={
    ["y"]=-1701.1008300781, ["z"]=-668.80279541016, ["x"]=566.07098388672,}, ["id"]="0x100de9e", ["contentFile"]="morrowind.esm", ["recordId"]="misc_com_bottle_04", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=0, ["x"]=0,}, ["refNum"]=56990,},
    [253]={
    ["position"]={
    ["y"]=-1688.0986328125, ["z"]=-668.80279541016, ["x"]=575.25305175781,}, ["id"]="0x100de9f", ["contentFile"]="morrowind.esm", ["recordId"]="misc_com_bottle_04", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=0, ["x"]=0,}, ["refNum"]=56991,},
    [254]={
    ["position"]={
    ["y"]=-1658.7713623047, ["z"]=-669.77691650391, ["x"]=576.72814941406,}, ["id"]="0x100dea0", ["contentFile"]="morrowind.esm", ["recordId"]="misc_com_bottle_05", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=0, ["x"]=0,}, ["refNum"]=56992,},
    [255]={
    ["position"]={
    ["y"]=-1658.7296142578, ["z"]=-669.77691650391, ["x"]=563.6806640625,}, ["id"]="0x100dea1", ["contentFile"]="morrowind.esm", ["recordId"]="misc_com_bottle_05", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=0, ["x"]=0,}, ["refNum"]=56993,},
    [256]={
    ["position"]={
    ["y"]=-1643.5877685547, ["z"]=-669.77691650391, ["x"]=580.11834716797,}, ["id"]="0x100dea2", ["contentFile"]="morrowind.esm", ["recordId"]="misc_com_bottle_05", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=-0.59999924898148, ["x"]=0,},
    ["refNum"]=56994,},
    [257]={
    ["position"]={
    ["y"]=-1714.1452636719, ["z"]=-620.92022705078, ["x"]=574.36181640625,}, ["id"]="0x100dea3", ["contentFile"]="morrowind.esm", ["recordId"]="misc_com_bottle_03", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=-1.1999987363815, ["x"]=0,},
    ["refNum"]=56995,},
    [258]={
    ["position"]={
    ["y"]=-1690.8287353516, ["z"]=-620.92022705078, ["x"]=572.87237548828,}, ["id"]="0x100dea4", ["contentFile"]="morrowind.esm", ["recordId"]="misc_com_bottle_03", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=-0.099999733269215, ["x"]=0,},
    ["refNum"]=56996,},
    [259]={
    ["position"]={
    ["y"]=-1589.8880615234, ["z"]=-619.87005615234, ["x"]=564.10046386719,}, ["id"]="0x100dea5", ["contentFile"]="morrowind.esm", ["recordId"]="misc_com_bottle_14", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=1.5, ["x"]=0,}, ["refNum"]=56997,},
    [260]={
    ["position"]={
    ["y"]=-1566.3182373047, ["z"]=-619.87005615234, ["x"]=578.44476318359,}, ["id"]="0x100dea6", ["contentFile"]="morrowind.esm", ["recordId"]="misc_com_bottle_14", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=0, ["x"]=0,}, ["refNum"]=56998,},
    [261]={
    ["position"]={
    ["y"]=-1564.7561035156, ["z"]=-619.87005615234, ["x"]=564.60083007813,}, ["id"]="0x100dea7", ["contentFile"]="morrowind.esm", ["recordId"]="misc_com_bottle_14", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=0, ["x"]=0,}, ["refNum"]=56999,},
    [262]={
    ["position"]={
    ["y"]=-1578.12890625, ["z"]=-619.87005615234, ["x"]=579.64526367188,}, ["id"]="0x100dea8", ["contentFile"]="morrowind.esm", ["recordId"]="misc_com_bottle_14", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=0, ["x"]=0,}, ["refNum"]=57000,},
    [263]={
    ["position"]={
    ["y"]=-1577.6929931641, ["z"]=-619.86999511719, ["x"]=565.09002685547,}, ["id"]="0x100dea9", ["contentFile"]="morrowind.esm", ["recordId"]="misc_com_bottle_14", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=-0.79936105012894, ["x"]=0,},
    ["refNum"]=57001,},
    [264]={
    ["position"]={
    ["y"]=-1590.9367675781, ["z"]=-619.87005615234, ["x"]=581.00494384766,}, ["id"]="0x100deaa", ["contentFile"]="morrowind.esm", ["recordId"]="misc_com_bottle_14", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=-0.79999959468842, ["x"]=0,},
    ["refNum"]=57002,},
    [265]={
    ["position"]={
    ["y"]=-1629.1906738281, ["z"]=-635.79998779297, ["x"]=568.46801757813,}, ["id"]="0x100deab", ["contentFile"]="morrowind.esm", ["recordId"]="misc_de_bowl_bugdesign_01", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=0, ["x"]=0,}, ["refNum"]=57003,},
    [266]={
    ["position"]={
    ["y"]=-1679.2965087891, ["z"]=-728.64111328125, ["x"]=566.08728027344,}, ["id"]="0x100deac", ["contentFile"]="morrowind.esm", ["recordId"]="misc_de_goblet_03", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=-2.3999977111816, ["x"]=0,},
    ["refNum"]=57004,},
    [267]={
    ["position"]={
    ["y"]=-1716.5554199219, ["z"]=-728.64111328125, ["x"]=580.34130859375,}, ["id"]="0x100dead", ["contentFile"]="morrowind.esm", ["recordId"]="misc_de_goblet_03", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=0, ["x"]=0,}, ["refNum"]=57005,},
    [268]={
    ["position"]={
    ["y"]=-1716.9951171875, ["z"]=-728.64111328125, ["x"]=566.08654785156,}, ["id"]="0x100deae", ["contentFile"]="morrowind.esm", ["recordId"]="misc_de_goblet_03", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=0, ["x"]=0,}, ["refNum"]=57006,},
    [269]={
    ["position"]={
    ["y"]=-1704.1370849609, ["z"]=-728.64111328125, ["x"]=577.72790527344,}, ["id"]="0x100deaf", ["contentFile"]="morrowind.esm", ["recordId"]="misc_de_goblet_03", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=0, ["x"]=0,}, ["refNum"]=57007,},
    [270]={
    ["position"]={
    ["y"]=-1702.7668457031, ["z"]=-728.64111328125, ["x"]=559.43316650391,}, ["id"]="0x100deb0", ["contentFile"]="morrowind.esm", ["recordId"]="misc_de_goblet_03", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=-0.99999892711639, ["x"]=0,},
    ["refNum"]=57008,},
    [271]={
    ["position"]={
    ["y"]=-1692.9809570313, ["z"]=-728.64111328125, ["x"]=581.13256835938,}, ["id"]="0x100deb1", ["contentFile"]="morrowind.esm", ["recordId"]="misc_de_goblet_03", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=-0.99999892711639, ["x"]=0,},
    ["refNum"]=57009,},
    [272]={
    ["position"]={
    ["y"]=-1690.7670898438, ["z"]=-728.64111328125, ["x"]=559.81762695313,}, ["id"]="0x100deb2", ["contentFile"]="morrowind.esm", ["recordId"]="misc_de_goblet_03", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=-2.3999977111816, ["x"]=0,},
    ["refNum"]=57010,},
    [273]={
    ["position"]={
    ["y"]=-1604.4412841797, ["z"]=-735.66027832031, ["x"]=580.89245605469,}, ["id"]="0x100deb3", ["contentFile"]="morrowind.esm", ["recordId"]="misc_de_glass_green_01", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=2.2831881046295, ["x"]=0,},
    ["refNum"]=57011,},
    [274]={
    ["position"]={
    ["y"]=-1562.8132324219, ["z"]=-735.66027832031, ["x"]=580.72283935547,}, ["id"]="0x100deb4", ["contentFile"]="morrowind.esm", ["recordId"]="misc_de_glass_green_01", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=0, ["x"]=0,}, ["refNum"]=57012,},
    [275]={
    ["position"]={
    ["y"]=-1563.0811767578, ["z"]=-735.66027832031, ["x"]=564.58050537109,}, ["id"]="0x100deb5", ["contentFile"]="morrowind.esm", ["recordId"]="misc_de_glass_green_01", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=-0.59999924898148, ["x"]=0,},
    ["refNum"]=57013,},
    [276]={
    ["position"]={
    ["y"]=-1575.0815429688, ["z"]=-735.66027832031, ["x"]=582.18676757813,}, ["id"]="0x100deb6", ["contentFile"]="morrowind.esm", ["recordId"]="misc_de_glass_green_01", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=-0.59999924898148, ["x"]=0,},
    ["refNum"]=57014,},
    [277]={
    ["position"]={
    ["y"]=-1578.03515625, ["z"]=-735.66027832031, ["x"]=562.49597167969,}, ["id"]="0x100deb7", ["contentFile"]="morrowind.esm", ["recordId"]="misc_de_glass_green_01", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=-1.4999994039536, ["x"]=0,},
    ["refNum"]=57015,},
    [278]={
    ["position"]={
    ["y"]=-1588.5902099609, ["z"]=-735.66027832031, ["x"]=581.67126464844,}, ["id"]="0x100deb8", ["contentFile"]="morrowind.esm", ["recordId"]="misc_de_glass_green_01", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=-1.4999994039536, ["x"]=0,},
    ["refNum"]=57016,},
    [279]={
    ["position"]={
    ["y"]=-1590.958984375, ["z"]=-735.66027832031, ["x"]=562.21997070313,}, ["id"]="0x100deb9", ["contentFile"]="morrowind.esm", ["recordId"]="misc_de_glass_green_01", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=2.2831881046295, ["x"]=0,},
    ["refNum"]=57017,},
    [280]={
    ["position"]={
    ["y"]=-1663.2569580078, ["z"]=-735.06506347656, ["x"]=563.14416503906,}, ["id"]="0x100deba", ["contentFile"]="morrowind.esm", ["recordId"]="misc_de_foldedcloth00", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=-2.3999977111816, ["x"]=0,},
    ["refNum"]=57018,},
    [281]={
    ["position"]={
    ["y"]=-1627.4782714844, ["z"]=-735.06506347656, ["x"]=572.7978515625,}, ["id"]="0x100debb", ["contentFile"]="morrowind.esm", ["recordId"]="misc_de_foldedcloth00", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=-0.6999990940094, ["x"]=0,},
    ["refNum"]=57019,},
    [282]={
    ["position"]={
    ["y"]=-1647.5661621094, ["z"]=-735.06506347656, ["x"]=578.81066894531,}, ["id"]="0x100debc", ["contentFile"]="morrowind.esm", ["recordId"]="misc_de_foldedcloth00", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=-1.6999982595444, ["x"]=0,},
    ["refNum"]=57020,},
    [283]={
    ["position"]={
    ["y"]=-1379.1599121094, ["z"]=-652.3193359375, ["x"]=598.79113769531,}, ["id"]="0x100debd", ["contentFile"]="morrowind.esm", ["recordId"]="misc_de_pitcher_01", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=-1.4999984502792, ["x"]=0,},
    ["refNum"]=57021,},
    [284]={
    ["position"]={
    ["y"]=-1407.6186523438, ["z"]=-666.23541259766, ["x"]=598.99163818359,}, ["id"]="0x100debe", ["contentFile"]="morrowind.esm", ["recordId"]="misc_de_pot_blue_01", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=0, ["x"]=0,}, ["refNum"]=57022,},
    [285]={
    ["position"]={
    ["y"]=-1472.5545654297, ["z"]=-711.16632080078, ["x"]=38.500297546387,}, ["id"]="0x1046ea5", ["contentFile"]="morrowind.esm", ["recordId"]="misc_uni_pillow_01", ["scale"]=1.1400001049042, ["rotation"]={ ["y"]=-0, ["z"]=1.4853984117508, ["x"]=0,},
    ["refNum"]=28325,},
    [286]={
    ["position"]={
    ["y"]=-1704.501953125, ["z"]=-711.166015625, ["x"]=35.15299987793,}, ["id"]="0x1046ea6", ["contentFile"]="morrowind.esm", ["recordId"]="misc_uni_pillow_01", ["scale"]=1.1400001049042, ["rotation"]={ ["y"]=-0, ["z"]=1.5847589969635, ["x"]=0,},
    ["refNum"]=28326,},
    [287]={
    ["position"]={
    ["y"]=-1472.5545654297, ["z"]=-631.43902587891, ["x"]=38.500297546387,}, ["id"]="0x1046ea7", ["contentFile"]="morrowind.esm", ["recordId"]="misc_uni_pillow_01", ["scale"]=1.2200000286102, ["rotation"]={ ["y"]=-0, ["z"]=1.5853983163834, ["x"]=0,},
    ["refNum"]=28327,},
    [288]={
    ["position"]={
    ["y"]=-1699.8255615234, ["z"]=-631.43902587891, ["x"]=39.64656829834,}, ["id"]="0x1046ea8", ["contentFile"]="morrowind.esm", ["recordId"]="misc_uni_pillow_01", ["scale"]=1.1600000858307, ["rotation"]={ ["y"]=-0, ["z"]=1.6853984594345, ["x"]=0,},
    ["refNum"]=28328,},
    [289]={
    ["position"]={
    ["y"]=-1641.5983886719, ["z"]=-687.19366455078, ["x"]=-267.79522705078,}, ["id"]="0x104bdeb", ["contentFile"]="morrowind.esm", ["recordId"]="misc_com_bottle_01", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=1.325000166893, ["x"]=0,},
    ["refNum"]=48619,},
    [290]={
    ["position"]={
    ["y"]=-1662.6936035156, ["z"]=-687.70233154297, ["x"]=-239.80648803711,}, ["id"]="0x104bded", ["contentFile"]="morrowind.esm", ["recordId"]="misc_com_bottle_09", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=0.60000002384186, ["x"]=0,},
    ["refNum"]=48621,},
    [291]={
    ["position"]={
    ["y"]=-1614.4091796875, ["z"]=-686.96252441406, ["x"]=-256.13043212891,}, ["id"]="0x104bdee", ["contentFile"]="morrowind.esm", ["recordId"]="misc_com_bottle_11", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=-0.54999953508377, ["x"]=0,},
    ["refNum"]=48622,},
    [292]={
    ["position"]={
    ["y"]=-1624.0856933594, ["z"]=-686.96252441406, ["x"]=-273.26531982422,}, ["id"]="0x104bdef", ["contentFile"]="morrowind.esm", ["recordId"]="misc_com_bottle_11", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=-0.77499943971634, ["x"]=0,},
    ["refNum"]=48623,},
    [293]={
    ["position"]={
    ["y"]=-1647.4045410156, ["z"]=-703.02282714844, ["x"]=-202.34677124023,}, ["id"]="0x104bdf1", ["contentFile"]="morrowind.esm", ["recordId"]="misc_de_bowl_redware_01", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=1.0000736713409, ["x"]=0,},
    ["refNum"]=48625,},
    [294]={
    ["position"]={
    ["y"]=-1609.5086669922, ["z"]=-696.64910888672, ["x"]=-265.90777587891,}, ["id"]="0x104bdf2", ["contentFile"]="morrowind.esm", ["recordId"]="misc_de_goblet_02", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=0.54999995231628, ["x"]=0,},
    ["refNum"]=48626,},
    [295]={
    ["position"]={
    ["y"]=-1595.1676025391, ["z"]=-703.02282714844, ["x"]=-233.1227722168,}, ["id"]="0x104bdf0", ["contentFile"]="morrowind.esm", ["recordId"]="misc_de_bowl_redware_01", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=1.0000736713409, ["x"]=0,},
    ["refNum"]=48624,},
    [296]={
    ["position"]={
    ["y"]=20.624294281006, ["z"]=66.397613525391, ["x"]=-503.96243286133,}, ["id"]="0x1071cfe", ["contentFile"]="morrowind.esm", ["refNum"]=7422, ["recordId"]="misc_soulgem_petty", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=0, ["x"]=0,},
    ["soulId"]="alit",},
    [297]={
    ["position"]={
    ["y"]=18.654792785645, ["z"]=66.397613525391, ["x"]=-488.8014831543,}, ["id"]="0x1071cff", ["contentFile"]="morrowind.esm", ["refNum"]=7423, ["recordId"]="misc_soulgem_petty", ["scale"]=1,
    ["rotation"]={ ["y"]=-0, ["z"]=-0.89999902248383, ["x"]=0,}, ["soulId"]="skeleton warrior",},
    [298]={
    ["position"]={
    ["y"]=8.0975179672241, ["z"]=66.397613525391, ["x"]=-481.18161010742,}, ["id"]="0x1071d00", ["contentFile"]="morrowind.esm", ["refNum"]=7424, ["recordId"]="misc_soulgem_petty", ["scale"]=1,
    ["rotation"]={ ["y"]=-0, ["z"]=-1.7999981641769, ["x"]=0,}, ["soulId"]="rat",},
    [299]={
    ["position"]={
    ["y"]=21.237655639648, ["z"]=67.11238861084, ["x"]=-481.67642211914,}, ["id"]="0x1071d01", ["contentFile"]="morrowind.esm", ["refNum"]=7425, ["recordId"]="misc_soulgem_common", ["scale"]=1,
    ["rotation"]={ ["y"]=-0, ["z"]=-1.7999994754791, ["x"]=0,}, ["soulId"]="ogrim titan",},
    [300]={
    ["position"]={
    ["y"]=16.187507629395, ["z"]=67.11238861084, ["x"]=-480.61376953125,}, ["id"]="0x1071d02", ["contentFile"]="morrowind.esm", ["refNum"]=7426, ["recordId"]="misc_soulgem_common", ["scale"]=1,
    ["rotation"]={ ["y"]=-0, ["z"]=-1.200000166893, ["x"]=0,}, ["soulId"]="skeleton warrior",},
    [301]={
    ["position"]={
    ["y"]=24.397451400757, ["z"]=70.585884094238, ["x"]=-518.22357177734,}, ["id"]="0x1071d03", ["contentFile"]="morrowind.esm", ["refNum"]=7427, ["recordId"]="misc_soulgem_greater", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=0, ["x"]=0,},
    ["soulId"]="dremora",},
    [302]={
    ["position"]={
    ["y"]=22.057712554932, ["z"]=65.559539794922, ["x"]=-527.58282470703,}, ["id"]="0x1071d04", ["contentFile"]="morrowind.esm", ["refNum"]=7428, ["recordId"]="misc_soulgem_grand", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=0, ["x"]=0,},
    ["soulId"]="winged twilight",},
    [303]={
    ["position"]={
    ["y"]=12.861470222473, ["z"]=67.033546447754, ["x"]=-510.8044128418,}, ["id"]="0x1071d05", ["contentFile"]="morrowind.esm", ["refNum"]=7429, ["recordId"]="misc_soulgem_lesser", ["scale"]=1,
    ["rotation"]={ ["y"]=-0, ["z"]=-0.1999996304512, ["x"]=0,}, ["soulId"]="guar",},
    [304]={
    ["position"]={
    ["y"]=8.9297151565552, ["z"]=66.744110107422, ["x"]=-525.68597412109,}, ["id"]="0x1071d06", ["contentFile"]="morrowind.esm", ["refNum"]=7430, ["recordId"]="misc_soulgem_lesser", ["scale"]=1,
    ["rotation"]={ ["y"]=-0.099999740719795,
    ["z"]=-1.3999993801117, ["x"]=0,}, ["soulId"]="kagouti_blighted",},
    [305]={
    ["position"]={
    ["y"]=7.6131734848022, ["z"]=67.033546447754, ["x"]=-516.82946777344,}, ["id"]="0x1071d07", ["contentFile"]="morrowind.esm", ["recordId"]="misc_soulgem_lesser", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=0.20000000298023, ["x"]=0,},
    ["refNum"]=7431,},
    [306]={
    ["position"]={
    ["y"]=-608.48034667969, ["z"]=-734.85003662109, ["x"]=-309.5407409668,}, ["id"]="0x1d002a7c", ["contentFile"]="beautiful cities of morrowind.esp", ["recordId"]="misc_de_pot_redware_01", ["scale"]=1.1400001049042,
    ["rotation"]={ ["y"]=-0, ["z"]=0,
    ["x"]=0,}, ["refNum"]=10876,},
    [307]={
    ["position"]={
    ["y"]=-651.67681884766, ["z"]=-636.52215576172, ["x"]=-308.72775268555,}, ["id"]="0x1d002a8b", ["contentFile"]="beautiful cities of morrowind.esp", ["recordId"]="misc_de_pot_redware_01", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=0, ["x"]=0,},
    ["refNum"]=10891,},
    [308]={
    ["position"]={
    ["y"]=-584.85882568359, ["z"]=-636.35546875, ["x"]=-317.56195068359,}, ["id"]="0x1d002a91", ["contentFile"]="beautiful cities of morrowind.esp", ["recordId"]="misc_quill", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=-1.2750004529953, ["x"]=0,},
    ["refNum"]=10897,},
    [309]={
    ["position"]={
    ["y"]=-566.5244140625, ["z"]=-636.35546875, ["x"]=-316.57049560547,}, ["id"]="0x1d002a92", ["contentFile"]="beautiful cities of morrowind.esp", ["recordId"]="misc_quill", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=-0.97500067949295, ["x"]=0,},
    ["refNum"]=10898,},
    [310]={
    ["position"]={
    ["y"]=-574.69274902344, ["z"]=-636.35546875, ["x"]=-319.19329833984,}, ["id"]="0x1d002a93", ["contentFile"]="beautiful cities of morrowind.esp", ["recordId"]="misc_quill", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=-1.3500002622604, ["x"]=0,},
    ["refNum"]=10899,},
    [311]={
    ["position"]={
    ["y"]=-530.24365234375, ["z"]=-631.02197265625, ["x"]=-322.9573059082,}, ["id"]="0x1d002a94", ["contentFile"]="beautiful cities of morrowind.esp", ["recordId"]="misc_inkwell", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=0, ["x"]=0,},
    ["refNum"]=10900,},
    [312]={
    ["position"]={
    ["y"]=-541.51434326172, ["z"]=-631.02197265625, ["x"]=-316.09652709961,}, ["id"]="0x1d002a95", ["contentFile"]="beautiful cities of morrowind.esp", ["recordId"]="misc_inkwell", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=0, ["x"]=0,},
    ["refNum"]=10901,},
    [313]={
    ["position"]={
    ["y"]=-554.58197021484, ["z"]=-631.02197265625, ["x"]=-310.06036376953,}, ["id"]="0x1d002a96", ["contentFile"]="beautiful cities of morrowind.esp", ["recordId"]="misc_inkwell", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=0, ["x"]=0,},
    ["refNum"]=10902,},
    [314]={
    ["position"]={
    ["y"]=-527.00506591797, ["z"]=-631.02197265625, ["x"]=-309.16928100586,}, ["id"]="0x1d002a97", ["contentFile"]="beautiful cities of morrowind.esp", ["recordId"]="misc_inkwell", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=0, ["x"]=0,},
    ["refNum"]=10903,},
    [315]={
    ["position"]={
    ["y"]=-434.69186401367, ["z"]=-636.98101806641, ["x"]=73.366523742676,}, ["id"]="0x1d002a9d", ["contentFile"]="beautiful cities of morrowind.esp", ["recordId"]="misc_de_bowl_glass_peach_01", ["scale"]=1,
    ["rotation"]={ ["y"]=-0, ["z"]=0, ["x"]=0,}, ["refNum"]=10909,},
    [316]={
    ["position"]={
    ["y"]=-655.19311523438, ["z"]=-636.771484375, ["x"]=72.845726013184,}, ["id"]="0x1d002aad", ["contentFile"]="beautiful cities of morrowind.esp", ["recordId"]="misc_de_pot_redware_03", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=0, ["x"]=0,},
    ["refNum"]=10925,},
    [317]={
    ["position"]={
    ["y"]=-412.09130859375, ["z"]=-735.09936523438, ["x"]=-277.18890380859,}, ["id"]="0x1d002ac2", ["contentFile"]="beautiful cities of morrowind.esp", ["recordId"]="misc_de_pot_blue_01", ["scale"]=1,
    ["rotation"]={ ["y"]=-0, ["z"]=-0.1499994546175,
    ["x"]=0,}, ["refNum"]=10946,},
    [318]={
    ["position"]={
    ["y"]=-400.92761230469, ["z"]=-734.94482421875, ["x"]=-258.36724853516,}, ["id"]="0x1d002ac3", ["contentFile"]="beautiful cities of morrowind.esp", ["recordId"]="misc_de_pot_blue_02", ["scale"]=1,
    ["rotation"]={ ["y"]=-0, ["z"]=-0.1499994546175,
    ["x"]=0,}, ["refNum"]=10947,},
    [319]={
    ["position"]={
    ["y"]=70.939125061035, ["z"]=-185.82023620605, ["x"]=-555.06848144531,}, ["id"]="0x1d002ac8", ["contentFile"]="beautiful cities of morrowind.esp", ["recordId"]="misc_inkwell", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=0, ["x"]=0,},
    ["refNum"]=10952,},
    [320]={
    ["position"]={
    ["y"]=72.099731445313, ["z"]=-175.39921569824, ["x"]=-554.04852294922,}, ["id"]="0x1d002ac9", ["contentFile"]="beautiful cities of morrowind.esp", ["recordId"]="misc_quill", ["scale"]=1,
    ["rotation"]={ ["y"]=-0.074999637901783,
    ["z"]=0.075000941753387, ["x"]=-1.4249994754791,}, ["refNum"]=10953,},
    [321]={
    ["position"]={
    ["y"]=52.154132843018, ["z"]=-235.01751708984, ["x"]=-528.67864990234,}, ["id"]="0x1d002aca", ["contentFile"]="beautiful cities of morrowind.esp", ["recordId"]="misc_com_bottle_02", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=0, ["x"]=0,},
    ["refNum"]=10954,},
    [322]={
    ["position"]={
    ["y"]=63.784473419189, ["z"]=-244.84440612793, ["x"]=-520.501953125,}, ["id"]="0x1d002acb", ["contentFile"]="beautiful cities of morrowind.esp", ["recordId"]="misc_de_goblet_06", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=0, ["x"]=0,},
    ["refNum"]=10955,},
    [323]={
    ["position"]={
    ["y"]=-1621.8879394531, ["z"]=-696.64910888672, ["x"]=-212.60844421387,}, ["id"]="0x1d002ad4", ["contentFile"]="beautiful cities of morrowind.esp", ["recordId"]="misc_de_goblet_02", ["scale"]=1,
    ["rotation"]={ ["y"]=-0, ["z"]=0.54999995231628,
    ["x"]=0,}, ["refNum"]=10964,},
    [324]={
    ["position"]={
    ["y"]=-361.84289550781, ["z"]=-683.11730957031, ["x"]=-231.58030700684,}, ["id"]="0x1d002ae3", ["contentFile"]="beautiful cities of morrowind.esp", ["recordId"]="misc_com_redware_platter", ["scale"]=1,
    ["rotation"]={ ["y"]=-0,
    ["z"]=-0.82500004768372, ["x"]=0,}, ["refNum"]=10979,},
    [325]={
    ["position"]={
    ["y"]=-725.26690673828, ["z"]=-632.90936279297, ["x"]=54.78791809082,}, ["id"]="0x1d002ae4", ["contentFile"]="beautiful cities of morrowind.esp", ["recordId"]="misc_com_redware_platter", ["scale"]=1,
    ["rotation"]={ ["y"]=-0,
    ["z"]=-1.200001001358, ["x"]=0,}, ["refNum"]=10980,},
    [326]={
    ["position"]={
    ["y"]=-734.87902832031, ["z"]=-636.14221191406, ["x"]=48.312568664551,}, ["id"]="0x1d002ae5", ["contentFile"]="beautiful cities of morrowind.esp", ["recordId"]="misc_de_bowl_redware_03", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=0, ["x"]=0,},
    ["refNum"]=10981,},
    [327]={
    ["position"]={
    ["y"]=-713.71459960938, ["z"]=-636.14221191406, ["x"]=62.986568450928,}, ["id"]="0x1d002ae6", ["contentFile"]="beautiful cities of morrowind.esp", ["recordId"]="misc_de_bowl_redware_03", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=0, ["x"]=0,},
    ["refNum"]=10982,},
    [328]={
    ["position"]={
    ["y"]=-720.64526367188, ["z"]=-636.14221191406, ["x"]=49.808650970459,}, ["id"]="0x1d002ae7", ["contentFile"]="beautiful cities of morrowind.esp", ["recordId"]="misc_de_bowl_redware_03", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=0, ["x"]=0,},
    ["refNum"]=10983,},
    [329]={
    ["position"]={
    ["y"]=-725.99859619141, ["z"]=-634.96380615234, ["x"]=60.918327331543,}, ["id"]="0x1d002ae8", ["contentFile"]="beautiful cities of morrowind.esp", ["recordId"]="misc_de_pot_redware_04", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=0, ["x"]=0,},
    ["refNum"]=10984,},
    [330]={
    ["position"]={
    ["y"]=-1654.9172363281, ["z"]=-702.88586425781, ["x"]=-284.32696533203,}, ["id"]="0x1d002af3", ["contentFile"]="beautiful cities of morrowind.esp", ["recordId"]="misc_com_wood_fork", ["scale"]=1,
    ["rotation"]={ ["y"]=-0, ["z"]=-0.67499858140945,
    ["x"]=0,}, ["refNum"]=10995,},
    [331]={
    ["position"]={
    ["y"]=-1680.0506591797, ["z"]=-702.71954345703, ["x"]=-262.67965698242,}, ["id"]="0x1d002af4", ["contentFile"]="beautiful cities of morrowind.esp", ["recordId"]="misc_com_wood_knife", ["scale"]=1,
    ["rotation"]={ ["y"]=-0, ["z"]=-1.1249988079071,
    ["x"]=0,}, ["refNum"]=10996,},
    [332]={
    ["position"]={
    ["y"]=-364.96368408203, ["z"]=-685.15393066406, ["x"]=-229.87403869629,}, ["id"]="0x1d002afe", ["contentFile"]="beautiful cities of morrowind.esp", ["recordId"]="misc_de_bowl_redware_02", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=0, ["x"]=0,},
    ["refNum"]=11006,},
    [333]={
    ["position"]={
    ["y"]=-352.59609985352, ["z"]=-673.82385253906, ["x"]=-215.19415283203,}, ["id"]="0x1d002aff", ["contentFile"]="beautiful cities of morrowind.esp", ["recordId"]="misc_com_redware_flask", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=0, ["x"]=0,},
    ["refNum"]=11007,},
    [334]={
    ["position"]={
    ["y"]=-345.14819335938, ["z"]=-673.82385253906, ["x"]=-225.9875793457,}, ["id"]="0x1d002b00", ["contentFile"]="beautiful cities of morrowind.esp", ["recordId"]="misc_com_redware_flask", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=0, ["x"]=0,},
    ["refNum"]=11008,},
    [335]={
    ["position"]={
    ["y"]=-371.22814941406, ["z"]=-684.99670410156, ["x"]=-243.57711791992,}, ["id"]="0x1d002b01", ["contentFile"]="beautiful cities of morrowind.esp", ["recordId"]="misc_soulgem_lesser", ["scale"]=1,
    ["rotation"]={ ["y"]=-0, ["z"]=-0.6000007390976,
    ["x"]=0,}, ["refNum"]=11009,},
    [336]={
    ["position"]={
    ["y"]=-374.58483886719, ["z"]=-684.99670410156, ["x"]=-246.34851074219,}, ["id"]="0x1d002b02", ["contentFile"]="beautiful cities of morrowind.esp", ["recordId"]="misc_soulgem_lesser", ["scale"]=1,
    ["rotation"]={ ["y"]=-0, ["z"]=-0.22500163316727,
    ["x"]=0,}, ["refNum"]=11010,},
    [337]={
    ["position"]={
    ["y"]=-364.4951171875, ["z"]=-681.82537841797, ["x"]=-229.06735229492,}, ["id"]="0x1d002b03", ["contentFile"]="beautiful cities of morrowind.esp", ["recordId"]="misc_soulgem_common", ["scale"]=1,
    ["rotation"]={ ["y"]=-0, ["z"]=1.5750000476837,
    ["x"]=0,}, ["refNum"]=11011,},
    [338]={
    ["position"]={
    ["y"]=-432.46966552734, ["z"]=-687.06231689453, ["x"]=75.824600219727,}, ["id"]="0x1d002b0f", ["contentFile"]="beautiful cities of morrowind.esp", ["recordId"]="misc_de_pot_redware_03", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=0, ["x"]=0,},
    ["refNum"]=11023,},
    [339]={
    ["position"]={
    ["y"]=17.514009475708, ["z"]=65.528144836426, ["x"]=-504.62326049805,}, ["id"]="0x1d002b11", ["contentFile"]="beautiful cities of morrowind.esp", ["recordId"]="misc_lw_platter", ["scale"]=0.80000001192093,
    ["rotation"]={ ["y"]=-0, ["z"]=0,
    ["x"]=0,}, ["refNum"]=11025,},
    [340]={
    ["position"]={
    ["y"]=-326.05477905273, ["z"]=-636.771484375, ["x"]=-126.40937042236,}, ["id"]="0x1d002b12", ["contentFile"]="beautiful cities of morrowind.esp", ["recordId"]="misc_de_pot_redware_04", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=0, ["x"]=0,},
    ["refNum"]=11026,},
    [341]={
    ["position"]={
    ["y"]=-1077.2739257813, ["z"]=-762, ["x"]=-626.99163818359,}, ["id"]="0x101081f", ["contentFile"]="morrowind.esm", ["rotation"]={ ["y"]=-0, ["z"]=0, ["x"]=0,}, ["recordId"]="masalinie merian", ["scale"]=1, ["refNum"]=2079,
    ["inventory"]={
    [1]={
    ["recordId"]="expensive_ring_02", ["count"]=1, ["equipped"]="true",}, [2]={ ["recordId"]="expensive_robe_03", ["count"]=1, ["equipped"]="true",}, [3]={ ["recordId"]="expensive_shoes_03", ["count"]=1, ["equipped"]="true",},},},
    [342]={
    ["position"]={
    ["y"]=-679.64880371094, ["z"]=-762, ["x"]=-75.356880187988,}, ["id"]="0x1010820", ["contentFile"]="morrowind.esm", ["rotation"]={ ["y"]=-0, ["z"]=-2.3561942577362, ["x"]=0,}, ["recordId"]="marayn dren", ["scale"]=1, ["refNum"]=2080,
    ["inventory"]={
    [1]={
    ["recordId"]="extravagant_robe_01_a", ["count"]=1, ["equipped"]="true",}, [2]={ ["recordId"]="expensive_belt_03", ["count"]=1, ["equipped"]="true",}, [3]={ ["recordId"]="expensive_shoes_02", ["count"]=1, ["equipped"]="true",},
    [4]={
    ["recordId"]="gold_001", ["count"]=25,},},},
    [343]={
    ["position"]={
    ["y"]=-1675.9384765625, ["z"]=-762, ["x"]=489.85263061523,}, ["id"]="0x1010821", ["contentFile"]="morrowind.esm", ["rotation"]={ ["y"]=-0, ["z"]=1.570796251297, ["x"]=0,}, ["recordId"]="sharn gra-muzgob", ["scale"]=1, ["refNum"]=2081,
    ["inventory"]={
    [1]={
    ["recordId"]="common_robe_05_b", ["count"]=1, ["equipped"]="true",}, [2]={ ["recordId"]="common_shoes_04", ["count"]=1, ["equipped"]="true",},},},
    [344]={
    ["position"]={
    ["y"]=-745.86004638672, ["z"]=-762, ["x"]=-524.16345214844,}, ["id"]="0x1010822", ["contentFile"]="morrowind.esm", ["rotation"]={ ["y"]=-0, ["z"]=3.0831859111786, ["x"]=0,}, ["recordId"]="ajira", ["scale"]=1, ["refNum"]=2082,
    ["inventory"]={
    [1]={
    ["recordId"]="expensive_amulet_02", ["count"]=1, ["equipped"]="true",}, [2]={ ["recordId"]="common_robe_05_a", ["count"]=1, ["equipped"]="true",}, [3]={ ["recordId"]="extravagant_pants_01", ["count"]=1, ["equipped"]="true",},
    [4]={
    ["recordId"]="ingred_willow_anther_01", ["count"]=5,}, [5]={ ["recordId"]="ingred_scales_01", ["count"]=5,}, [6]={ ["recordId"]="food_kwama_egg_01", ["count"]=5,}, [7]={ ["recordId"]="ingred_kwama_cuttle_01", ["count"]=5,},
    [8]={
    ["recordId"]="ingred_hound_meat_01", ["count"]=5,}, [9]={ ["recordId"]="ingred_heather_01", ["count"]=5,}, [10]={ ["recordId"]="ingred_crab_meat_01", ["count"]=5,}, [11]={ ["recordId"]="ingred_comberry_01", ["count"]=5,},
    [12]={
    ["recordId"]="ingred_black_anther_01", ["count"]=5,},},},
    [345]={
    ["position"]={
    ["y"]=-1145.8579101563, ["z"]=-761.99987792969, ["x"]=-24.059413909912,}, ["id"]="0x1010823", ["contentFile"]="morrowind.esm", ["rotation"]={ ["y"]=-0, ["z"]=-2.6999990940094, ["x"]=0,}, ["recordId"]="estirdalin", ["scale"]=1, ["refNum"]=2083,
    ["inventory"]={
    [1]={
    ["recordId"]="expensive_ring_01", ["count"]=1, ["equipped"]="true",}, [2]={ ["recordId"]="common_robe_05_c", ["count"]=1, ["equipped"]="true",}, [3]={ ["recordId"]="expensive_shoes_01", ["count"]=1, ["equipped"]="true",},
    [4]={
    ["recordId"]="steel spider blade", ["count"]=1, ["equipped"]="true",},},},
    [346]={
    ["position"]={
    ["y"]=-47.351631164551, ["z"]=-250.00032043457, ["x"]=-560.40484619141,}, ["id"]="0x1010824", ["contentFile"]="morrowind.esm", ["rotation"]={ ["y"]=-0, ["z"]=1.4999997615814, ["x"]=0,}, ["recordId"]="ranis athrys", ["scale"]=1, ["refNum"]=2084,
    ["inventory"]={
    [1]={
    ["recordId"]="expensive_ring_02", ["count"]=1, ["equipped"]="true",}, [2]={ ["recordId"]="expensive_robe_02_a", ["count"]=1, ["equipped"]="true",}, [3]={ ["recordId"]="expensive_shoes_01", ["count"]=1, ["equipped"]="true",},
    [4]={
    ["recordId"]="steel tanto", ["count"]=1, ["equipped"]="true",},},},
    [347]={
    ["position"]={
    ["y"]=-105.54742431641, ["z"]=5.9999084472656, ["x"]=-554.59020996094,}, ["id"]="0x1010825", ["contentFile"]="morrowind.esm", ["rotation"]={ ["y"]=-0, ["z"]=1.5299997329712, ["x"]=0,}, ["recordId"]="galbedir", ["scale"]=1, ["refNum"]=2085,
    ["inventory"]={
    [1]={
    ["recordId"]="sc_taldamsscorcher", ["count"]=2,}, [2]={ ["recordId"]="sc_vitality", ["count"]=2,}, [3]={ ["recordId"]="sc_vigor", ["count"]=2,}, [4]={ ["recordId"]="sc_ondusisunhinging", ["count"]=2,},
    [5]={ ["recordId"]="sc_divineintervention",
    ["count"]=2,}, [6]={ ["recordId"]="sc_almsiviintervention", ["count"]=2,}, [7]={ ["recordId"]="sc_fphyggisgemfeeder", ["count"]=2,}, [8]={ ["recordId"]="sc_tinurshoptoad", ["count"]=5,},
    [9]={
    ["recordId"]="sc_drathiswinterguest", ["count"]=2,}, [10]={ ["recordId"]="sc_celerity", ["count"]=2,}, [11]={ ["recordId"]="sc_dedresmasterfuleye", ["count"]=2,}, [12]={ ["recordId"]="sc_salensvivication", ["count"]=2,},
    [13]={
    ["recordId"]="sc_savagemight", ["count"]=2,}, [14]={ ["recordId"]="sc_secondbarrier", ["count"]=2,}, [15]={ ["recordId"]="sc_firstbarrier", ["count"]=2,}, [16]={ ["recordId"]="sc_healing", ["count"]=5,},
    [17]={ ["recordId"]="expensive_amulet_03",
    ["count"]=1, ["equipped"]="true",}, [18]={ ["recordId"]="amulet of stamina", ["count"]=1,}, [19]={ ["recordId"]="doze charm", ["count"]=1,},
    [20]={ ["recordId"]="exquisite_robe_01", ["count"]=1, ["equipped"]="true",}, [21]={ ["recordId"]="expensive_belt_01", ["count"]=1, ["equipped"]="true",}, [22]={ ["recordId"]="common_shoes_04", ["count"]=1, ["equipped"]="true",},
    [23]={
    ["recordId"]="iron sparkskewer", ["count"]=1, ["equipped"]="true",}, [24]={ ["recordId"]="dire viperblade", ["count"]=1,},},},
    [348]={
    ["position"]={
    ["y"]=0, ["z"]=-128, ["x"]=-256,}, ["id"]="0x100a399", ["contentFile"]="morrowind.esm", ["recordId"]="in_hlaalu_hall_3way", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=-3.141592502594, ["x"]=0,}, ["refNum"]=41881,},
    [349]={
    ["position"]={
    ["y"]=0, ["z"]=-128, ["x"]=0,}, ["id"]="0x100a39a", ["contentFile"]="morrowind.esm", ["recordId"]="in_hlaalu_hall_3way", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=-3.141592502594, ["x"]=0,}, ["refNum"]=41882,},
    [350]={
    ["position"]={
    ["y"]=-256, ["z"]=-128, ["x"]=0,}, ["id"]="0x100a3a0", ["contentFile"]="morrowind.esm", ["recordId"]="in_hlaalu_hall_3way", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=0, ["x"]=0,}, ["refNum"]=41888,},
    [351]={
    ["position"]={
    ["y"]=0, ["z"]=128, ["x"]=-256,}, ["id"]="0x100a3a2", ["contentFile"]="morrowind.esm", ["recordId"]="in_hlaalu_hall_3way", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=-3.141592502594, ["x"]=0,}, ["refNum"]=41890,},
    [352]={
    ["position"]={
    ["y"]=0, ["z"]=128, ["x"]=-512,}, ["id"]="0x100a3a4", ["contentFile"]="morrowind.esm", ["recordId"]="in_hlaalu_hall_corner_01", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=1.570796251297, ["x"]=0,}, ["refNum"]=41892,},
    [353]={
    ["position"]={
    ["y"]=0, ["z"]=-128, ["x"]=-512,}, ["id"]="0x100a3a5", ["contentFile"]="morrowind.esm", ["recordId"]="in_hlaalu_hall_corner_01", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=1.570796251297, ["x"]=0,}, ["refNum"]=41893,},
    [354]={
    ["position"]={
    ["y"]=0, ["z"]=-256, ["x"]=256,}, ["id"]="0x100a3a6", ["contentFile"]="morrowind.esm", ["recordId"]="in_hlaalu_hall_stairsr", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=-3.141592502594, ["x"]=0,}, ["refNum"]=41894,},
    [355]={
    ["position"]={
    ["y"]=0, ["z"]=-384, ["x"]=512,}, ["id"]="0x100a3a7", ["contentFile"]="morrowind.esm", ["recordId"]="in_hlaalu_hall_3way", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=-3.141592502594, ["x"]=0,}, ["refNum"]=41895,},
    [356]={
    ["position"]={
    ["y"]=-1024, ["z"]=-640, ["x"]=768,}, ["id"]="0x100a3a8", ["contentFile"]="morrowind.esm", ["recordId"]="in_hlaalu_hall_corner_01", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=-1.570796251297, ["x"]=0,}, ["refNum"]=41896,},
    [357]={
    ["position"]={
    ["y"]=-512, ["z"]=-512, ["x"]=768,}, ["id"]="0x100a3a9", ["contentFile"]="morrowind.esm", ["recordId"]="in_hlaalu_hall_ramp", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=1.570796251297, ["x"]=0,}, ["refNum"]=41897,},
    [358]={
    ["position"]={
    ["y"]=-128, ["z"]=-384, ["x"]=512,}, ["id"]="0x100a3ab", ["contentFile"]="morrowind.esm", ["recordId"]="in_hlaalu_wall", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=-1.570796251297, ["x"]=0,}, ["refNum"]=41899,},
    [359]={
    ["position"]={
    ["y"]=0, ["z"]=-384, ["x"]=768,}, ["id"]="0x100a3ac", ["contentFile"]="morrowind.esm", ["recordId"]="in_hlaalu_hall_corner_01", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=-3.141592502594, ["x"]=0,}, ["refNum"]=41900,},
    [360]={
    ["position"]={
    ["y"]=-256, ["z"]=-384, ["x"]=768,}, ["id"]="0x100a3ad", ["contentFile"]="morrowind.esm", ["recordId"]="in_hlaalu_hall_ramp", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=1.570796251297, ["x"]=0,}, ["refNum"]=41901,},
    [361]={
    ["position"]={
    ["y"]=-1024, ["z"]=-640, ["x"]=-128,}, ["id"]="0x100a3ae", ["contentFile"]="morrowind.esm", ["recordId"]="in_hlaalu_roomt_sided", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=1.570796251297, ["x"]=0,}, ["refNum"]=41902,},
    [362]={
    ["position"]={
    ["y"]=-1536, ["z"]=-640, ["x"]=-128,}, ["id"]="0x100a3af", ["contentFile"]="morrowind.esm", ["recordId"]="in_hlaalu_roomt_corner_01", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=-3.141592502594, ["x"]=0,}, ["refNum"]=41903,},
    [363]={
    ["position"]={
    ["y"]=-128, ["z"]=-384, ["x"]=256,}, ["id"]="0x100a3b1", ["contentFile"]="morrowind.esm", ["recordId"]="in_hlaalu_wall", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=-1.570796251297, ["x"]=0,}, ["refNum"]=41905,},
    [364]={
    ["position"]={
    ["y"]=-512, ["z"]=-640, ["x"]=-128,}, ["id"]="0x100a3b2", ["contentFile"]="morrowind.esm", ["recordId"]="in_hlaalu_roomt_corner_01", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=-1.570796251297, ["x"]=0,}, ["refNum"]=41906,},
    [365]={
    ["position"]={
    ["y"]=-1536, ["z"]=-640, ["x"]=384,}, ["id"]="0x100a3b3", ["contentFile"]="morrowind.esm", ["recordId"]="in_hlaalu_roomt_corner_01", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=1.570796251297, ["x"]=0,}, ["refNum"]=41907,},
    [366]={
    ["position"]={
    ["y"]=-512, ["z"]=-640, ["x"]=384,}, ["id"]="0x100a3b4", ["contentFile"]="morrowind.esm", ["recordId"]="in_hlaalu_roomt_corner_01", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=1.7484555314695e-07, ["x"]=0,}, ["refNum"]=41908,},
    [367]={
    ["position"]={
    ["y"]=-254.50172424316, ["z"]=5.6450958251953, ["x"]=-513.74438476563,}, ["id"]="0x100a3ba", ["contentFile"]="morrowind.esm", ["recordId"]="furn_de_rug_01", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=-1.570796251297, ["x"]=0,}, ["refNum"]=41914,},
    [368]={
    ["position"]={
    ["y"]=-128, ["z"]=128, ["x"]=-256,}, ["id"]="0x100a3bc", ["contentFile"]="morrowind.esm", ["recordId"]="in_hlaalu_room_rail", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=1.7484555314695e-07, ["x"]=0,}, ["refNum"]=41916,},
    [369]={
    ["position"]={
    ["y"]=-1024, ["z"]=-640, ["x"]=384,}, ["id"]="0x100de00", ["contentFile"]="morrowind.esm", ["recordId"]="in_hlaalu_roomt_sided", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=-1.570796251297, ["x"]=0,}, ["refNum"]=56832,},
    [370]={
    ["position"]={
    ["y"]=-1024, ["z"]=-640, ["x"]=-512,}, ["id"]="0x100de01", ["contentFile"]="morrowind.esm", ["recordId"]="in_hlaalu_room_door1", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=-1.570796251297, ["x"]=0,}, ["refNum"]=56833,},
    [371]={
    ["position"]={
    ["y"]=-1024, ["z"]=-640, ["x"]=-768,}, ["id"]="0x100de02", ["contentFile"]="morrowind.esm", ["recordId"]="in_hlaalu_room_corner", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=1.7484555314695e-07, ["x"]=0,}, ["refNum"]=56834,},
    [372]={
    ["position"]={
    ["y"]=-768, ["z"]=-640, ["x"]=-512,}, ["id"]="0x100de03", ["contentFile"]="morrowind.esm", ["recordId"]="in_hlaalu_room_corner", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=-3.141592502594, ["x"]=0,}, ["refNum"]=56835,},
    [373]={
    ["position"]={
    ["y"]=-768, ["z"]=-640, ["x"]=-768,}, ["id"]="0x100de04", ["contentFile"]="morrowind.esm", ["recordId"]="in_hlaalu_room_corner", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=1.570796251297, ["x"]=0,}, ["refNum"]=56836,},
    [374]={
    ["position"]={
    ["y"]=-1024, ["z"]=-640, ["x"]=-384,}, ["id"]="0x100de06", ["contentFile"]="morrowind.esm", ["recordId"]="in_hlaalu_doorjamb", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=1.7484555314695e-07, ["x"]=0,}, ["refNum"]=56838,},
    [375]={
    ["position"]={
    ["y"]=-1.2675100564957, ["z"]=-252.74456787109, ["x"]=-251.17837524414,}, ["id"]="0x100de08", ["contentFile"]="morrowind.esm", ["recordId"]="furn_de_rug_big_09", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=-3.141592502594, ["x"]=0,},
    ["refNum"]=56840,},
    [376]={
    ["position"]={
    ["y"]=46.653930664063, ["z"]=-219.97319030762, ["x"]=-561.12487792969,}, ["id"]="0x100de0a", ["contentFile"]="morrowind.esm", ["recordId"]="furn_de_p_table_05", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=1.570796251297, ["x"]=0,},
    ["refNum"]=56842,},
    [377]={
    ["position"]={
    ["y"]=-120.22648620605, ["z"]=-125.56908416748, ["x"]=-296.23620605469,}, ["id"]="0x100de19", ["contentFile"]="morrowind.esm", ["recordId"]="furn_de_p_shelf_02", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=-3.141592502594, ["x"]=0,},
    ["refNum"]=56857,},
    [378]={
    ["position"]={
    ["y"]=-726.41955566406, ["z"]=-728.90002441406, ["x"]=-770.0419921875,}, ["id"]="0x100de21", ["contentFile"]="morrowind.esm", ["recordId"]="furn_de_p_table_04", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=0, ["x"]=0,}, ["refNum"]=56865,},
    [379]={
    ["position"]={
    ["y"]=-728.8134765625, ["z"]=-689.66583251953, ["x"]=-716.96612548828,}, ["id"]="0x100de2c", ["contentFile"]="morrowind.esm", ["recordId"]="furn_de_tray_01", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=-1.6999986171722, ["x"]=0,},
    ["refNum"]=56876,},
    [380]={
    ["position"]={
    ["y"]=-1742.7600097656, ["z"]=-529.62127685547, ["x"]=281.583984375,}, ["id"]="0x100de34", ["contentFile"]="morrowind.esm", ["recordId"]="furn_de_rope_03", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=-1.5292013883591, ["x"]=0,}, ["refNum"]=56884,},
    [381]={
    ["position"]={
    ["y"]=-1424.6259765625, ["z"]=-529.00036621094, ["x"]=588.89654541016,}, ["id"]="0x100de37", ["contentFile"]="morrowind.esm", ["recordId"]="furn_de_rope_03", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=-0.029203716665506, ["x"]=0,},
    ["refNum"]=56887,},
    [382]={
    ["position"]={
    ["y"]=-615.94256591797, ["z"]=-526.65515136719, ["x"]=589.09783935547,}, ["id"]="0x100de39", ["contentFile"]="morrowind.esm", ["recordId"]="furn_de_rope_03", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=-0.029203716665506, ["x"]=0,},
    ["refNum"]=56889,},
    [383]={
    ["position"]={
    ["y"]=-305.13287353516, ["z"]=-531.81341552734, ["x"]=278.60144042969,}, ["id"]="0x100de3b", ["contentFile"]="morrowind.esm", ["recordId"]="furn_de_rope_03", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=-1.6292026042938, ["x"]=0,},
    ["refNum"]=56891,},
    [384]={
    ["position"]={
    ["y"]=-303.94931030273, ["z"]=-531.78082275391, ["x"]=-19.669178009033,}, ["id"]="0x100de3d", ["contentFile"]="morrowind.esm", ["recordId"]="furn_de_rope_03", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=-1.5292023420334, ["x"]=0,},
    ["refNum"]=56893,},
    [385]={
    ["position"]={
    ["y"]=-608.30181884766, ["z"]=-531.60601806641, ["x"]=-348.11502075195,}, ["id"]="0x100de3f", ["contentFile"]="morrowind.esm", ["recordId"]="furn_de_rope_03", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=3.1241393089294, ["x"]=0,},
    ["refNum"]=56895,},
    [386]={
    ["position"]={
    ["y"]=-1435.2607421875, ["z"]=-534.25842285156, ["x"]=-333.13647460938,}, ["id"]="0x100de41", ["contentFile"]="morrowind.esm", ["recordId"]="furn_de_rope_03", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=-3.1292018890381, ["x"]=0,},
    ["refNum"]=56897,},
    [387]={
    ["position"]={
    ["y"]=-1738.2919921875, ["z"]=-526.09515380859, ["x"]=-13.937036514282,}, ["id"]="0x100de43", ["contentFile"]="morrowind.esm", ["recordId"]="furn_de_rope_03", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=1.5760322809219, ["x"]=0,},
    ["refNum"]=56899,},
    [388]={
    ["position"]={
    ["y"]=-799.25457763672, ["z"]=-733.96752929688, ["x"]=-184.85612487793,}, ["id"]="0x100de4c", ["contentFile"]="morrowind.esm", ["recordId"]="furn_de_r_table_07", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=-3.1415922641754, ["x"]=0,},
    ["refNum"]=56908,},
    [389]={
    ["position"]={
    ["y"]=-735.10266113281, ["z"]=-724.63977050781, ["x"]=-199.60963439941,}, ["id"]="0x100de4d", ["contentFile"]="morrowind.esm", ["recordId"]="furn_de_r_chair_03", ["scale"]=0.88000005483627,
    ["rotation"]={ ["y"]=-0, ["z"]=0.10821077227592, ["x"]=0,}, ["refNum"]=56909,},
    [390]={
    ["position"]={
    ["y"]=-535.10241699219, ["z"]=-733.96752929688, ["x"]=-81.046417236328,}, ["id"]="0x100de51", ["contentFile"]="morrowind.esm", ["recordId"]="furn_de_r_table_07", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=-3.1415922641754, ["x"]=0,},
    ["refNum"]=56913,},
    [391]={
    ["position"]={
    ["y"]=-602.67266845703, ["z"]=-724.63977050781, ["x"]=-57.832317352295,}, ["id"]="0x100de52", ["contentFile"]="morrowind.esm", ["recordId"]="furn_de_r_chair_03", ["scale"]=0.88000005483627,
    ["rotation"]={ ["y"]=-0, ["z"]=-3.1409533023834, ["x"]=0,}, ["refNum"]=56914,},
    [392]={
    ["position"]={
    ["y"]=-469.49633789063, ["z"]=-724.63977050781, ["x"]=-87.355209350586,}, ["id"]="0x100de53", ["contentFile"]="morrowind.esm", ["recordId"]="furn_de_r_chair_03", ["scale"]=0.88000005483627, ["rotation"]={ ["y"]=-0, ["z"]=0, ["x"]=0,},
    ["refNum"]=56915,},
    [393]={
    ["position"]={
    ["y"]=-1681.6555175781, ["z"]=-763.10345458984, ["x"]=120.21214294434,}, ["id"]="0x100de57", ["contentFile"]="morrowind.esm", ["recordId"]="furn_de_r_wallscreen_02", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=1.5707968473434, ["x"]=0,},
    ["refNum"]=56919,},
    [394]={
    ["position"]={
    ["y"]=-1289.3739013672, ["z"]=-763.10345458984, ["x"]=-262.18402099609,}, ["id"]="0x100de58", ["contentFile"]="morrowind.esm", ["recordId"]="furn_de_r_wallscreen_02", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=-3.0199160505617e-07, ["x"]=0,},
    ["refNum"]=56920,},
    [395]={
    ["position"]={
    ["y"]=-1402.6071777344, ["z"]=-763.10345458984, ["x"]=120.21223449707,}, ["id"]="0x100de59", ["contentFile"]="morrowind.esm", ["recordId"]="furn_de_r_wallscreen_02", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=1.5707968473434, ["x"]=0,},
    ["refNum"]=56921,},
    [396]={
    ["position"]={
    ["y"]=-1273.2125244141, ["z"]=-763.10345458984, ["x"]=20.242280960083,}, ["id"]="0x100de5a", ["contentFile"]="morrowind.esm", ["recordId"]="furn_de_r_wallscreen_02", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=-3.0199160505617e-07, ["x"]=0,},
    ["refNum"]=56922,},
    [397]={
    ["position"]={
    ["y"]=-1542.5004882813, ["z"]=-763.10345458984, ["x"]=128.04472351074,}, ["id"]="0x100de5b", ["contentFile"]="morrowind.esm", ["recordId"]="furn_de_r_wallscreen_02", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=1.5707968473434, ["x"]=0,},
    ["refNum"]=56923,},
    [398]={
    ["position"]={
    ["y"]=-533.86614990234, ["z"]=-740.78240966797, ["x"]=529.47552490234,}, ["id"]="0x100de5c", ["contentFile"]="morrowind.esm", ["recordId"]="furn_de_r_bench_01", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=0, ["x"]=0,}, ["refNum"]=56924,},
    [399]={
    ["position"]={
    ["y"]=-540.62646484375, ["z"]=-740.78198242188, ["x"]=216.52386474609,}, ["id"]="0x100de5d", ["contentFile"]="morrowind.esm", ["recordId"]="furn_de_r_bench_01", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=0, ["x"]=0,}, ["refNum"]=56925,},
    [400]={
    ["position"]={
    ["y"]=-652.57122802734, ["z"]=-740.78240966797, ["x"]=216.52359008789,}, ["id"]="0x100de5e", ["contentFile"]="morrowind.esm", ["recordId"]="furn_de_r_bench_01", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=0, ["x"]=0,}, ["refNum"]=56926,},
    [401]={
    ["position"]={
    ["y"]=-644.27386474609, ["z"]=-740.78240966797, ["x"]=529.47552490234,}, ["id"]="0x100de5f", ["contentFile"]="morrowind.esm", ["recordId"]="furn_de_r_bench_01", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=0, ["x"]=0,}, ["refNum"]=56927,},
    [402]={
    ["position"]={
    ["y"]=-287.73489379883, ["z"]=-682.27880859375, ["x"]=511.22692871094,}, ["id"]="0x100de60", ["contentFile"]="morrowind.esm", ["recordId"]="furn_de_r_shelf_01", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=0, ["x"]=0,}, ["refNum"]=56928,},
    [403]={
    ["position"]={
    ["y"]=-287.73489379883, ["z"]=-681.78826904297, ["x"]=256.22372436523,}, ["id"]="0x100de61", ["contentFile"]="morrowind.esm", ["recordId"]="furn_de_r_shelf_01", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=0, ["x"]=0,}, ["refNum"]=56929,},
    [404]={
    ["position"]={
    ["y"]=-782.49731445313, ["z"]=-725.75720214844, ["x"]=-452.35397338867,}, ["id"]="0x100de6a", ["contentFile"]="morrowind.esm", ["recordId"]="furn_de_p_chair_01", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=1.0000001192093, ["x"]=0,},
    ["refNum"]=56938,},
    [405]={
    ["position"]={
    ["y"]=-839.51080322266, ["z"]=-725.75720214844, ["x"]=-783.40087890625,}, ["id"]="0x100de6b", ["contentFile"]="morrowind.esm", ["recordId"]="furn_de_p_chair_01", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=0.78539824485779, ["x"]=0,},
    ["refNum"]=56939,},
    [406]={
    ["position"]={
    ["y"]=-738.01202392578, ["z"]=-763.10302734375, ["x"]=514.68402099609,}, ["id"]="0x100de70", ["contentFile"]="morrowind.esm", ["recordId"]="furn_de_r_wallscreen_02", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=-2.7925267219543, ["x"]=0,},
    ["refNum"]=56944,},
    [407]={
    ["position"]={
    ["y"]=-744.00500488281, ["z"]=-763.10302734375, ["x"]=235.92199707031,}, ["id"]="0x100de71", ["contentFile"]="morrowind.esm", ["recordId"]="furn_de_r_wallscreen_02", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=2.7925267219543, ["x"]=0,},
    ["refNum"]=56945,},
    [408]={
    ["position"]={
    ["y"]=-644.15979003906, ["z"]=-763.10345458984, ["x"]=133.54585266113,}, ["id"]="0x100de72", ["contentFile"]="morrowind.esm", ["recordId"]="furn_de_r_wallscreen_02", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=-1.570796251297, ["x"]=0,},
    ["refNum"]=56946,},
    [409]={
    ["position"]={
    ["y"]=-509.60940551758, ["z"]=-763.10345458984, ["x"]=125.24740600586,}, ["id"]="0x100de73", ["contentFile"]="morrowind.esm", ["recordId"]="furn_de_r_wallscreen_02", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=-1.570796251297, ["x"]=0,},
    ["refNum"]=56947,},
    [410]={
    ["position"]={
    ["y"]=-371.92178344727, ["z"]=-763.10345458984, ["x"]=133.54585266113,}, ["id"]="0x100de74", ["contentFile"]="morrowind.esm", ["recordId"]="furn_de_r_wallscreen_02", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=-1.570796251297, ["x"]=0,},
    ["refNum"]=56948,},
    [411]={
    ["position"]={
    ["y"]=-1561.4122314453, ["z"]=-732.89129638672, ["x"]=272.57662963867,}, ["id"]="0x100de75", ["contentFile"]="morrowind.esm", ["recordId"]="furn_de_r_table_07", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=-3.1415922641754, ["x"]=0,},
    ["refNum"]=56949,},
    [412]={
    ["position"]={
    ["y"]=-1402.1307373047, ["z"]=-724.64001464844, ["x"]=360.25442504883,}, ["id"]="0x100de76", ["contentFile"]="morrowind.esm", ["recordId"]="furn_de_r_chair_03", ["scale"]=0.88000005483627,
    ["rotation"]={ ["y"]=-0, ["z"]=2.7665920257568, ["x"]=0,}, ["refNum"]=56950,},
    [413]={
    ["position"]={
    ["y"]=-1329.1363525391, ["z"]=-732.89129638672, ["x"]=273.31176757813,}, ["id"]="0x100de7a", ["contentFile"]="morrowind.esm", ["recordId"]="furn_de_r_table_07", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=-3.1415922641754, ["x"]=0,},
    ["refNum"]=56954,},
    [414]={
    ["position"]={
    ["y"]=-1393.2883300781, ["z"]=-724.63977050781, ["x"]=271.78411865234,}, ["id"]="0x100de7b", ["contentFile"]="morrowind.esm", ["recordId"]="furn_de_r_chair_03", ["scale"]=0.88000005483627,
    ["rotation"]={ ["y"]=-0, ["z"]=-3.0410764217377, ["x"]=0,}, ["refNum"]=56955,},
    [415]={
    ["position"]={
    ["y"]=-1266.4586181641, ["z"]=-724.63977050781, ["x"]=274.69259643555,}, ["id"]="0x100de7c", ["contentFile"]="morrowind.esm", ["recordId"]="furn_de_r_chair_03", ["scale"]=0.88000005483627, ["rotation"]={ ["y"]=-0, ["z"]=0, ["x"]=0,},
    ["refNum"]=56956,},
    [416]={
    ["position"]={
    ["y"]=-1639.7525634766, ["z"]=-682.66107177734, ["x"]=572.25628662109,}, ["id"]="0x100de7f", ["contentFile"]="morrowind.esm", ["recordId"]="furn_de_r_bookshelf_02", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=-1.570796251297, ["x"]=0,},
    ["refNum"]=56959,},
    [417]={
    ["position"]={
    ["y"]=-1407.6976318359, ["z"]=-655.78607177734, ["x"]=607.61169433594,}, ["id"]="0x100de80", ["contentFile"]="morrowind.esm", ["recordId"]="furn_de_r_shelf_01", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=1.570796251297, ["x"]=0,},
    ["refNum"]=56960,},
    [418]={
    ["position"]={
    ["y"]=-1504.8355712891, ["z"]=-765.12872314453, ["x"]=-140.14758300781,}, ["id"]="0x100de83", ["contentFile"]="morrowind.esm", ["recordId"]="furn_de_rug_big_09", ["scale"]=1.3800001144409,
    ["rotation"]={ ["y"]=-0, ["z"]=-1.570796251297, ["x"]=0,}, ["refNum"]=56963,},
    [419]={
    ["position"]={
    ["y"]=-1636.3356933594, ["z"]=-730.98699951172, ["x"]=-239.62963867188,}, ["id"]="0x100de84", ["contentFile"]="morrowind.esm", ["recordId"]="furn_de_p_table_01", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=0, ["x"]=0,}, ["refNum"]=56964,},
    [420]={
    ["position"]={
    ["y"]=-1677.0938720703, ["z"]=-722.51220703125, ["x"]=-182.10656738281,}, ["id"]="0x100de85", ["contentFile"]="morrowind.esm", ["recordId"]="furn_de_p_chair_01", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=1.970796585083, ["x"]=0,},
    ["refNum"]=56965,},
    [421]={
    ["position"]={
    ["y"]=-1695.6846923828, ["z"]=-722.51220703125, ["x"]=-304.67156982422,}, ["id"]="0x100de86", ["contentFile"]="morrowind.esm", ["recordId"]="furn_de_p_chair_01", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=-2.3561947345734, ["x"]=0,},
    ["refNum"]=56966,},
    [422]={
    ["position"]={
    ["y"]=-1574.7016601563, ["z"]=-722.51220703125, ["x"]=-274.9072265625,}, ["id"]="0x100de87", ["contentFile"]="morrowind.esm", ["recordId"]="furn_de_p_chair_01", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=0, ["x"]=0,}, ["refNum"]=56967,},
    [423]={
    ["position"]={
    ["y"]=-389.67526245117, ["z"]=-764.25506591797, ["x"]=376.57885742188,}, ["id"]="0x100de8f", ["contentFile"]="morrowind.esm", ["recordId"]="furn_de_rug_big_09", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=0, ["x"]=0,}, ["refNum"]=56975,},
    [424]={
    ["position"]={
    ["y"]=-256, ["z"]=0, ["x"]=-256,}, ["id"]="0x100dec0", ["contentFile"]="morrowind.esm", ["recordId"]="in_hlaalu_hall_stairsl", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=-3.141592502594, ["x"]=0,}, ["refNum"]=57024,},
    [425]={
    ["position"]={
    ["y"]=-431.81243896484, ["z"]=-705.55596923828, ["x"]=375.72573852539,}, ["id"]="0x100fac1", ["contentFile"]="morrowind.esm", ["recordId"]="furn_de_lecturn", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=-3.141592502594, ["x"]=0,}, ["refNum"]=64193,},
    [426]={
    ["position"]={
    ["y"]=-892.91595458984, ["z"]=-764.83032226563, ["x"]=-627.53631591797,}, ["id"]="0x104bdf4", ["contentFile"]="morrowind.esm", ["recordId"]="furn_de_rug_big_09", ["scale"]=1.3400000333786,
    ["rotation"]={ ["y"]=-0, ["z"]=0.78539824485779, ["x"]=0,}, ["refNum"]=48628,},
    [427]={
    ["position"]={
    ["y"]=-256, ["z"]=-128, ["x"]=-512,}, ["id"]="0x105c5d0", ["contentFile"]="morrowind.esm", ["recordId"]="in_hlaalu_hall_center", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=0, ["x"]=0,}, ["refNum"]=50640,},
    [428]={
    ["position"]={
    ["y"]=-256, ["z"]=-128, ["x"]=256,}, ["id"]="0x105c5d1", ["contentFile"]="morrowind.esm", ["recordId"]="in_hlaalu_hall_corner_01", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=-1.570796251297, ["x"]=0,}, ["refNum"]=50641,},
    [429]={
    ["position"]={
    ["y"]=-128, ["z"]=-128, ["x"]=256,}, ["id"]="0x105c5d2", ["contentFile"]="morrowind.esm", ["recordId"]="in_hlaalu_hall_rail", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=0, ["x"]=0,}, ["refNum"]=50642,},
    [430]={
    ["position"]={
    ["y"]=-768, ["z"]=-640, ["x"]=128,}, ["id"]="0x105c5d5", ["contentFile"]="morrowind.esm", ["recordId"]="in_hlaalu_roomt_post", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=0, ["x"]=0,}, ["refNum"]=50645,},
    [431]={
    ["position"]={
    ["y"]=-1280, ["z"]=-640, ["x"]=128,}, ["id"]="0x105c5d6", ["contentFile"]="morrowind.esm", ["recordId"]="in_hlaalu_roomt_post", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=0, ["x"]=0,}, ["refNum"]=50646,},
    [432]={
    ["position"]={
    ["y"]=-1024, ["z"]=-808.05554199219, ["x"]=128,}, ["id"]="0x105c5d7", ["contentFile"]="morrowind.esm", ["recordId"]="furn_planter_02", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=0, ["x"]=0,}, ["refNum"]=50647,},
    [433]={
    ["position"]={
    ["y"]=-1025.0294189453, ["z"]=-755.73229980469, ["x"]=128.107421875,}, ["id"]="0x105c5d8", ["contentFile"]="morrowind.esm", ["recordId"]="in_hlaalu_platform_01", ["scale"]=0.54000002145767, ["rotation"]={ ["y"]=-0, ["z"]=0, ["x"]=0,},
    ["refNum"]=50648,},
    [434]={
    ["position"]={
    ["y"]=-1021.3717651367, ["z"]=-638.08123779297, ["x"]=130.64178466797,}, ["id"]="0x105c5d9", ["contentFile"]="morrowind.esm", ["recordId"]="flora_tree_ai_03", ["scale"]=0.5, ["rotation"]={ ["y"]=-0, ["z"]=0, ["x"]=0,}, ["refNum"]=50649,},
    [435]={
    ["position"]={
    ["y"]=-1000.0656738281, ["z"]=-699.65228271484, ["x"]=163.97470092773,}, ["id"]="0x105c5da", ["contentFile"]="morrowind.esm", ["recordId"]="flora_bush_01", ["scale"]=0.5, ["rotation"]={ ["y"]=-0, ["z"]=0.01460075378418, ["x"]=0,},
    ["refNum"]=50650,},
    [436]={
    ["position"]={
    ["y"]=-1001.3406982422, ["z"]=-699.08538818359, ["x"]=95.832794189453,}, ["id"]="0x105c5db", ["contentFile"]="morrowind.esm", ["recordId"]="flora_bush_01", ["scale"]=0.5, ["rotation"]={ ["y"]=-0, ["z"]=-1.7853978872299, ["x"]=0,},
    ["refNum"]=50651,},
    [437]={
    ["position"]={
    ["y"]=-1062.8199462891, ["z"]=-707.12835693359, ["x"]=129.11209106445,}, ["id"]="0x105c5dc", ["contentFile"]="morrowind.esm", ["recordId"]="flora_bush_01", ["scale"]=0.5, ["rotation"]={ ["y"]=-0, ["z"]=1.6146010160446, ["x"]=0,}, ["refNum"]=50652,},
    [438]={
    ["position"]={
    ["y"]=-1041.0377197266, ["z"]=-713.08898925781, ["x"]=88.606384277344,}, ["id"]="0x105c5dd", ["contentFile"]="morrowind.esm", ["recordId"]="flora_grass_01", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=1, ["x"]=0,}, ["refNum"]=50653,},
    [439]={
    ["position"]={
    ["y"]=-1033.1920166016, ["z"]=-721.53259277344, ["x"]=156.27452087402,}, ["id"]="0x105c5de", ["contentFile"]="morrowind.esm", ["recordId"]="flora_grass_01", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=0.80000001192093, ["x"]=0,}, ["refNum"]=50654,},
    [440]={
    ["position"]={
    ["y"]=-986.55950927734, ["z"]=-713.58746337891, ["x"]=121.76184844971,}, ["id"]="0x105c5df", ["contentFile"]="morrowind.esm", ["recordId"]="flora_grass_01", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=2.9802322387695e-08, ["x"]=0,},
    ["refNum"]=50655,},
    [441]={
    ["position"]={
    ["y"]=-384, ["z"]=-128, ["x"]=-512,}, ["id"]="0x105c5e2", ["contentFile"]="morrowind.esm", ["recordId"]="in_hlaalu_doorjamb_load", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=1.570796251297, ["x"]=0,}, ["refNum"]=50658,},
    [442]={
    ["position"]={
    ["y"]=0, ["z"]=128, ["x"]=-128,}, ["id"]="0x105c5e3", ["contentFile"]="morrowind.esm", ["recordId"]="in_hlaalu_doorjamb_load", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=1.7484555314695e-07, ["x"]=0,}, ["refNum"]=50659,},
    [443]={
    ["position"]={
    ["y"]=-384, ["z"]=128, ["x"]=-512,}, ["id"]="0x105c5e4", ["contentFile"]="morrowind.esm", ["recordId"]="in_hlaalu_doorjamb_load", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=1.570796251297, ["x"]=0,}, ["refNum"]=50660,},
    [444]={
    ["position"]={
    ["y"]=-768, ["z"]=-640, ["x"]=768,}, ["id"]="0x105c5e7", ["contentFile"]="morrowind.esm", ["recordId"]="in_hlaalu_hall_center", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=0, ["x"]=0,}, ["refNum"]=50663,},
    [445]={
    ["position"]={
    ["y"]=-1002.7947998047, ["z"]=-521.13262939453, ["x"]=748.76489257813,}, ["id"]="0x105c5e8", ["contentFile"]="morrowind.esm", ["recordId"]="furn_de_rope_03", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=-2.370795249939, ["x"]=0,}, ["refNum"]=50664,},
    [446]={
    ["position"]={
    ["y"]=26.200393676758, ["z"]=-280.9384765625, ["x"]=793.76324462891,}, ["id"]="0x105c5e9", ["contentFile"]="morrowind.esm", ["recordId"]="furn_de_rope_03", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=-0.39999946951866, ["x"]=0,}, ["refNum"]=50665,},
    [447]={
    ["position"]={
    ["y"]=-1129.0659179688, ["z"]=-551.87103271484, ["x"]=-509.70709228516,}, ["id"]="0x1074e12", ["contentFile"]="morrowind.esm", ["recordId"]="furn_de_tapestry_07", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=1.7484555314695e-07, ["x"]=0,},
    ["refNum"]=19986,},
    [448]={
    ["position"]={
    ["y"]=-783.77386474609, ["z"]=-551.87103271484, ["x"]=-871.82763671875,}, ["id"]="0x1074e13", ["contentFile"]="morrowind.esm", ["recordId"]="furn_de_tapestry_07", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=1.5707960128784, ["x"]=0,},
    ["refNum"]=19987,},
    [449]={
    ["position"]={
    ["y"]=-662.52966308594, ["z"]=-551.87103271484, ["x"]=-763.00231933594,}, ["id"]="0x1074e14", ["contentFile"]="morrowind.esm", ["recordId"]="furn_de_tapestry_07", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=-3.141592502594, ["x"]=0,},
    ["refNum"]=19988,},
    [450]={
    ["position"]={
    ["y"]=-662.52966308594, ["z"]=-551.87103271484, ["x"]=-521.76947021484,}, ["id"]="0x1074e15", ["contentFile"]="morrowind.esm", ["recordId"]="furn_de_tapestry_07", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=-3.141592502594, ["x"]=0,},
    ["refNum"]=19989,},
    [451]={
    ["position"]={
    ["y"]=-1025.0059814453, ["z"]=-551.87103271484, ["x"]=-871.82763671875,}, ["id"]="0x1074e16", ["contentFile"]="morrowind.esm", ["recordId"]="furn_de_tapestry_07", ["scale"]=0.87000006437302,
    ["rotation"]={ ["y"]=-0, ["z"]=1.5707960128784, ["x"]=0,}, ["refNum"]=19990,},
    [452]={
    ["position"]={
    ["y"]=-1129.0659179688, ["z"]=-551.87103271484, ["x"]=-758.00244140625,}, ["id"]="0x1074e17", ["contentFile"]="morrowind.esm", ["recordId"]="furn_de_tapestry_07", ["scale"]=0.87000006437302,
    ["rotation"]={ ["y"]=-0, ["z"]=1.7484555314695e-07,
    ["x"]=0,}, ["refNum"]=19991,},
    [453]={
    ["position"]={
    ["y"]=-258.93139648438, ["z"]=216.1600189209, ["x"]=-613.03283691406,}, ["id"]="0x1074e18", ["contentFile"]="morrowind.esm", ["recordId"]="furn_de_tapestry_07", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=1.570796251297, ["x"]=0,},
    ["refNum"]=19992,},
    [454]={
    ["position"]={
    ["y"]=-256.18954467773, ["z"]=-41.647792816162, ["x"]=-405.00872802734,}, ["id"]="0x1074e19", ["contentFile"]="morrowind.esm", ["recordId"]="furn_de_tapestry_07", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=-1.570796251297, ["x"]=0,},
    ["refNum"]=19993,},
    [455]={
    ["position"]={
    ["y"]=-358.4489440918, ["z"]=216.1600189209, ["x"]=-262.62442016602,}, ["id"]="0x1074e1a", ["contentFile"]="morrowind.esm", ["recordId"]="furn_de_tapestry_07", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=-3.0199160505617e-07, ["x"]=0,},
    ["refNum"]=19994,},
    [456]={
    ["position"]={
    ["y"]=-250.91268920898, ["z"]=216.1600189209, ["x"]=-150.73921203613,}, ["id"]="0x1074e1b", ["contentFile"]="morrowind.esm", ["recordId"]="furn_de_tapestry_07", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=-1.570796251297, ["x"]=0,},
    ["refNum"]=19995,},
    [457]={
    ["position"]={
    ["y"]=96.104721069336, ["z"]=208.98513793945, ["x"]=-260.4508972168,}, ["id"]="0x1074e1c", ["contentFile"]="morrowind.esm", ["recordId"]="furn_c_t_wizard_01", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=-3.141592502594, ["x"]=0,},
    ["refNum"]=19996,},
    [458]={
    ["position"]={
    ["y"]=22.527338027954, ["z"]=242.10266113281, ["x"]=-222.78137207031,}, ["id"]="0x1074e1e", ["contentFile"]="morrowind.esm", ["recordId"]="furn_de_rope_03", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=-0.21460086107254, ["x"]=0,},
    ["refNum"]=19998,},
    [459]={
    ["position"]={
    ["y"]=-4.0131182670593, ["z"]=-41.647792816162, ["x"]=-610.6689453125,}, ["id"]="0x1074e1f", ["contentFile"]="morrowind.esm", ["recordId"]="furn_de_tapestry_07", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=1.570796251297, ["x"]=0,},
    ["refNum"]=19999,},
    [460]={
    ["position"]={
    ["y"]=-307.15808105469, ["z"]=-17.079719543457, ["x"]=-507.98193359375,}, ["id"]="0x1074e22", ["contentFile"]="morrowind.esm", ["recordId"]="furn_de_rope_03", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=-0.99999839067459, ["x"]=0,},
    ["refNum"]=20002,},
    [461]={
    ["position"]={
    ["y"]=-256.18954467773, ["z"]=-41.647792816162, ["x"]=-610.6689453125,}, ["id"]="0x1074e23", ["contentFile"]="morrowind.esm", ["recordId"]="furn_de_tapestry_07", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=1.570796251297, ["x"]=0,},
    ["refNum"]=20003,},
    [462]={
    ["position"]={
    ["y"]=-58.546039581299, ["z"]=-11.28870677948, ["x"]=8.5522994995117,}, ["id"]="0x1074e25", ["contentFile"]="morrowind.esm", ["recordId"]="furn_de_rope_03", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=1.9415932893753, ["x"]=0,}, ["refNum"]=20005,},
    [463]={
    ["position"]={
    ["y"]=-248.06454467773, ["z"]=-42.359687805176, ["x"]=364.53167724609,}, ["id"]="0x1074e26", ["contentFile"]="morrowind.esm", ["recordId"]="furn_de_tapestry_07", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=-1.570796251297, ["x"]=0,},
    ["refNum"]=20006,},
    [464]={
    ["position"]={
    ["y"]=103.52300262451, ["z"]=216.1600189209, ["x"]=-501.14764404297,}, ["id"]="0x1074e27", ["contentFile"]="morrowind.esm", ["recordId"]="furn_de_tapestry_07", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=-3.141592502594, ["x"]=0,},
    ["refNum"]=20007,},
    [465]={
    ["position"]={
    ["y"]=-4.0131182670593, ["z"]=216.1600189209, ["x"]=-613.03283691406,}, ["id"]="0x1074e28", ["contentFile"]="morrowind.esm", ["recordId"]="furn_de_tapestry_07", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=1.570796251297, ["x"]=0,},
    ["refNum"]=20008,},
    [466]={
    ["position"]={
    ["y"]=103.88195037842, ["z"]=-42.359687805176, ["x"]=-255.56636047363,}, ["id"]="0x1074e29", ["contentFile"]="morrowind.esm", ["recordId"]="furn_de_tapestry_07", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=-3.141592502594, ["x"]=0,},
    ["refNum"]=20009,},
    [467]={
    ["position"]={
    ["y"]=-361.16882324219, ["z"]=-42.359687805176, ["x"]=0.33826768398285,}, ["id"]="0x1074e2a", ["contentFile"]="morrowind.esm", ["recordId"]="furn_de_tapestry_07", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=-3.0199160505617e-07, ["x"]=0,},
    ["refNum"]=20010,},
    [468]={
    ["position"]={
    ["y"]=-2.917799949646, ["z"]=-42.359687805176, ["x"]=364.53167724609,}, ["id"]="0x1074e2b", ["contentFile"]="morrowind.esm", ["recordId"]="furn_de_tapestry_07", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=-1.570796251297, ["x"]=0,},
    ["refNum"]=20011,},
    [469]={
    ["position"]={
    ["y"]=-361.16882324219, ["z"]=-42.359687805176, ["x"]=253.81237792969,}, ["id"]="0x1074e2d", ["contentFile"]="morrowind.esm", ["recordId"]="furn_de_tapestry_07", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=-3.0199160505617e-07, ["x"]=0,},
    ["refNum"]=20013,},
    [470]={
    ["position"]={
    ["y"]=103.88195037842, ["z"]=-42.359687805176, ["x"]=-5.2477588653564,}, ["id"]="0x1074e2e", ["contentFile"]="morrowind.esm", ["recordId"]="furn_de_tapestry_07", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=-3.141592502594, ["x"]=0,},
    ["refNum"]=20014,},
    [471]={
    ["position"]={
    ["y"]=103.88195037842, ["z"]=-42.359687805176, ["x"]=248.22639465332,}, ["id"]="0x1074e2f", ["contentFile"]="morrowind.esm", ["recordId"]="furn_de_tapestry_07", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=-3.141592502594, ["x"]=0,},
    ["refNum"]=20015,},
    [472]={
    ["position"]={
    ["y"]=-1065.4578857422, ["z"]=-746.70001220703, ["x"]=-809.47485351563,}, ["id"]="0x10753f4", ["contentFile"]="morrowind.esm", ["recordId"]="in_hlaalu_platform_01", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=-0.78539788722992, ["x"]=0,},
    ["refNum"]=21492,},
    [473]={
    ["position"]={
    ["y"]=-1090.3430175781, ["z"]=-531.28985595703, ["x"]=-835.98345947266,}, ["id"]="0x10753f6", ["contentFile"]="morrowind.esm", ["recordId"]="furn_de_rope_03", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=2.3561944961548, ["x"]=0,},
    ["refNum"]=21494,},
    [474]={
    ["position"]={
    ["y"]=-1102.3685302734, ["z"]=-691.49859619141, ["x"]=-846.69268798828,}, ["id"]="0x10753f7", ["contentFile"]="morrowind.esm", ["recordId"]="flora_grass_01", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=0.80000001192093, ["x"]=0,},
    ["refNum"]=21495,},
    [475]={
    ["position"]={
    ["y"]=-1103.6748046875, ["z"]=-730.36071777344, ["x"]=-844.38275146484,}, ["id"]="0x10753f8", ["contentFile"]="morrowind.esm", ["recordId"]="furn_planter_01", ["scale"]=0.5, ["rotation"]={ ["y"]=-0, ["z"]=0, ["x"]=0,}, ["refNum"]=21496,},
    [476]={
    ["position"]={
    ["y"]=-995.15667724609, ["z"]=-713.80804443359, ["x"]=-855.72869873047,}, ["id"]="0x10753f9", ["contentFile"]="morrowind.esm", ["recordId"]="furn_de_railing_04", ["scale"]=0.7600000500679,
    ["rotation"]={ ["y"]=-0, ["z"]=-1.570796251297, ["x"]=0,}, ["refNum"]=21497,},
    [477]={
    ["position"]={
    ["y"]=-1110.7747802734, ["z"]=-713.80804443359, ["x"]=-734.97503662109,}, ["id"]="0x10753fa", ["contentFile"]="morrowind.esm", ["recordId"]="furn_de_railing_04", ["scale"]=0.7600000500679,
    ["rotation"]={ ["y"]=-0, ["z"]=-3.141592502594, ["x"]=0,}, ["refNum"]=21498,},
    [478]={
    ["position"]={
    ["y"]=-1078.4343261719, ["z"]=-713.80804443359, ["x"]=-819.86444091797,}, ["id"]="0x10753fb", ["contentFile"]="morrowind.esm", ["recordId"]="furn_de_railing_04", ["scale"]=0.7600000500679,
    ["rotation"]={ ["y"]=-0, ["z"]=-2.3561942577362, ["x"]=0,}, ["refNum"]=21499,},
    [479]={
    ["position"]={
    ["y"]=-944.24884033203, ["z"]=-718.41156005859, ["x"]=-855.98553466797,}, ["id"]="0x10753fc", ["contentFile"]="morrowind.esm", ["recordId"]="furn_de_railing_06", ["scale"]=0.7600000500679, ["rotation"]={ ["y"]=-0, ["z"]=0, ["x"]=0,},
    ["refNum"]=21500,},
    [480]={
    ["position"]={
    ["y"]=-771.02984619141, ["z"]=-551.87103271484, ["x"]=-405.90194702148,}, ["id"]="0x1075811", ["contentFile"]="morrowind.esm", ["recordId"]="furn_de_tapestry_07", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=-1.570796251297, ["x"]=0,},
    ["refNum"]=22545,},
    [481]={
    ["position"]={
    ["y"]=-703.1796875, ["z"]=-692.68975830078, ["x"]=-442.53961181641,}, ["id"]="0x1075812", ["contentFile"]="morrowind.esm", ["recordId"]="flora_grass_01", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=0.80000001192093, ["x"]=0,}, ["refNum"]=22546,},
    [482]={
    ["position"]={
    ["y"]=-704.48596191406, ["z"]=-731.55194091797, ["x"]=-440.22967529297,}, ["id"]="0x1075813", ["contentFile"]="morrowind.esm", ["recordId"]="furn_planter_01", ["scale"]=0.5, ["rotation"]={ ["y"]=-0, ["z"]=0, ["x"]=0,}, ["refNum"]=22547,},
    [483]={
    ["position"]={
    ["y"]=-256, ["z"]=128, ["x"]=-512,}, ["id"]="0x100a39d", ["contentFile"]="morrowind.esm", ["recordId"]="^_mgrd_bal_i", ["scale"]=1, ["rotation"]={ ["y"]=-0, ["z"]=1.570796251297, ["x"]=0,}, ["refNum"]=41885,},
    [484]={
    ["position"]={
    ["y"]=-273.96975708008, ["z"]=450.53546142578, ["x"]=-520.07952880859,}, ["id"]="0x1d002a48", ["contentFile"]="beautiful cities of morrowind.esp", ["recordId"]="furn_de_rope1_01", ["scale"]=1,
    ["rotation"]={ ["y"]=-0, ["z"]=-0.24665673077106,
    ["x"]=0,}, ["refNum"]=10824,},
    [485]={
    ["position"]={
    ["y"]=-686.42199707031, ["z"]=-682.66143798828, ["x"]=66.831100463867,}, ["id"]="0x1d002a4a", ["contentFile"]="beautiful cities of morrowind.esp", ["recordId"]="furn_de_r_bookshelf_02", ["scale"]=1,
    ["rotation"]={ ["y"]=-0,
    ["z"]=-1.3500002622604, ["x"]=0,}, ["refNum"]=10826,},
    [486]={
    ["position"]={
    ["y"]=-604.40972900391, ["z"]=-682.412109375, ["x"]=-313.1809387207,}, ["id"]="0x1d002a4b", ["contentFile"]="beautiful cities of morrowind.esp", ["recordId"]="furn_de_r_bookshelf_02", ["scale"]=1,
    ["rotation"]={ ["y"]=-0, ["z"]=1.5581849813461,
    ["x"]=0,}, ["refNum"]=10827,},
    [487]={
    ["position"]={
    ["y"]=-401.13922119141, ["z"]=-682.66143798828, ["x"]=-266.36059570313,}, ["id"]="0x1d002a4c", ["contentFile"]="beautiful cities of morrowind.esp", ["recordId"]="furn_de_r_bookshelf_02", ["scale"]=1,
    ["rotation"]={ ["y"]=-0,
    ["z"]=2.2331852912903, ["x"]=0,}, ["refNum"]=10828,},
    [488]={
    ["position"]={
    ["y"]=-326.31793212891, ["z"]=-682.66143798828, ["x"]=-76.704040527344,}, ["id"]="0x1d002a4d", ["contentFile"]="beautiful cities of morrowind.esp", ["recordId"]="furn_de_r_bookshelf_02", ["scale"]=1,
    ["rotation"]={ ["y"]=-0,
    ["z"]=3.1331853866577, ["x"]=0,}, ["refNum"]=10829,},
    [489]={
    ["position"]={
    ["y"]=-866.07403564453, ["z"]=-724.63977050781, ["x"]=-177.4427947998,}, ["id"]="0x1d002ac0", ["contentFile"]="beautiful cities of morrowind.esp", ["recordId"]="furn_de_r_chair_03", ["scale"]=0.88000005483627,
    ["rotation"]={ ["y"]=-0,
    ["z"]=-3.0417890548706, ["x"]=0,}, ["refNum"]=10944,},
    [490]={
    ["position"]={
    ["y"]=-849.61651611328, ["z"]=-724.63977050781, ["x"]=-290.17169189453,}, ["id"]="0x1d002ac1", ["contentFile"]="beautiful cities of morrowind.esp", ["recordId"]="furn_de_r_chair_03", ["scale"]=0.88000005483627,
    ["rotation"]={ ["y"]=-0,
    ["z"]=-2.5167887210846, ["x"]=0,}, ["refNum"]=10945,},
    [491]={
    ["position"]={
    ["y"]=55.700271606445, ["z"]=-212.24586486816, ["x"]=-483.72415161133,}, ["id"]="0x1d002ac4", ["contentFile"]="beautiful cities of morrowind.esp", ["recordId"]="furn_de_r_chair_03", ["scale"]=0.88000005483627,
    ["rotation"]={ ["y"]=-0,
    ["z"]=-0.11678917706013, ["x"]=0,}, ["refNum"]=10948,},
    [492]={
    ["position"]={
    ["y"]=-1624.1979980469, ["z"]=-724.64001464844, ["x"]=273.63497924805,}, ["id"]="0x1d002acc", ["contentFile"]="beautiful cities of morrowind.esp", ["recordId"]="furn_de_r_chair_03", ["scale"]=0.88000005483627,
    ["rotation"]={ ["y"]=-0,
    ["z"]=-3.141592502594, ["x"]=0,}, ["refNum"]=10956,},
    [493]={
    ["position"]={
    ["y"]=-1630.3370361328, ["z"]=-724.64001464844, ["x"]=199.68237304688,}, ["id"]="0x1d002acd", ["contentFile"]="beautiful cities of morrowind.esp", ["recordId"]="furn_de_r_chair_03", ["scale"]=0.88000005483627,
    ["rotation"]={ ["y"]=-0,
    ["z"]=-2.9165923595428, ["x"]=0,}, ["refNum"]=10957,},
    [494]={
    ["position"]={
    ["y"]=-1493.6374511719, ["z"]=-724.64001464844, ["x"]=290.5696105957,}, ["id"]="0x1d002ace", ["contentFile"]="beautiful cities of morrowind.esp", ["recordId"]="furn_de_r_chair_03", ["scale"]=0.88000005483627,
    ["rotation"]={ ["y"]=-0,
    ["z"]=0.30840617418289, ["x"]=0,}, ["refNum"]=10958,},
    [495]={
    ["position"]={
    ["y"]=-1482.7369384766, ["z"]=-724.64001464844, ["x"]=223.14416503906,}, ["id"]="0x1d002acf", ["contentFile"]="beautiful cities of morrowind.esp", ["recordId"]="furn_de_r_chair_03", ["scale"]=0.88000005483627,
    ["rotation"]={ ["y"]=-0,
    ["z"]=-0.21659308671951, ["x"]=0,}, ["refNum"]=10959,},
    [496]={
    ["position"]={
    ["y"]=-1404.9462890625, ["z"]=-724.64001464844, ["x"]=195.91358947754,}, ["id"]="0x1d002ad0", ["contentFile"]="beautiful cities of morrowind.esp", ["recordId"]="furn_de_r_chair_03", ["scale"]=0.88000005483627,
    ["rotation"]={ ["y"]=-0,
    ["z"]=-2.6165933609009, ["x"]=0,}, ["refNum"]=10960,},
    [497]={
    ["position"]={
    ["y"]=-1322.3851318359, ["z"]=-724.64001464844, ["x"]=406.54992675781,}, ["id"]="0x1d002ad1", ["contentFile"]="beautiful cities of morrowind.esp", ["recordId"]="furn_de_r_chair_03", ["scale"]=0.88000005483627,
    ["rotation"]={ ["y"]=-0,
    ["z"]=1.6415917873383, ["x"]=0,}, ["refNum"]=10961,},
    [498]={
    ["position"]={
    ["y"]=-1756.8895263672, ["z"]=-542.53314208984, ["x"]=-4.0177536010742,}, ["id"]="0x1d002ad2", ["contentFile"]="beautiful cities of morrowind.esp", ["recordId"]="furn_de_tapestry_07", ["scale"]=1,
    ["rotation"]={ ["y"]=-0,
    ["z"]=1.7484555314695e-07, ["x"]=0,}, ["refNum"]=10962,},
    [499]={
    ["position"]={
    ["y"]=-1758.0201416016, ["z"]=-542.53314208984, ["x"]=-230.7802734375,}, ["id"]="0x1d002ad3", ["contentFile"]="beautiful cities of morrowind.esp", ["recordId"]="furn_de_tapestry_07", ["scale"]=1,
    ["rotation"]={ ["y"]=-0,
    ["z"]=1.7484555314695e-07, ["x"]=0,}, ["refNum"]=10963,},
    [500]={
    ["position"]={
    ["y"]=-132.33827209473, ["z"]=-277.83386230469, ["x"]=253.30651855469,}, ["id"]="0x1d002b0d", ["contentFile"]="beautiful cities of morrowind.esp", ["recordId"]="furn_de_r_wallscreen_02", ["scale"]=1.4700000286102,
    ["rotation"]={ ["y"]=-0,
    ["z"]=0.0042043621651828, ["x"]=0,}, ["refNum"]=11021,},
    [501]={
    ["position"]={
    ["y"]=103.88195037842, ["z"]=-42.359687805176, ["x"]=-509.7444152832,}, ["id"]="0x1d002b0e", ["contentFile"]="beautiful cities of morrowind.esp", ["recordId"]="furn_de_tapestry_07", ["scale"]=1,
    ["rotation"]={ ["y"]=-0, ["z"]=-3.141592502594,
    ["x"]=0,}, ["refNum"]=11022,},
    [502]={
    ["position"]={
    ["y"]=-12.674137115479, ["z"]=41.488632202148, ["x"]=-508.14343261719,}, ["id"]="0x1d002b10", ["contentFile"]="beautiful cities of morrowind.esp", ["recordId"]="t_de_furnr_chair_01", ["scale"]=1,
    ["rotation"]={ ["y"]=-0, ["z"]=-3.1377875804901,
    ["x"]=0,}, ["refNum"]=11024,},
    [503]={
    ["position"]={
    ["y"]=-260.69387817383, ["z"]=-250.2582244873, ["x"]=-510.45516967773,}, ["id"]="0x1d002b13", ["contentFile"]="beautiful cities of morrowind.esp", ["recordId"]="t_de_furn_rugsmall_06", ["scale"]=1.1500000953674,
    ["rotation"]={ ["y"]=-0,
    ["z"]=-1.570796251297, ["x"]=0,}, ["refNum"]=11027,},
    [504]={
    ["position"]={
    ["y"]=-462.21060180664, ["z"]=-682.66143798828, ["x"]=75.310455322266,}, ["id"]="0x1d002b14", ["contentFile"]="beautiful cities of morrowind.esp", ["recordId"]="furn_de_r_bookshelf_02", ["scale"]=1,
    ["rotation"]={ ["y"]=-0, ["z"]=-1.649999499321,
    ["x"]=0,}, ["refNum"]=11028,},}