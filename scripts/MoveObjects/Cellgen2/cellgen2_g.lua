local structureData = require("scripts.MoveObjects.StructureData")
local util = require("openmw.util")
local world = require("openmw.world")
local core = require("openmw.core")
local types = require("openmw.types")
local storage = require("openmw.storage")
local I = require("openmw.interfaces")
local async = require("openmw.async")

local cellGenStorage = storage.globalSection("AACellGen2")
local cellGenData = {}

local function processNewStructure(objectList, templateName, markerList)
    --Process the list of exterior objects, copy the interior cell.
    --Find the doors, link them.

    async:newUnsavableSimulationTimer(0.1, function()
        I.AA_CellGen_2_CellCopy.copyCell(templateName, objectList,markerList)
    end

    )
end
local function saveCellGenData(cdata)
    if not cellGenData then
        cellGenData = {}
    end
    table.insert(cellGenData, cdata)
    cellGenStorage:set("CellGenData", cellGenData)
end
local function getCellGenData()
    return cellGenData
end

local function distanceBetweenPos(vector1, vector2)
    --Quick way to find out the distance between two vectors.
    --Very similar to getdistance in mwscript
    if (vector1 == nil or vector2 == nil) then
        error("Invalid position data provided")
    end
    local dx = vector2.x - vector1.x
    local dy = vector2.y - vector1.y
    local dz = vector2.z - vector1.z
    return math.sqrt(dx * dx + dy * dy + dz * dz)
end

local function getSurroundCellObjects(cell, distance, originPos)
    -- Ensure cell is valid; if given as an ID, retrieve the actual cell
    if not cell.id then
        cell = world.getCellById(cell)
    end
    -- Fallback to the player's current cell if no cell is provided
    if not cell then cell = world.players[1].cell end
    -- If the cell is not exterior, return all objects in it
    if not cell.isExterior then return cell:getAll() end

    -- Define relative grid offsets for surrounding cells
    local offsets = {
        {0, 0}, -- Current cell
        {0, 1}, -- North
        {0, -1}, -- South
        {-1, 0}, -- West
        {1, 0}, -- East
        {1, 1}, -- Northeast
        {1, -1}, -- Southeast
        {-1, 1}, -- Northwest
        {-1, -1} -- Southwest
    }

    -- Collect objects from all relevant cells
    local objectList = {}
    for _, offset in ipairs(offsets) do
        local adjacentCell = world.getExteriorCell(cell.gridX + offset[1], cell.gridY + offset[2])
        if adjacentCell then -- Ensure the cell is valid
            for _, obj in ipairs(adjacentCell:getAll()) do
                local inRange = true
                if distance and originPos then
                    inRange = distanceBetweenPos(obj.position, originPos) < distance
                end
                if inRange then
                    
                table.insert(objectList, obj)
                end
            end
        end
    end

    return objectList
end


local function findDoorPair(door)
    --TODO: Check for rotation
    if door.type ~= types.Door then return end
    if not types.Door.isTeleport(door) then return end
    local doorDestCell = types.Door.destCell(door)
    local doorDestPos = types.Door.destPosition(door)
    for index, value in ipairs(getSurroundCellObjects(doorDestCell)) do
        if value.type == types.Door and value.enabled == true then
            if types.Door.isTeleport(value) and types.Door.destCell(value).worldSpaceId == door.cell.worldSpaceId then
                local destCheck = I.ZackUtilsAA.distanceBetweenPos(value.position, doorDestPos)
                if destCheck < 200 then
                    return { value, door }
                end
            end
        end
    end
    for index, value in ipairs(getSurroundCellObjects(doorDestCell)) do
        if value.type == types.Door and value.enabled == true then
            if types.Door.isTeleport(value) and types.Door.destCell(value).worldSpaceId == door.cell.worldSpaceId then
                local destCheck = I.ZackUtilsAA.distanceBetweenPos(value.position, doorDestPos)
                if destCheck < 400 then
                    return { value, door }
                end
            end
        end
    end
    for index, value in ipairs(getSurroundCellObjects(doorDestCell)) do
        if value.type == types.Door and value.enabled == true then
            if types.Door.isTeleport(value) and types.Door.destCell(value).worldSpaceId == door.cell.worldSpaceId then
                local destCheck = I.ZackUtilsAA.distanceBetweenPos(value.position, doorDestPos)
                if destCheck < 600 then
                    return { value, door }
                end
            end
        end
    end
    for index, value in ipairs(getSurroundCellObjects(doorDestCell)) do
        if value.type == types.Door and value.enabled == true then
            if types.Door.isTeleport(value) and types.Door.destCell(value).worldSpaceId == door.cell.worldSpaceId then
                local destCheck = I.ZackUtilsAA.distanceBetweenPos(value.position, doorDestPos)
                if destCheck < 1600 then
                    return { value, door }
                end
                print(destCheck)
            end
        end
    end
    error("Unable to find door!")
end
return {
    interfaceName = "AA_CellGen_2",
    interface = {
        version                = 1,
        findDoorPair           = findDoorPair,
        processNewStructure    = processNewStructure,
        saveCellGenData        = saveCellGenData,
        getCellGenData         = getCellGenData,
        getSurroundCellObjects = getSurroundCellObjects,
    },
    eventHandlers = {
    },
    engineHandlers = {
        onSave = function() return { cellGenData = cellGenData } end,
        onLoad = function(data)
            if not data then
                cellGenStorage:set("CellGenData", cellGenData)
                return
            end
            cellGenData = data.cellGenData
            cellGenStorage:set("CellGenData", cellGenData)
        end
    }
}
