local core = require("openmw.core")
local isUpdated = core.API_REVISION >= 68
local devMode = false
local enableTravelOverride = true
local devPrint = function(words)
    if devMode then
        print(words)
    end
end
return { isUpdated = isUpdated, buildDate = "Nov 21, 2024",print = devPrint , enableTravelOverride = enableTravelOverride}
